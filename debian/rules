#!/usr/bin/make -f
# Sample debian/rules that uses debhelper. GNU copyright 1997 by Joey Hess.

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

export QUILT_PATCHES := debian/patches

DDIR=$(shell pwd)/debian/tmp

unpatch:
	dh_testdir
	quilt pop -a || test $$? = 2
	rm -rf .pc
	rm -f patch-stamp

patch: patch-stamp
patch-stamp:
	dh_testdir
	quilt push -a || test $$? = 2
	touch patch-stamp

clean: clean-patched unpatch
	dh_testdir
	dh_testroot
	dh_clean

clean-patched: patch-stamp
	dh_testdir
	dh_testroot
	rm -rf $(DDIR)

build-indep: patch-stamp
	# Nothing to do here.

build-arch:
	# Nothing to do here.

build: build-indep build-arch
	# Nothing to do here.

install: build-indep
	dh_testdir
	dh_testroot
	dh_clean -k

	mkdir -p $(DDIR)

	# Copy man pages to destination dir:
	cp -r man $(DDIR)

	# Fix the erlang_mode tags:
	sed -e"s/^Erlang mode for Emacs/erlang_mode/" \
	    -e"s/erlang\\\&\.el/erlang_mode/" \
	    man/man3/erlang_mode.3 >$(DDIR)/man/man3/erlang_mode.3

	# Remove M$ Windows related manuals
	for i in erlsrv start_erl werl ; do \
	    rm $(DDIR)/man/man1/$$i.1 ; \
	done

	# Fix erl_driver and driver_entry manpages (move from section 1 to section 3)
	#for i in erl_driver driver_entry ; do \
	#    sed 's/\(^\.TH[[:space:]]\+[[:alnum:]_]*[[:space:]]\+\)1 /\1 3 /' \
	#	man/man1/$$i.1 >$(DDIR)/man/man1/$$i.1 ; \
	#done

	# Rename start manpage to start_embedded one (start is too general name to use
	# see bug #449099).
	mv $(DDIR)/man/man1/start.1 $(DDIR)/man/man1/start_embedded.1
	sed -i -e's/^\.TH[[:space:]]\+start[[:space:]]/.TH start_embedded /' \
	    $(DDIR)/man/man1/start_embedded.1
	for i in $(DDIR)/man/man1/* ; do \
	    sed -i -e's/start(1)/start_embedded(1)/g' $$i ; \
	done

	# Fix dialyzer manpage (move from section 3 to section 1)
	for i in dialyzer ; do \
	    sed 's/\(^\.TH[[:space:]]\+[[:alnum:]_]*[[:space:]]\+\)3 /\1 1 /' \
		man/man3/$$i.3 >$(DDIR)/man/man1/$$i.1 ; \
	done

	# Fix manpages (migrate "3" to "3erl",
	# replace Latin-1 numbered chars by their Troff equivalents)
	for i in man1 man3 man4 man6 ; do \
	    (cd $(DDIR)/man/$$i ; perl -w ../../../fixmanpages) \
	done

# Build architecture-independent files here.
binary-indep: install
	dh_testdir
	dh_testroot
	dh_installdirs -i
	dh_installdocs -i
#	dh_installexamples -i
#	dh_installmenu -i
#	dh_installemacsen -i
#	dh_installinit -i
#	dh_installcron -i
	dh_installman -i
#	dh_undocumented -i
	dh_installchangelogs -i
#	dh_strip -i
	dh_compress -i
	dh_fixperms -i
	dh_makeshlibs -i
	dh_installdeb -i
#	dh_shlibdeps -i
	dh_gencontrol -i
	dh_md5sums -i
	dh_builddeb -i


# Build architecture-dependent files here.
binary-arch:
	# Nothing to do here.

binary: binary-indep binary-arch

get-orig-source:
	wget -O erlang-manpages_13.b.2.1.orig.tar.gz \
		http://www.erlang.org/download/otp_doc_man_R13B02-1.tar.gz

.PHONY: patch unpatch build build-indep build-arch clean clean-patched binary-indep binary-arch binary install get-orig-source
