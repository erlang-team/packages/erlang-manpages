.TH common_test 6 "common_test  1.4.5" "Ericsson AB" "ERLANG APPLICATION DEFINITION"
.SH APPLICATION
Common Test \- A framework for automated testing of arbitrary target nodes
.SH DESCRIPTION
.LP
The \fICommon Test\fR framework is an environment for implementing and performing automatic and semi-automatic execution of test cases\&. Common Test uses the OTP Test Server as engine for test case execution and logging\&.
.LP
In brief, Common Test supports:
.RS 2
.TP 2
*
Automated execution of test suites (sets of test cases)\&.
.TP 2
*
Logging of the events during execution\&.
.TP 2
*
HTML presentation of test suite results\&.
.TP 2
*
HTML presentation of test suite code\&.
.TP 2
*
Support functions for test suite authors\&.
.TP 2
*
Step by step execution of test cases\&.
.RE
.LP
The following sections describe the mandatory and optional test suite functions Common Test will call during test execution\&. For more details see Common Test User\&'s Guide\&. 

.SH TEST CASE CALLBACK FUNCTIONS
.LP
The following functions define the callback interface for a test suite\&.
.SH EXPORTS
.LP
.B
Module:all() -> TestCases | {skip,Reason} 
.br
.RS
.TP
Types
TestCases = [atom() | {group, GroupName}]
.br
Reason = term()
.br
GroupName = atom()
.br
.RE
.RS
.LP
MANDATORY 
.LP
This function must return the list of all test cases and test case groups in the test suite module that are to be executed\&. This list also specifies the order the cases and groups will be executed by Common Test\&. A test case is represented by an atom, the name of the test case function\&. A test case group is represented by a \fI{group, GroupName}\fR tuple, where \fIGroupName\fR, an atom, is the name of the group (defined with \fIgroups/0\fR)\&.
.LP
If \fI{skip, Reason}\fR is returned, all test cases in the module will be skipped, and the \fIReason\fR will be printed on the HTML result page\&.
.LP
For details on groups, see Test case groups in the User\&'s Guide\&.
.RE
.LP
.B
Module:groups() -> GroupDefs
.br
.RS
.TP
Types
GroupDefs = [Group]
.br
Group = {GroupName, Properties, GroupsAndTestCases}
.br
GroupName = atom()
.br
Properties = [parallel | sequence | Shuffle | {RepeatType, N}]
.br
GroupsAndTestCases = [Group | {group, GroupName} | TestCase]
.br
TestCase = atom()
.br
Shuffle = shuffle | {shuffle, Seed}
.br
Seed = {integer(), integer(), integer()}
.br
RepeatType = repeat | repeat_until_all_ok | repeat_until_all_fail | repeat_until_any_ok | repeat_until_any_fail
.br
N = integer() | forever
.br
.RE
.RS
.LP
OPTIONAL 
.LP
See Test case groups in the User\&'s Guide for details\&.
.RE
.LP
.B
Module:suite() -> [Info] 
.br
.RS
.TP
Types
 Info = {timetrap, Time} | {require, Required} | {require, Name, Required} | {userdata, UserData} | {silent_connections, Conns} | {stylesheet, CSSFile}
.br
 Time = MilliSec | {seconds, integer()} | {minutes, integer()} | {hours, integer()}
.br
 MilliSec = integer()
.br
 Required = Key | {Key, SubKeys}
.br
 Key = atom()
.br
 SubKeys = SubKey | [SubKey]
.br
 SubKey = atom()
.br
 Name = atom()
.br
 UserData = term()
.br
 Conns = [atom()]
.br
 CSSFile = string()
.br
.RE
.RS
.LP
OPTIONAL 
.LP
This is the test suite info function\&. It is supposed to return a list of tagged tuples that specify various properties regarding the execution of this test suite (common for all test cases in the suite)\&.
.LP
The \fItimetrap\fR tag sets the maximum time each test case is allowed to take (including \fIinit_per_testcase/2\fR and \fIend_per_testcase/2\fR)\&. If the timetrap time is exceeded, the test case fails with reason \fItimetrap_timeout\fR\&.
.LP
The \fIrequire\fR tag specifies configuration variables that are required by test cases in the suite\&. If the required configuration variables are not found in any of the configuration files, all test cases are skipped\&. For more information about the \&'require\&' functionality, see the reference manual for the function \fIct:require/[1, 2]\fR\&.
.LP
With \fIuserdata\fR, it is possible for the user to specify arbitrary test suite related information which can be read by calling \fIct:userdata/2\fR\&.
.LP
Other tuples than the ones defined will simply be ignored\&.
.LP
For more information about the test suite info function, see Test suite info function in the User\&'s Guide\&.
.RE
.LP
.B
Module:init_per_suite(Config) -> NewConfig | {skip,Reason} | {skip_and_save,Reason,SaveConfig}
.br
.RS
.TP
Types
 Config = NewConfig = SaveConfig = [{Key, Value}]
.br
 Key = atom()
.br
 Value = term()
.br
 Reason = term()
.br
.RE
.RS
.LP
OPTIONAL 
.LP
This function is called as the first function in the suite\&. It typically contains initialization which is common for all test cases in the suite, and which shall only be done once\&. The \fIConfig\fR parameter is the configuration which can be modified here\&. Whatever is returned from this function is given as \fIConfig\fR to all configuration functions and test cases in the suite\&. If \fI{skip, Reason}\fR is returned, all test cases in the suite will be skipped and \fIReason\fR printed in the overview log for the suite\&.
.LP
For information on \fIsave_config\fR and \fIskip_and_save\fR, please see Dependencies between Test Cases and Suites in the User\&'s Guide\&.
.RE
.LP
.B
Module:end_per_suite(Config) -> void() | {save_config,SaveConfig}
.br
.RS
.TP
Types
 Config = SaveConfig = [{Key, Value}]
.br
 Key = atom()
.br
 Value = term()
.br
.RE
.RS
.LP
OPTIONAL 
.LP
This function is called as the last test case in the suite\&. It is meant to be used for cleaning up after \fIinit_per_suite/1\fR\&. For information on \fIsave_config\fR, please see Dependencies between Test Cases and Suites in the User\&'s Guide\&.
.RE
.LP
.B
Module:init_per_group(GroupName, Config) -> NewConfig | {skip,Reason}
.br
.RS
.TP
Types
 GroupName = atom()
.br
 Config = NewConfig = [{Key, Value}]
.br
 Key = atom()
.br
 Value = term()
.br
 Reason = term()
.br
.RE
.RS
.LP
OPTIONAL 
.LP
This function is called before execution of a test case group\&. It typically contains initialization which is common for all test cases in the group, and which shall only be performed once\&. \fIGroupName\fR is the name of the group, as specified in the group definition (see \fIgroups/0\fR)\&. The \fIConfig\fR parameter is the configuration which can be modified here\&. Whatever is returned from this function is given as \fIConfig\fR to all test cases in the group\&. If \fI{skip, Reason}\fR is returned, all test cases in the group will be skipped and \fIReason\fR printed in the overview log for the group\&.
.LP
For information about test case groups, please see Test case groups chapter in the User\&'s Guide\&.
.RE
.LP
.B
Module:end_per_group(GroupName, Config) -> void() | {return_group_result,Status}
.br
.RS
.TP
Types
 GroupName = atom()
.br
 Config = [{Key, Value}]
.br
 Key = atom()
.br
 Value = term()
.br
 Status = ok | skipped | failed
.br
.RE
.RS
.LP
OPTIONAL 
.LP
This function is called after the execution of a test case group is finished\&. It is meant to be used for cleaning up after \fIinit_per_group/2\fR\&. By means of \fI{return_group_result, Status}\fR, it is possible to return a status value for a nested sub-group\&. The status can be retrieved in \fIend_per_group/2\fR for the group on the level above\&. The status will also be used by Common Test for deciding if execution of a group should proceed in case the property \fIsequence\fR or \fIrepeat_until_*\fR is set\&.
.LP
For more information about test case groups, please see Test case groups chapter in the User\&'s Guide\&.
.RE
.LP
.B
Module:init_per_testcase(TestCase, Config) -> NewConfig | {skip,Reason}
.br
.RS
.TP
Types
 TestCase = atom()
.br
 Config = NewConfig = [{Key, Value}]
.br
 Key = atom()
.br
 Value = term()
.br
 Reason = term()
.br
.RE
.RS
.LP
OPTIONAL
.LP
This function is called before each test case\&. The \fITestCase\fR argument is the name of the test case, and \fIConfig\fR is the configuration which can be modified here\&. Whatever is returned from this function is given as \fIConfig\fR to the test case\&. If \fI{skip, Reason}\fR is returned, the test case will be skipped and \fIReason\fR printed in the overview log for the suite\&.
.RE
.LP
.B
Module:end_per_testcase(TestCase, Config) -> void() | {save_config,SaveConfig}
.br
.RS
.TP
Types
 TestCase = atom()
.br
 Config = SaveConfig = [{Key, Value}]
.br
 Key = atom()
.br
 Value = term()
.br
.RE
.RS
.LP
OPTIONAL 
.LP
This function is called after each test case, and can be used to clean up after \fIinit_per_testcase/2\fR and the test case\&. Any return value (besides \fI{save_config, SaveConfig}\fR) is ignored\&. For information on \fIsave_config\fR, please see Dependencies between Test Cases and Suites in the User\&'s Guide
.RE
.LP
.B
Module:testcase() -> [Info] 
.br
.RS
.TP
Types
 Info = {timetrap, Time} | {require, Required} | {require, Name, Required} | {userdata, UserData} | {silent_connections, Conns}
.br
 Time = MilliSec | {seconds, integer()} | {minutes, integer()} | {hours, integer()}
.br
 MilliSec = integer()
.br
 Required = Key | {Key, SubKeys}
.br
 Key = atom()
.br
 SubKeys = SubKey | [SubKey]
.br
 SubKey = atom()
.br
 Name = atom()
.br
 UserData = term()
.br
 Conns = [atom()]
.br
.RE
.RS
.LP
OPTIONAL
.LP
This is the test case info function\&. It is supposed to return a list of tagged tuples that specify various properties regarding the execution of this particular test case\&.
.LP
The \fItimetrap\fR tag sets the maximum time the test case is allowed to take\&. If the timetrap time is exceeded, the test case fails with reason \fItimetrap_timeout\fR\&. \fIinit_per_testcase/2\fR and \fIend_per_testcase/2\fR are included in the timetrap time\&.
.LP
The \fIrequire\fR tag specifies configuration variables that are required by the test case\&. If the required configuration variables are not found in any of the configuration files, the test case is skipped\&. For more information about the \&'require\&' functionality, see the reference manual for the function \fIct:require/[1, 2]\fR\&.
.LP
If \fItimetrap\fR and/or \fIrequire\fR is not set, the default values specified in the \fIsuite/0\fR return list will be used\&.
.LP
With \fIuserdata\fR, it is possible for the user to specify arbitrary test case related information which can be read by calling \fIct:userdata/3\fR\&.
.LP
Other tuples than the ones defined will simply be ignored\&.
.LP
For more information about the test case info function, see Test case info function in the User\&'s Guide\&.
.RE
.LP
.B
Module:testcase(Config) -> void() | {skip,Reason} | {comment,Comment} | {save_config,SaveConfig} | {skip_and_save,Reason,SaveConfig} | exit() 
.br
.RS
.TP
Types
 Config = SaveConfig = [{Key, Value}]
.br
 Key = atom()
.br
 Value = term()
.br
 Reason = term()
.br
 Comment = string()
.br
.RE
.RS
.LP
MANDATORY 
.LP
This is the implementation of a test case\&. Here you must call the functions you want to test, and do whatever you need to check the result\&. If someting fails, make sure the function causes a runtime error, or call \fIct:fail/[0, 1]\fR (which also causes the test case process to crash)\&.
.LP
Elements from the \fIConfig\fR parameter can be read with the \fI?config\fR macro\&. The \fIconfig\fR macro is defined in \fIct\&.hrl\fR
.LP
You can return \fI{skip, Reason}\fR if you decide not to run the test case after all\&. \fIReason\fR will then be printed in \&'Comment\&' field on the HTML result page\&.
.LP
You can return \fI{comment, Comment}\fR if you wish to print some information in the \&'Comment\&' field on the HTML result page\&.
.LP
If the function returns anything else, the test case is considered successful\&. (The return value always gets printed in the test case log file)\&.
.LP
For more information about test case implementation, please see Test cases in the User\&'s Guide\&.
.LP
For information on \fIsave_config\fR and \fIskip_and_save\fR, please see Dependencies between Test Cases and Suites in the User\&'s Guide\&.
.RE
