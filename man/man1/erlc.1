.TH erlc 1 "erts  5.7.3" "Ericsson AB" "USER COMMANDS"
.SH NAME
erlc \- Compiler
.SH DESCRIPTION
.LP
The \fIerlc\fR program provides a common way to run all compilers in the Erlang system\&. Depending on the extension of each input file, \fIerlc\fR will invoke the appropriate compiler\&. Regardless of which compiler is used, the same flags are used to provide parameters such as include paths and output directory\&.
.LP
The current working directory, \fI"\&."\fR, will not be included in the code path when running the compiler (to avoid loading Beam files from the current working directory that could potentially be in conflict with the compiler or Erlang/OTP system used by the compiler)\&.

.SH EXPORTS
.LP
.B
erlc flags file1\&.ext file2\&.ext\&.\&.\&.
.br
.RS
.LP
\fIErlc\fR compiles one or more files\&. The files must include the extension, for example \fI\&.erl\fR for Erlang source code, or \fI\&.yrl\fR for Yecc source code\&. \fIErlc\fR uses the extension to invoke the correct compiler\&.
.RE
.SH GENERALLY USEFUL FLAGS
.LP
The following flags are supported: 
.RS 2
.TP 4
.B
-I \fIdirectory\fR:
Instructs the compiler to search for include files in the specified directory\&. When encountering an \fI-include\fR or \fI-include_dir\fR directive, the compiler searches for header files in the following directories:
.RS 4
.LP

.RS 2
.TP 4
1.
\fI"\&."\fR, the current working directory of the file server;
.TP 4
2.
the base name of the compiled file;
.TP 4
3.
the directories specified using the \fI-I\fR option\&. The directory specified last is searched first\&.
.RE
.RE
.TP 4
.B
-o \fIdirectory\fR:
The directory where the compiler should place the output files\&. If not specified, output files will be placed in the current working directory\&.
.TP 4
.B
-D\fIname\fR:
Defines a macro\&.
.TP 4
.B
-D\fIname\fR=\fIvalue\fR:
Defines a macro with the given value\&. The value can be any Erlang term\&. Depending on the platform, the value may need to be quoted if the shell itself interprets certain characters\&. On Unix, terms which contain tuples and list must be quoted\&. Terms which contain spaces must be quoted on all platforms\&.
.TP 4
.B
-W\fInumber\fR:
Sets warning level to \fInumber\fR\&. Default is \fI1\fR\&. Use \fI-W0\fR to turn off warnings\&.
.TP 4
.B
-W:
Same as \fI-W1\fR\&. Default\&.
.TP 4
.B
-v:
Enables verbose output\&.
.TP 4
.B
-b \fIoutput-type\fR:
Specifies the type of output file\&. Generally, \fIoutput-type\fR is the same as the file extension of the output file but without the period\&. This option will be ignored by compilers that have a a single output format\&.
.TP 4
.B
-hybrid:
Compile using the hybrid-heap emulator\&. This is mainly useful for compiling native code, which needs to be compiled with the same run-time system that it should be run on\&.
.TP 4
.B
-smp:
Compile using the SMP emulator\&. This is mainly useful for compiling native code, which needs to be compiled with the same run-time system that it should be run on\&.
.TP 4
.B
--:
Signals that no more options will follow\&. The rest of the arguments will be treated as file names, even if they start with hyphens\&.
.TP 4
.B
+\fIterm\fR:
A flag starting with a plus (\&'\fI+\fR\&') rather than a hyphen will be converted to an Erlang term and passed unchanged to the compiler\&. For instance, the \fIexport_all\fR option for the Erlang compiler can be specified as follows:
.RS 4
.LP


.nf
erlc +export_all file\&.erl
.fi
.LP

.LP
Depending on the platform, the value may need to be quoted if the shell itself interprets certain characters\&. On Unix, terms which contain tuples and list must be quoted\&. Terms which contain spaces must be quoted on all platforms\&.
.RE
.RE
.SH SPECIAL FLAGS
.LP
The flags in this section are useful in special situations such as re-building the OTP system\&.
.RS 2
.TP 4
.B
-pa \fIdirectory\fR:
Appends \fIdirectory\fR to the front of the code path in the invoked Erlang emulator\&. This can be used to invoke another compiler than the default one\&.
.TP 4
.B
-pz \fIdirectory\fR:
Appends \fIdirectory\fR to the code path in the invoked Erlang emulator\&.
.RE
.SH SUPPORTED COMPILERS
.RS 2
.TP 4
.B
\&.erl:
Erlang source code\&. It generates a \fI\&.beam\fR file\&.
.RS 4
.LP

.LP
The options -P, -E, and -S are equivalent to +\&'P\&', +\&'E\&', and +\&'S\&', except that it is not necessary to include the single quotes to protect them from the shell\&.
.LP

.LP
Supported options: -I, -o, -D, -v, -W, -b\&.
.RE
.TP 4
.B
\&.yrl:
Yecc source code\&. It generates an \fI\&.erl\fR file\&.
.RS 4
.LP

.LP
Use the -I option with the name of a file to use that file as a customized prologue file (the \fIincludefile\fR option)\&.
.LP

.LP
Supported options: -o, -v, -I, -W (see above)\&.
.RE
.TP 4
.B
\&.mib:
MIB for SNMP\&. It generates a \fI\&.bin\fR file\&.
.RS 4
.LP

.LP
Supported options: -I, -o, -W\&.
.RE
.TP 4
.B
\&.bin:
A compiled MIB for SNMP\&. It generates a \fI\&.hrl\fR file\&.
.RS 4
.LP

.LP
Supported options: -o, -v\&.
.RE
.TP 4
.B
\&.rel:
Script file\&. It generates a boot file\&.
.RS 4
.LP

.LP
Use the -I to name directories to be searched for application files (equivalent to the \fIpath\fR in the option list for \fIsystools:make_script/2\fR)\&.
.LP

.LP
Supported options: -o\&.
.RE
.TP 4
.B
\&.asn1:
ASN1 file\&.
.RS 4
.LP

.LP
Creates an \fI\&.erl\fR, \fI\&.hrl\fR, and \fI\&.asn1db\fR file from an \fI\&.asn1\fR file\&. Also compiles the \fI\&.erl\fR using the Erlang compiler unless the \fI+noobj\fR options is given\&.
.LP

.LP
Supported options: -I, -o, -b, -W\&.
.RE
.TP 4
.B
\&.idl:
IC file\&.
.RS 4
.LP

.LP
Runs the IDL compiler\&.
.LP

.LP
Supported options: -I, -o\&.
.RE
.RE
.SH ENVIRONMENT VARIABLES
.RS 2
.TP 4
.B
ERLC_EMULATOR:
The command for starting the emulator\&. Default is \fIerl\fR in the same directory as the \fIerlc\fR program itself, or if it doesn\&'t exist, \fIerl\fR in any of the directories given in the \fIPATH\fR environment variable\&.
.RE
.SH SEE ALSO
.LP
erl(1), compile(3), yecc(3), snmp(3)
