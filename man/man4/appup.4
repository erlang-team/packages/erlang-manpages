.TH appup 4 "sasl  2.1.7" "Ericsson AB" "FILES"
.SH NAME
appup \- Application upgrade file\&.
.SH DESCRIPTION
.LP
The \fIapplication upgrade file\fR defines how an application is upgraded or downgraded in a running system\&.
.LP
This file is used by the functions in \fIsystools\fR when generating a release upgrade file \fIrelup\fR\&.

.SH FILE SYNTAX
.LP
The application upgrade file should be called \fIApplication\&.appup\fR where \fIApplication\fR is the name of the application\&. The file should be located in the \fIebin\fR directory for the application\&.
.LP
The \fI\&.appup\fR file contains one single Erlang term, which defines the instructions used to upgrade or downgrade the application\&. The file has the following syntax:

.nf
{Vsn,
  [{UpFromVsn, Instructions}, \&.\&.\&.],
  [{DownToVsn, Instructions}, \&.\&.\&.]}\&.
    
.fi
.RS 2
.TP 2
*
\fIVsn = string()\fR is the current version of the application\&.
.TP 2
*
\fIUpFromVsn = string()\fR is an earlier version of the application to upgrade from\&.
.TP 2
*
\fIDownToVsn = string()\fR is an earlier version of the application to downgrade to\&.
.TP 2
*
\fIInstructions\fR is a list of \fIrelease upgrade instructions\fR, see below\&. It is recommended to use high-level instructions only\&. These are automatically translated to low-level instructions by \fIsystools\fR when creating the \fIrelup\fR file\&.
.RE
.SH RELEASE UPGRADE INSTRUCTIONS
.LP
Release upgrade instructions are interpreted by the release handler when an upgrade or downgrade is made\&. For more information about release handling, refer to \fIOTP Design Principes\fR\&.
.LP
A process is said to \fIuse\fR a module \fIMod\fR, if \fIMod\fR is listed in the \fIModules\fR part of the child specification used to start the process, see \fIsupervisor(3)\fR\&. In the case of gen_event, an event manager process is said to use \fIMod\fR if \fIMod\fR is an installed event handler\&.
.LP
\fIHigh-level instructions\fR

.nf
{update, Mod}
{update, Mod, supervisor}
{update, Mod, Change}
{update, Mod, DepMods}
{update, Mod, Change, DepMods}
{update, Mod, Change, PrePurge, PostPurge, DepMods}
{update, Mod, Timeout, Change, PrePurge, PostPurge, DepMods}
{update, Mod, ModType, Timeout, Change, PrePurge, PostPurge, DepMods}
  Mod = atom()
  ModType = static | dynamic
  Timeout = int()>0 | default | infinity
  Change = soft | {advanced,Extra}
    Extra = term()
  PrePurge = PostPurge = soft_purge | brutal_purge
  DepMods = [Mod]
    
.fi
.LP
Synchronized code replacement of processes using the module \fIMod\fR\&. All those processes are suspended using \fIsys:suspend\fR, the new version of the module is loaded and then the processes are resumed using \fIsys:resume\fR\&.
.LP
\fIChange\fR defaults to \fIsoft\fR and defines the type of code change\&. If it is set to \fI{advanced, Extra}\fR, processes implemented using gen_server, gen_fsm or gen_event will transform their internal state by calling the callback function \fIcode_change\fR\&. Special processes will call the callback function \fIsystem_code_change/4\fR\&. In both cases, the term \fIExtra\fR is passed as an argument to the callback function\&.
.LP
\fIPrePurge\fR defaults to \fIbrutal_purge\fR and controls what action to take with processes that are executing old code before loading the new version of the module\&. If the value is \fIbrutal_purge\fR, the processes are killed\&. If the value is \fIsoft_purge\fR, \fIrelease_handler:install_release/1\fR returns \fI{error, {old_processes, Mod}}\fR\&.
.LP
\fIPostPurge\fR defaults to \fIbrutal_purge\fR and controls what action to take with processes that are executing old code when the new version of the module has been loaded\&. If the value is \fIbrutal_purge\fR, the code is purged when the release is made permanent and the processes are killed\&. If the value is \fIsoft_purge\fR, the release handler will purge the old code when no remaining processes execute the code\&.
.LP
\fIDepMods\fR defaults to [] and defines which other modules \fIMod\fR is dependent on\&. In \fIrelup\fR, instructions for suspending processes using \fIMod\fR will come before instructions for suspending processes using modules in \fIDepMods\fR when upgrading, and vice versa when downgrading\&. In case of circular dependencies, the order of the instructions in the \fIappup\fR script is kept\&.
.LP
\fITimeout\fR defines the timeout when suspending processes\&. If no value or \fIdefault\fR is given, the default value for \fIsys:suspend\fR is used\&.
.LP
\fIModType\fR defaults to \fIdynamic\fR and specifies if the code is "dynamic", that is if a process using the module does spontaneously switch to new code, or if it is "static"\&. When doing an advanced update and upgrading, the new version of a dynamic module is loaded before the process is asked to change code\&. When downgrading, the process is asked to change code before loading the new version\&. For static modules, the new version is loaded before the process is asked to change code, both in the case of upgrading and downgrading\&. Callback modules are dynamic\&.
.LP
\fIupdate\fR with argument \fIsupervisor\fR is used when changing the start specification of a supervisor\&.

.nf
{load_module, Mod}
{load_module, Mod, DepMods}
{load_module, Mod, PrePurge, PostPurge, DepMods}
  Mod = atom()
  PrePurge = PostPurge = soft_purge | brutal_purge
  DepMods = [Mod]
    
.fi
.LP
Simple code replacement of the module \fIMod\fR\&.
.LP
See \fIupdate\fR above for a description of \fIPrePurge\fR and \fIPostPurge\fR\&.
.LP
\fIDepMods\fR defaults to [] and defines which other modules \fIMod\fR is dependent on\&. In \fIrelup\fR, instructions for loading these modules will come before the instruction for loading \fIMod\fR when upgrading, and vice versa when downgrading\&.

.nf
{add_module, Mod}
  Mod = atom()
    
.fi
.LP
Loads a new module \fIMod\fR\&.

.nf
{delete_module, Mod}
  Mod = atom()
    
.fi
.LP
Deletes a module \fIMod\fR using the low-level instructions \fIremove\fR and \fIpurge\fR\&.

.nf
{add_application, Application}
  Application = atom()
    
.fi
.LP
Adding an application means that the modules defined by the \fImodules\fR key in the \fI\&.app\fR file are loaded using \fIadd_module\fR, then the application is started\&.

.nf
{remove_application, Application}
  Application = atom()
    
.fi
.LP
Removing an application means that the application is stopped, the modules are unloaded using \fIdelete_module\fR and then the application specification is unloaded from the application controller\&.

.nf
{restart_application, Application}
  Application = atom()
    
.fi
.LP
Restarting an application means that the application is stopped and then started again similar to using the instructions \fIremove_application\fR and \fIadd_application\fR in sequence\&.
.LP
\fILow-level instructions\fR

.nf
{load_object_code, {App, Vsn, [Mod]}}
  App = Mod = atom()
  Vsn = string()
    
.fi
.LP
Reads each \fIMod\fR from the directory \fIApp-Vsn/ebin\fR as a binary\&. It does not load the modules\&. The instruction should be placed first in the script in order to read all new code from file to make the suspend-load-resume cycle less time consuming\&. After this instruction has been executed, the code server with the new version of \fIApp\fR\&.

.nf
point_of_no_return
    
.fi
.LP
If a crash occurs after this instruction, the system cannot recover and is restarted from the old version of the release\&. The instruction must only occur once in a script\&. It should be placed after all \fIload_object_code\fR instructions\&.

.nf
{load, {Mod, PrePurge, PostPurge}}
  Mod = atom()
  PrePurge = PostPurge = soft_purge | brutal_purge
    
.fi
.LP
Before this instruction occurs, \fIMod\fR must have been loaded using \fIload_object_code\fR\&. This instruction loads the module\&. \fIPrePurge\fR is ignored\&. See the high-level instruction \fIupdate\fR for a description of \fIPostPurge\fR\&.

.nf
{remove, {Mod, PrePurge, PostPurge}}
  Mod = atom()
  PrePurge = PostPurge = soft_purge | brutal_purge
    
.fi
.LP
Makes the current version of \fIMod\fR old\&. \fIPrePurge\fR is ignored\&. See the high-level instruction \fIupdate\fR for a description of \fIPostPurge\fR\&.

.nf
{purge, [Mod]}
  Mod = atom()
    
.fi
.LP
Purges each module \fIMod\fR, that is removes the old code\&. Note that any process executing purged code is killed\&.

.nf
{suspend, [Mod | {Mod, Timeout}]}
  Mod = atom()
  Timeout = int()>0 | default | infinity
    
.fi
.LP
Tries to suspend all processes using a module \fIMod\fR\&. If a process does not respond, it is ignored\&. This may cause the process to die, either because it crashes when it spontaneously switches to new code, or as a result of a purge operation\&. If no \fITimeout\fR is specified or \fIdefault\fR is given, the default value for \fIsys:suspend\fR is used\&.

.nf
{resume, [Mod]}
  Mod = atom()
    
.fi
.LP
Resumes all suspended processes using a module \fIMod\fR\&.

.nf
{code_change, [{Mod, Extra}]}
{code_change, Mode, [{Mod, Extra}]}
  Mod = atom()
  Mode = up | down
  Extra = term()
    
.fi
.LP
\fIMode\fR defaults to \fIup\fR and specifies if it is an upgrade or downgrade\&.
.LP
This instruction sends a \fIcode_change\fR system message to all processes using a module \fIMod\fR by calling the function \fIsys:change_code\fR, passing the term \fIExtra\fR as argument\&.

.nf
{stop, [Mod]}
  Mod = atom()
    
.fi
.LP
Stops all processes using a module \fIMod\fR by calling \fIsupervisor:terminate_child/2\fR\&. The instruction is useful when the simplest way to change code is to stop and restart the processes which run the code\&.

.nf
{start, [Mod]}
  Mod = atom()
    
.fi
.LP
Starts all stopped processes using a module \fIMod\fR by calling \fIsupervisor:restart_child/2\fR\&.

.nf
{sync_nodes, Id, [Node]}
{sync_nodes, Id, {M, F, A}}
  Id = term()
  Node = node()
  M = F = atom()
  A = [term()]
    
.fi
.LP
\fIapply(M, F, A)\fR must return a list of nodes\&.
.LP
The instruction synchronizes the release installation with other nodes\&. Each \fINode\fR must evaluate this command, with the same \fIId\fR\&. The local node waits for all other nodes to evaluate the instruction before execution continues\&. In case a node goes down, it is considered to be an unrecoverable error, and the local node is restarted from the old release\&. There is no timeout for this instruction, which means that it may hang forever\&.

.nf
{apply, {M, F, A}}
  M = F = atom()
  A = [term()]
    
.fi
.LP
Evaluates \fIapply(M, F, A)\fR\&. If the instruction appears before the \fIpoint_of_no_return\fR instruction, a failure is caught\&. \fIrelease_handler:install_release/1\fR then returns \fI{error, {\&'EXIT\&', Reason}}\fR, unless \fI{error, Error}\fR is thrown or returned\&. Then it returns \fI{error, Error}\fR\&.
.LP
If the instruction appears after the \fIpoint_of_no_return\fR instruction, and the function call fails, the system is restarted\&.

.nf
restart_new_emulator
    
.fi
.LP
Shuts down the current emulator and starts a ne one\&. All processes are terminated gracefully\&. The new release must still be made permanent when the new emulator is up and running\&. Otherwise, the old emulator is started in case of a emulator restart\&. This instruction should be used when a new emulator is introduced, or if a complete reboot of the system should be done\&.
.SH SEE ALSO
.LP
relup(4), release_handler(3), supervisor(3), systools(3)
