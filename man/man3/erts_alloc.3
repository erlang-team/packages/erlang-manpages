.TH erts_alloc 3 "erts  5.7.3" "Ericsson AB" "C LIBRARY FUNCTIONS"
.SH NAME
erts_alloc \- An Erlang Run-Time System internal memory allocator library\&.
.SH DESCRIPTION
.LP
\fIerts_alloc\fR is an Erlang Run-Time System internal memory allocator library\&. \fIerts_alloc\fR provides the Erlang Run-Time System with a number of memory allocators\&.

.SH ALLOCATORS
.LP
Currently the following allocators are present:
.RS 2
.TP 4
.B
\fItemp_alloc\fR:
Allocator used for temporary allocations\&.
.TP 4
.B
\fIeheap_alloc\fR:
Allocator used for Erlang heap data, such as Erlang process heaps\&.
.TP 4
.B
\fIbinary_alloc\fR:
Allocator used for Erlang binary data\&.
.TP 4
.B
\fIets_alloc\fR:
Allocator used for ETS data\&.
.TP 4
.B
\fIdriver_alloc\fR:
Allocator used for driver data\&.
.TP 4
.B
\fIsl_alloc\fR:
Allocator used for memory blocks that are expected to be short-lived\&.
.TP 4
.B
\fIll_alloc\fR:
Allocator used for memory blocks that are expected to be long-lived, for example Erlang code\&.
.TP 4
.B
\fIfix_alloc\fR:
A very fast allocator used for some fix-sized data\&. \fIfix_alloc\fR manages a set of memory pools from which memory blocks are handed out\&. \fIfix_alloc\fR allocates memory pools from \fIll_alloc\fR\&. Memory pools that have been allocated are never deallocated\&.
.TP 4
.B
\fIstd_alloc\fR:
Allocator used for most memory blocks not allocated via any of the other allocators described above\&.
.TP 4
.B
\fIsys_alloc\fR:
This is normally the default \fImalloc\fR implementation used on the specific OS\&.
.TP 4
.B
\fImseg_alloc\fR:
A memory segment allocator\&. \fImseg_alloc\fR is used by other allocators for allocating memory segments and is currently only available on systems that have the \fImmap\fR system call\&. Memory segments that are deallocated are kept for a while in a segment cache before they are destroyed\&. When segments are allocated, cached segments are used if possible instead of creating new segments\&. This in order to reduce the number of system calls made\&.
.RE
.LP
\fIsys_alloc\fR and \fIfix_alloc\fR are always enabled and cannot be disabled\&. \fImseg_alloc\fR is always enabled if it is available and an allocator that uses it is enabled\&. All other allocators can be enabled or disabled\&. By default all allocators are enabled\&. When an allocator is disabled, \fIsys_alloc\fR is used instead of the disabled allocator\&.
.LP
The main idea with the \fIerts_alloc\fR library is to separate memory blocks that are used differently into different memory areas, and by this achieving less memory fragmentation\&. By putting less effort in finding a good fit for memory blocks that are frequently allocated than for those less frequently allocated, a performance gain can be achieved\&.
.SH THE ALLOC_UTIL FRAMEWORK
.LP
Internally a framework called \fIalloc_util\fR is used for implementing allocators\&. \fIsys_alloc\fR, \fIfix_alloc\fR, and \fImseg_alloc\fR do not use this framework; hence, the following does \fInot\fR apply to them\&.
.LP
An allocator manages multiple areas, called carriers, in which memory blocks are placed\&. A carrier is either placed in a separate memory segment (allocated via \fImseg_alloc\fR) or in the heap segment (allocated via \fIsys_alloc\fR)\&. Multiblock carriers are used for storage of several blocks\&. Singleblock carriers are used for storage of one block\&. Blocks that are larger than the value of the singleblock carrier threshold (sbct) parameter are placed in singleblock carriers\&. Blocks smaller than the value of the \fIsbct\fR parameter are placed in multiblock carriers\&. Normally an allocator creates a "main multiblock carrier"\&. Main multiblock carriers are never deallocated\&. The size of the main multiblock carrier is determined by the value of the mmbcs parameter\&.
.LP
 Sizes of multiblock carriers allocated via \fImseg_alloc\fR are decided based on the values of the largest multiblock carrier size (lmbcs), the smallest multiblock carrier size (smbcs), and the multiblock carrier growth stages (mbcgs) parameters\&. If \fInc\fR is the current number of multiblock carriers (the main multiblock carrier excluded) managed by an allocator, the size of the next \fImseg_alloc\fR multiblock carrier allocated by this allocator will roughly be \fIsmbcs+nc*(lmbcs-smbcs)/mbcgs\fR when \fInc <= mbcgs\fR, and \fIlmbcs\fR when \fInc > mbcgs\fR\&. If the value of the \fIsbct\fR parameter should be larger than the value of the \fIlmbcs\fR parameter, the allocator may have to create multiblock carriers that are larger than the value of the \fIlmbcs\fR parameter, though\&. Singleblock carriers allocated via \fImseg_alloc\fR are sized to whole pages\&.
.LP
Sizes of carriers allocated via \fIsys_alloc\fR are decided based on the value of the \fIsys_alloc\fR carrier size (ycs) parameter\&. The size of a carrier is the least number of multiples of the value of the \fIycs\fR parameter that satisfies the request\&.
.LP
Coalescing of free blocks are always performed immediately\&. Boundary tags (headers and footers) in free blocks are used which makes the time complexity for coalescing constant\&.
.LP
 The memory allocation strategy used for multiblock carriers by an allocator is configurable via the as parameter\&. Currently the following strategies are available:
.RS 2
.TP 4
.B
Best fit:
Strategy: Find the smallest block that satisfies the requested block size\&.
.RS 4
.LP

.LP
Implementation: A balanced binary search tree is used\&. The time complexity is proportional to log N, where N is the number of sizes of free blocks\&.
.RE
.TP 4
.B
Address order best fit:
Strategy: Find the smallest block that satisfies the requested block size\&. If multiple blocks are found, choose the one with the lowest address\&.
.RS 4
.LP

.LP
Implementation: A balanced binary search tree is used\&. The time complexity is proportional to log N, where N is the number of free blocks\&.
.RE
.TP 4
.B
Good fit:
Strategy: Try to find the best fit, but settle for the best fit found during a limited search\&.
.RS 4
.LP

.LP
Implementation: The implementation uses segregated free lists with a maximum block search depth (in each list) in order to find a good fit fast\&. When the maximum block search depth is small (by default 3) this implementation has a time complexity that is constant\&. The maximum block search depth is configurable via the mbsd parameter\&.
.RE
.TP 4
.B
A fit:
Strategy: Do not search for a fit, inspect only one free block to see if it satisfies the request\&. This strategy is only intended to be used for temporary allocations\&.
.RS 4
.LP

.LP
Implementation: Inspect the first block in a free-list\&. If it satisfies the request, it is used; otherwise, a new carrier is created\&. The implementation has a time complexity that is constant\&.
.LP

.LP
As of erts version 5\&.6\&.1 the emulator will refuse to use this strategy on other allocators than \fItemp_alloc\fR\&. This since it will only cause problems for other allocators\&.
.RE
.RE
.SH SYSTEM FLAGS EFFECTING ERTS_ALLOC
.SS Warning:
.LP
Only use these flags if you are absolutely sure what you are doing\&. Unsuitable settings may cause serious performance degradation and even a system crash at any time during operation\&.

.LP
Memory allocator system flags have the following syntax: \fI+M<S><P> <V>\fR where \fI<S>\fR is a letter identifying a subsystem, \fI<P>\fR is a parameter, and \fI<V>\fR is the value to use\&. The flags can be passed to the Erlang emulator (erl) as command line arguments\&.
.LP
System flags effecting specific allocators have an upper-case letter as \fI<S>\fR\&. The following letters are used for the currently present allocators:
.RS 2
.TP 2
*
\fIB: binary_alloc\fR
.TP 2
*
\fID: std_alloc\fR
.TP 2
*
\fIE: ets_alloc\fR
.TP 2
*
\fIF: fix_alloc\fR
.TP 2
*
\fIH: eheap_alloc\fR
.TP 2
*
\fIL: ll_alloc\fR
.TP 2
*
\fIM: mseg_alloc\fR
.TP 2
*
\fIR: driver_alloc\fR
.TP 2
*
\fIS: sl_alloc\fR
.TP 2
*
\fIT: temp_alloc\fR
.TP 2
*
\fIY: sys_alloc\fR
.RE
.LP
The following flags are available for configuration of \fImseg_alloc\fR:
.RS 2
.TP 4
.B
\fI+MMamcbf <size>\fR:
 Absolute max cache bad fit (in kilobytes)\&. A segment in the memory segment cache is not reused if its size exceeds the requested size with more than the value of this parameter\&. Default value is 4096\&.
.TP 4
.B
\fI+MMrmcbf <ratio>\fR:
 Relative max cache bad fit (in percent)\&. A segment in the memory segment cache is not reused if its size exceeds the requested size with more than relative max cache bad fit percent of the requested size\&. Default value is 20\&.
.TP 4
.B
\fI+MMmcs <amount>\fR:
 Max cached segments\&. The maximum number of memory segments stored in the memory segment cache\&. Valid range is 0-30\&. Default value is 5\&.
.TP 4
.B
\fI+MMcci <time>\fR:
 Cache check interval (in milliseconds)\&. The memory segment cache is checked for segments to destroy at an interval determined by this parameter\&. Default value is 1000\&.
.RE
.LP
The following flags are available for configuration of \fIfix_alloc\fR:
.RS 2
.TP 4
.B
\fI+MFe true\fR:
 Enable \fIfix_alloc\fR\&. Note: \fIfix_alloc\fR cannot be disabled\&.
.RE
.LP
The following flags are available for configuration of \fIsys_alloc\fR:
.RS 2
.TP 4
.B
\fI+MYe true\fR:
 Enable \fIsys_alloc\fR\&. Note: \fIsys_alloc\fR cannot be disabled\&.
.TP 4
.B
\fI+MYm libc\fR:
\fImalloc\fR library to use\&. Currently only \fIlibc\fR is available\&. \fIlibc\fR enables the standard \fIlibc\fR malloc implementation\&. By default \fIlibc\fR is used\&.
.TP 4
.B
\fI+MYtt <size>\fR:
 Trim threshold size (in kilobytes)\&. This is the maximum amount of free memory at the top of the heap (allocated by \fIsbrk\fR) that will be kept by \fImalloc\fR (not released to the operating system)\&. When the amount of free memory at the top of the heap exceeds the trim threshold, \fImalloc\fR will release it (by calling \fIsbrk\fR)\&. Trim threshold is given in kilobytes\&. Default trim threshold is 128\&. \fINote:\fR This flag will only have any effect when the emulator has been linked with the GNU C library, and uses its \fImalloc\fR implementation\&.
.TP 4
.B
\fI+MYtp <size>\fR:
 Top pad size (in kilobytes)\&. This is the amount of extra memory that will be allocated by \fImalloc\fR when \fIsbrk\fR is called to get more memory from the operating system\&. Default top pad size is 0\&. \fINote:\fR This flag will only have any effect when the emulator has been linked with the GNU C library, and uses its \fImalloc\fR implementation\&.
.RE
.LP
The following flags are available for configuration of allocators based on \fIalloc_util\fR\&. If \fIu\fR is used as subsystem identifier (i\&.e\&., \fI<S> = u\fR) all allocators based on \fIalloc_util\fR will be effected\&. If \fIB\fR, \fID\fR, \fIE\fR, \fIH\fR, \fIL\fR, \fIR\fR, \fIS\fR, or \fIT\fR is used as subsystem identifier, only the specific allocator identified will be effected:
.RS 2
.TP 4
.B
\fI+M<S>as bf|aobf|gf|af\fR:
 Allocation strategy\&. Valid strategies are \fIbf\fR (best fit), \fIaobf\fR (address order best fit), \fIgf\fR (good fit), and \fIaf\fR (a fit)\&. See the description of allocation strategies in "the \fIalloc_util\fR framework" section\&.
.TP 4
.B
\fI+M<S>asbcst <size>\fR:
 Absolute singleblock carrier shrink threshold (in kilobytes)\&. When a block located in an \fImseg_alloc\fR singleblock carrier is shrunk, the carrier will be left unchanged if the amount of unused memory is less than this threshold; otherwise, the carrier will be shrunk\&. See also rsbcst\&.
.TP 4
.B
\fI+M<S>e true|false\fR:
 Enable allocator \fI<S>\fR\&.
.TP 4
.B
\fI+M<S>lmbcs <size>\fR:
 Largest (\fImseg_alloc\fR) multiblock carrier size (in kilobytes)\&. See the description on how sizes for mseg_alloc multiblock carriers are decided in "the \fIalloc_util\fR framework" section\&.
.TP 4
.B
\fI+M<S>mbcgs <ratio>\fR:
 (\fImseg_alloc\fR) multiblock carrier growth stages\&. See the description on how sizes for mseg_alloc multiblock carriers are decided in "the \fIalloc_util\fR framework" section\&.
.TP 4
.B
\fI+M<S>mbsd <depth>\fR:
 Max block search depth\&. This flag has effect only if the good fit strategy has been selected for allocator \fI<S>\fR\&. When the good fit strategy is used, free blocks are placed in segregated free-lists\&. Each free list contains blocks of sizes in a specific range\&. The max block search depth sets a limit on the maximum number of blocks to inspect in a free list during a search for suitable block satisfying the request\&.
.TP 4
.B
\fI+M<S>mmbcs <size>\fR:
 Main multiblock carrier size\&. Sets the size of the main multiblock carrier for allocator \fI<S>\fR\&. The main multiblock carrier is allocated via \fIsys_alloc\fR and is never deallocated\&.
.TP 4
.B
\fI+M<S>mmmbc <amount>\fR:
 Max \fImseg_alloc\fR multiblock carriers\&. Maximum number of multiblock carriers allocated via \fImseg_alloc\fR by allocator \fI<S>\fR\&. When this limit has been reached, new multiblock carriers will be allocated via \fIsys_alloc\fR\&.
.TP 4
.B
\fI+M<S>mmsbc <amount>\fR:
 Max \fImseg_alloc\fR singleblock carriers\&. Maximum number of singleblock carriers allocated via \fImseg_alloc\fR by allocator \fI<S>\fR\&. When this limit has been reached, new singleblock carriers will be allocated via \fIsys_alloc\fR\&.
.TP 4
.B
\fI+M<S>ramv <bool>\fR:
 Realloc always moves\&. When enabled, reallocate operations will more or less be translated into an allocate, copy, free sequence\&. This often reduce memory fragmentation, but costs performance\&.
.TP 4
.B
\fI+M<S>rmbcmt <ratio>\fR:
 Relative multiblock carrier move threshold (in percent)\&. When a block located in a multiblock carrier is shrunk, the block will be moved if the ratio of the size of the returned memory compared to the previous size is more than this threshold; otherwise, the block will be shrunk at current location\&.
.TP 4
.B
\fI+M<S>rsbcmt <ratio>\fR:
 Relative singleblock carrier move threshold (in percent)\&. When a block located in a singleblock carrier is shrunk to a size smaller than the value of the sbct parameter, the block will be left unchanged in the singleblock carrier if the ratio of unused memory is less than this threshold; otherwise, it will be moved into a multiblock carrier\&.
.TP 4
.B
\fI+M<S>rsbcst <ratio>\fR:
 Relative singleblock carrier shrink threshold (in percent)\&. When a block located in an \fImseg_alloc\fR singleblock carrier is shrunk, the carrier will be left unchanged if the ratio of unused memory is less than this threshold; otherwise, the carrier will be shrunk\&. See also asbcst\&.
.TP 4
.B
\fI+M<S>sbct <size>\fR:
 Singleblock carrier threshold\&. Blocks larger than this threshold will be placed in singleblock carriers\&. Blocks smaller than this threshold will be placed in multiblock carriers\&.
.TP 4
.B
\fI+M<S>smbcs <size>\fR:
 Smallest (\fImseg_alloc\fR) multiblock carrier size (in kilobytes)\&. See the description on how sizes for mseg_alloc multiblock carriers are decided in "the \fIalloc_util\fR framework" section\&.
.TP 4
.B
\fI+M<S>t true|false|<amount>\fR:
 Multiple, thread specific instances of the allocator\&. This option will only have any effect on the runtime system with SMP support\&. Default behaviour on the runtime system with SMP support (\fIN\fR equals the number of scheduler threads): 
.RS 4
.RS 2
.TP 4
.B
\fItemp_alloc\fR:
\fIN + 1\fR instances\&.
.TP 4
.B
\fIll_alloc\fR:
\fI1\fR instance\&.
.TP 4
.B
Other allocators:
\fIN\fR instances when \fIN\fR is less than or equal to \fI16\fR\&. \fI16\fR instances when \fIN\fR is greater than \fI16\fR\&.
.RE
.LP
\fItemp_alloc\fR will always use \fIN + 1\fR instances when this option has been enabled regardless of the amount passed\&. Other allocators will use the same amount of instances as the amount passed as long as it isn\&'t greater than \fIN\fR\&.
.RE
.RE
.LP
Currently the following flags are available for configuration of \fIalloc_util\fR, i\&.e\&. all allocators based on \fIalloc_util\fR will be effected:
.RS 2
.TP 4
.B
\fI+Muycs <size>\fR:
\fIsys_alloc\fR carrier size\&. Carriers allocated via \fIsys_alloc\fR will be allocated in sizes which are multiples of the \fIsys_alloc\fR carrier size\&. This is not true for main multiblock carriers and carriers allocated during a memory shortage, though\&.
.TP 4
.B
\fI+Mummc <amount>\fR:
 Max \fImseg_alloc\fR carriers\&. Maximum number of carriers placed in separate memory segments\&. When this limit has been reached, new carriers will be placed in memory retrieved from \fIsys_alloc\fR\&.
.RE
.LP
Instrumentation flags:
.RS 2
.TP 4
.B
\fI+Mim true|false\fR:
 A map over current allocations is kept by the emulator\&. The allocation map can be retrieved via the \fIinstrument\fR module\&. \fI+Mim true\fR implies \fI+Mis true\fR\&. \fI+Mim true\fR is the same as -instr\&.
.TP 4
.B
\fI+Mis true|false\fR:
 Status over allocated memory is kept by the emulator\&. The allocation status can be retrieved via the \fIinstrument\fR module\&.
.TP 4
.B
\fI+Mit X\fR:
 Reserved for future use\&. Do \fInot\fR use this flag\&.
.RE
.SS Note:
.LP
When instrumentation of the emulator is enabled, the emulator uses more memory and runs slower\&.

.LP
Other flags:
.RS 2
.TP 4
.B
\fI+Mea min|max|r9c|r10b|r11b|config\fR:
 
.RS 4
.RS 2
.TP 4
.B
\fImin\fR:
Disables all allocators that can be disabled\&.
.TP 4
.B
\fImax\fR:
Enables all allocators (currently default)\&.
.TP 4
.B
\fIr9c|r10b|r11b\fR:
Configures all allocators as they were configured in respective OTP release\&. These will eventually be removed\&.
.TP 4
.B
\fIconfig\fR:
Disables features that cannot be enabled while creating an allocator configuration with erts_alloc_config(3)\&. Note, this option should only be used while running \fIerts_alloc_config\fR, \fInot\fR when using the created configuration\&.
.RE
.RE
.RE
.LP
Only some default values have been presented here\&. erlang:system_info(allocator), and erlang:system_info({allocator, Alloc}) can be used in order to obtain currently used settings and current status of the allocators\&.
.SS Note:
.LP
Most of these flags are highly implementation dependent, and they may be changed or removed without prior notice\&.
.LP
\fIerts_alloc\fR is not obliged to strictly use the settings that have been passed to it (it may even ignore them)\&.

.LP
erts_alloc_config(3) is a tool that can be used to aid creation of an \fIerts_alloc\fR configuration that is suitable for a limited number of runtime scenarios\&.
.SH SEE ALSO
.LP
erts_alloc_config(3), erl(1), instrument(3), erlang(3)
