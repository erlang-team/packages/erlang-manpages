.TH erl_tar 3 "stdlib  1.16.3" "Ericsson AB" "ERLANG MODULE DEFINITION"
.SH MODULE
erl_tar \- Unix \&'tar\&' utility for reading and writing tar archives
.SH DESCRIPTION
.LP
The \fIerl_tar\fR module archives and extract files to and from a tar file\&. The tar file format is the POSIX extended tar file format specified in IEEE Std 1003\&.1 and ISO/IEC 9945-1\&. That is the same format as used by \fItar\fR program on Solaris, but is not the same as used by the GNU tar program\&.
.LP
By convention, the name of a tar file should end in "\fI\&.tar\fR"\&. To abide to the convention, you\&'ll need to add "\fI\&.tar\fR" yourself to the name\&.
.LP
Tar files can be created in one operation using the create/2 or create/3 function\&.
.LP
Alternatively, for more control, the open, add/3,4, and close/1 functions can be used\&.
.LP
To extract all files from a tar file, use the extract/1 function\&. To extract only some files or to be able to specify some more options, use the extract/2 function\&.
.LP
To return a list of the files in a tar file, use either the table/1 or table/2 function\&. To print a list of files to the Erlang shell, use either the t/1 or tt/1 function\&.
.LP
To convert an error term returned from one of the functions above to a readable message, use the format_error/1 function\&.

.SH LIMITATIONS
.LP
For maximum compatibility, it is safe to archive files with names up to 100 characters in length\&. Such tar files can generally be extracted by any \fItar\fR program\&.
.LP
If filenames exceed 100 characters in length, the resulting tar file can only be correctly extracted by a POSIX-compatible \fItar\fR program (such as Solaris \fItar\fR), not by GNU tar\&.
.LP
File have longer names than 256 bytes cannot be stored at all\&.
.LP
The filename of the file a symbolic link points is always limited to 100 characters\&.
.SH EXPORTS
.LP
.B
add(TarDescriptor, Filename, Options) -> RetValue
.br
.RS
.TP
Types
TarDescriptor = term()
.br
Filename = filename()
.br
Options = [Option]
.br
Option = dereference|verbose
.br
RetValue = ok|{error, {Filename, Reason}}
.br
Reason = term()
.br
.RE
.RS
.LP
The \fIadd/3\fR function adds a file to a tar file that has been opened for writing by open/1\&.
.RS 2
.TP 4
.B
\fIdereference\fR:
By default, symbolic links will be stored as symbolic links in the tar file\&. Use the \fIdereference\fR option to override the default and store the file that the symbolic link points to into the tar file\&.
.TP 4
.B
\fIverbose\fR:
Print an informational message about the file being added\&.
.RE
.RE
.LP
.B
add(TarDescriptor, FilenameOrBin, NameInArchive, Options) -> RetValue 
.br
.RS
.TP
Types
TarDescriptor = term()
.br
FilenameOrBin = Filename()|binary()
.br
Filename = filename()()
.br
NameInArchive = filename()
.br
Options = [Option]
.br
Option = dereference|verbose
.br
RetValue = ok|{error, {Filename, Reason}}
.br
Reason = term()
.br
.RE
.RS
.LP
The \fIadd/4\fR function adds a file to a tar file that has been opened for writing by open/1\&. It accepts the same options as add/3\&.
.LP
\fINameInArchive\fR is the name under which the file will be stored in the tar file\&. That is the name that the file will get when it will be extracted from the tar file\&.
.RE
.LP
.B
close(TarDescriptor)
.br
.RS
.TP
Types
TarDescriptor = term()
.br
.RE
.RS
.LP
The \fIclose/1\fR function closes a tar file opened by open/1\&.
.RE
.LP
.B
create(Name, FileList) ->RetValue 
.br
.RS
.TP
Types
Name = filename()
.br
FileList = [Filename|{NameInArchive, binary()}, {NameInArchive, Filename}]
.br
Filename = filename()
.br
NameInArchive = filename()
.br
RetValue = ok|{error, {Name, Reason}} <V>Reason = term()
.br
.RE
.RS
.LP
The \fIcreate/2\fR function creates a tar file and archives the files whose names are given in \fIFileList\fR into it\&. The files may either be read from disk or given as binaries\&.
.RE
.LP
.B
create(Name, FileList, OptionList)
.br
.RS
.TP
Types
Name = filename()
.br
FileList = [Filename|{NameInArchive, binary()}, {NameInArchive, Filename}]
.br
Filename = filename()
.br
NameInArchive = filename()
.br
OptionList = [Option]
.br
Option = compressed|cooked|dereference|verbose
.br
RetValue = ok|{error, {Name, Reason}} <V>Reason = term()
.br
.RE
.RS
.LP
The \fIcreate/3\fR function creates a tar file and archives the files whose names are given in \fIFileList\fR into it\&. The files may either be read from disk or given as binaries\&.
.LP
The options in \fIOptionList\fR modify the defaults as follows\&. 
.RS 2
.TP 4
.B
\fIcompressed\fR:
The entire tar file will be compressed, as if it has been run through the \fIgzip\fR program\&. To abide to the convention that a compressed tar file should end in "\fI\&.tar\&.gz\fR" or "\fI\&.tgz\fR", you\&'ll need to add the appropriate extension yourself\&.
.TP 4
.B
\fIcooked\fR:
By default, the \fIopen/2\fR function will open the tar file in \fIraw\fR mode, which is faster but does not allow a remote (erlang) file server to be used\&. Adding \fIcooked\fR to the mode list will override the default and open the tar file without the \fIraw\fR option\&.
.TP 4
.B
\fIdereference\fR:
By default, symbolic links will be stored as symbolic links in the tar file\&. Use the \fIdereference\fR option to override the default and store the file that the symbolic link points to into the tar file\&.
.TP 4
.B
\fIverbose\fR:
Print an informational message about each file being added\&.
.RE
.RE
.LP
.B
extract(Name) -> RetValue
.br
.RS
.TP
Types
Name = filename()
.br
RetValue = ok|{error, {Name, Reason}}
.br
Reason = term()
.br
.RE
.RS
.LP
The \fIextract/1\fR function extracts all files from a tar archive\&.
.LP
If the \fIName\fR argument is given as "\fI{binary, Binary}\fR", the contents of the binary is assumed to be a tar archive\&. 
.LP
If the \fIName\fR argument is given as "\fI{file, Fd}\fR", \fIFd\fR is assumed to be a file descriptor returned from the \fIfile:open/2\fR function\&. 
.LP
Otherwise, \fIName\fR should be a filename\&.
.RE
.LP
.B
extract(Name, OptionList)
.br
.RS
.TP
Types
Name = filename() | {binary, Binary} | {file, Fd} 
.br
Binary = binary()
.br
Fd = file_descriptor()
.br
OptionList = [Option]
.br
Option = {cwd, Cwd}|{files, FileList}|keep_old_files|verbose|memory
.br
Cwd = [dirname()]
.br
FileList = [filename()]
.br
RetValue = ok|MemoryRetValue|{error, {Name, Reason}}
.br
MemoryRetValue = {ok, [{NameInArchive, binary()}]}
.br
NameInArchive = filename()
.br
Reason = term()
.br
.RE
.RS
.LP
The \fIextract/2\fR function extracts files from a tar archive\&.
.LP
If the \fIName\fR argument is given as "\fI{binary, Binary}\fR", the contents of the binary is assumed to be a tar archive\&. 
.LP
If the \fIName\fR argument is given as "\fI{file, Fd}\fR", \fIFd\fR is assumed to be a file descriptor returned from the \fIfile:open/2\fR function\&. 
.LP
Otherwise, \fIName\fR should be a filename\&. 
.LP
The following options modify the defaults for the extraction as follows\&.
.RS 2
.TP 4
.B
\fI{cwd, Cwd}\fR:
Files with relative filenames will by default be extracted to the current working directory\&. Given the \fI{cwd, Cwd}\fR option, the \fIextract/2\fR function will extract into the directory \fICwd\fR instead of to the current working directory\&.
.TP 4
.B
\fI{files, FileList}\fR:
By default, all files will be extracted from the tar file\&. Given the \fI{files, Files}\fR option, the \fIextract/2\fR function will only extract the files whose names are included in \fIFileList\fR\&.
.TP 4
.B
\fIcompressed\fR:
Given the \fIcompressed\fR option, the \fIextract/2\fR function will uncompress the file while extracting If the tar file is not actually compressed, the \fIcompressed\fR will effectively be ignored\&.
.TP 4
.B
\fIcooked\fR:
By default, the \fIopen/2\fR function will open the tar file in \fIraw\fR mode, which is faster but does not allow a remote (erlang) file server to be used\&. Adding \fIcooked\fR to the mode list will override the default and open the tar file without the \fIraw\fR option\&.
.TP 4
.B
\fImemory\fR:
Instead of extracting to a directory, the memory option will give the result as a list of tuples {Filename, Binary}, where Binary is a binary containing the extracted data of the file named Filename in the tar file\&.
.TP 4
.B
\fIkeep_old_files\fR:
By default, all existing files with the same name as file in the tar file will be overwritten Given the \fIkeep_old_files\fR option, the \fIextract/2\fR function will not overwrite any existing files\&.
.TP 4
.B
\fIverbose\fR:
Print an informational message as each file is being extracted\&.
.RE
.RE
.LP
.B
format_error(Reason) -> string()
.br
.RS
.TP
Types
Reason = term()
.br
.RE
.RS
.LP
The \fIformat_error/1\fR converts an error reason term to a human-readable error message string\&.
.RE
.LP
.B
open(Name, OpenModeList) -> RetValue
.br
.RS
.TP
Types
Name = filename()
.br
OpenModeList = [OpenMode]
.br
Mode = write|compressed|cooked
.br
RetValue = {ok, TarDescriptor}|{error, {Name, Reason}}
.br
TarDescriptor = term()
.br
Reason = term()
.br
.RE
.RS
.LP
The \fIopen/2\fR function creates a tar file for writing\&. (Any existing file with the same name will be truncated\&.)
.LP
By convention, the name of a tar file should end in "\fI\&.tar\fR"\&. To abide to the convention, you\&'ll need to add "\fI\&.tar\fR" yourself to the name\&.
.LP
Except for the \fIwrite\fR atom the following atoms may be added to \fIOpenModeList\fR:
.RS 2
.TP 4
.B
\fIcompressed\fR:
The entire tar file will be compressed, as if it has been run through the \fIgzip\fR program\&. To abide to the convention that a compressed tar file should end in "\fI\&.tar\&.gz\fR" or "\fI\&.tgz\fR", you\&'ll need to add the appropriate extension yourself\&.
.TP 4
.B
\fIcooked\fR:
By default, the \fIopen/2\fR function will open the tar file in \fIraw\fR mode, which is faster but does not allow a remote (erlang) file server to be used\&. Adding \fIcooked\fR to the mode list will override the default and open the tar file without the \fIraw\fR option\&.
.RE
.LP
Use the add/3,4 functions to add one file at the time into an opened tar file\&. When you are finished adding files, use the close function to close the tar file\&.
.SS Warning:
.LP
The \fITarDescriptor\fR term is not a file descriptor\&. You should not rely on the specific contents of the \fITarDescriptor\fR term, as it may change in future versions as more features are added to the \fIerl_tar\fR module\&.
.LP


.RE
.LP
.B
table(Name) -> RetValue
.br
.RS
.TP
Types
Name = filename()
.br
RetValue = {ok, [string()]}|{error, {Name, Reason}}
.br
Reason = term()
.br
.RE
.RS
.LP
The \fItable/1\fR function retrieves the names of all files in the tar file \fIName\fR\&.
.RE
.LP
.B
table(Name, Options)
.br
.RS
.TP
Types
Name = filename()
.br
.RE
.RS
.LP
The \fItable/2\fR function retrieves the names of all files in the tar file \fIName\fR\&.
.RE
.LP
.B
t(Name)
.br
.RS
.TP
Types
Name = filename()
.br
.RE
.RS
.LP
The \fIt/1\fR function prints the names of all files in the tar file \fIName\fR to the Erlang shell\&. (Similar to "\fItar t\fR"\&.)
.RE
.LP
.B
tt(Name)
.br
.RS
.TP
Types
Name = filename()
.br
.RE
.RS
.LP
The \fItt/1\fR function prints names and information about all files in the tar file \fIName\fR to the Erlang shell\&. (Similar to "\fItar tv\fR"\&.)
.RE
