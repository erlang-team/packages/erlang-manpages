.TH supervisor 3 "stdlib  1.16.3" "Ericsson AB" "ERLANG MODULE DEFINITION"
.SH MODULE
supervisor \- Generic Supervisor Behaviour
.SH DESCRIPTION
.LP
A behaviour module for implementing a supervisor, a process which supervises other processes called child processes\&. A child process can either be another supervisor or a worker process\&. Worker processes are normally implemented using one of the \fIgen_event\fR, \fIgen_fsm\fR, or \fIgen_server\fR behaviours\&. A supervisor implemented using this module will have a standard set of interface functions and include functionality for tracing and error reporting\&. Supervisors are used to build an hierarchical process structure called a supervision tree, a nice way to structure a fault tolerant application\&. Refer to \fIOTP Design Principles\fR for more information\&.
.LP
A supervisor assumes the definition of which child processes to supervise to be located in a callback module exporting a pre-defined set of functions\&.
.LP
Unless otherwise stated, all functions in this module will fail if the specified supervisor does not exist or if bad arguments are given\&.

.SH SUPERVISION PRINCIPLES
.LP
The supervisor is responsible for starting, stopping and monitoring its child processes\&. The basic idea of a supervisor is that it should keep its child processes alive by restarting them when necessary\&.
.LP
The children of a supervisor is defined as a list of \fIchild specifications\fR\&. When the supervisor is started, the child processes are started in order from left to right according to this list\&. When the supervisor terminates, it first terminates its child processes in reversed start order, from right to left\&.
.LP

.LP
A supervisor can have one of the following \fIrestart strategies\fR:
.RS 2
.TP 2
*
\fIone_for_one\fR - if one child process terminates and should be restarted, only that child process is affected\&.
.TP 2
*
\fIone_for_all\fR - if one child process terminates and should be restarted, all other child processes are terminated and then all child processes are restarted\&.
.TP 2
*
\fIrest_for_one\fR - if one child process terminates and should be restarted, the \&'rest\&' of the child processes -- i\&.e\&. the child processes after the terminated child process in the start order -- are terminated\&. Then the terminated child process and all child processes after it are restarted\&.
.TP 2
*
\fIsimple_one_for_one\fR - a simplified \fIone_for_one\fR supervisor, where all child processes are dynamically added instances of the same process type, i\&.e\&. running the same code\&.
.RS 2
.LP

.LP
The functions \fIterminate_child/2\fR, \fIdelete_child/2\fR and \fIrestart_child/2\fR are invalid for \fIsimple_one_for_one\fR supervisors and will return \fI{error, simple_one_for_one}\fR if the specified supervisor uses this restart strategy\&.
.RE
.RE
.LP
To prevent a supervisor from getting into an infinite loop of child process terminations and restarts, a \fImaximum restart frequency\fR is defined using two integer values \fIMaxR\fR and \fIMaxT\fR\&. If more than \fIMaxR\fR restarts occur within \fIMaxT\fR seconds, the supervisor terminates all child processes and then itself\&. 
.LP
This is the type definition of a child specification:

.nf
child_spec() = {Id,StartFunc,Restart,Shutdown,Type,Modules}
 Id = term()
 StartFunc = {M,F,A}
  M = F = atom()
  A = [term()]
 Restart = permanent | transient | temporary
 Shutdown = brutal_kill | int()>=0 | infinity
 Type = worker | supervisor
 Modules = [Module] | dynamic
  Module = atom()
.fi
.RS 2
.TP 2
*
\fIId\fR is a name that is used to identify the child specification internally by the supervisor\&.
.TP 2
*
\fIStartFunc\fR defines the function call used to start the child process\&. It should be a module-function-arguments tuple \fI{M, F, A}\fR used as \fIapply(M, F, A)\fR\&.
.RS 2
.LP

.LP

.br

.LP

.LP
The start function \fImust create and link to\fR the child process, and should return \fI{ok, Child}\fR or \fI{ok, Child, Info}\fR where \fIChild\fR is the pid of the child process and \fIInfo\fR an arbitrary term which is ignored by the supervisor\&.
.LP

.LP

.br

.LP

.LP
The start function can also return \fIignore\fR if the child process for some reason cannot be started, in which case the child specification will be kept by the supervisor but the non-existing child process will be ignored\&.
.LP

.LP

.br

.LP

.LP
If something goes wrong, the function may also return an error tuple \fI{error, Error}\fR\&.
.LP

.LP

.br

.LP

.LP
Note that the \fIstart_link\fR functions of the different behaviour modules fulfill the above requirements\&.
.RE
.TP 2
*
\fIRestart\fR defines when a terminated child process should be restarted\&. A \fIpermanent\fR child process should always be restarted, a \fItemporary\fR child process should never be restarted and a \fItransient\fR child process should be restarted only if it terminates abnormally, i\&.e\&. with another exit reason than \fInormal\fR\&.
.TP 2
*
\fIShutdown\fR defines how a child process should be terminated\&. \fIbrutal_kill\fR means the child process will be unconditionally terminated using \fIexit(Child, kill)\fR\&. An integer timeout value means that the supervisor will tell the child process to terminate by calling \fIexit(Child, shutdown)\fR and then wait for an exit signal with reason \fIshutdown\fR back from the child process\&. If no exit signal is received within the specified time, the child process is unconditionally terminated using \fIexit(Child, kill)\fR\&.
.RS 2
.LP

.LP
If the child process is another supervisor, \fIShutdown\fR should be set to \fIinfinity\fR to give the subtree ample time to shutdown\&.
.LP

.LP
\fIImportant note on simple-one-for-one supervisors:\fR The dynamically created child processes of a simple-one-for-one supervisor are not explicitly killed, regardless of shutdown strategy, but are expected to terminate when the supervisor does (that is, when an exit signal from the parent process is received)\&.
.LP

.LP
Note that all child processes implemented using the standard OTP behavior modules automatically adhere to the shutdown protocol\&.
.RE
.TP 2
*
\fIType\fR specifies if the child process is a supervisor or a worker\&.
.TP 2
*
\fIModules\fR is used by the release handler during code replacement to determine which processes are using a certain module\&. As a rule of thumb \fIModules\fR should be a list with one element \fI[Module]\fR, where \fIModule\fR is the callback module, if the child process is a supervisor, gen_server or gen_fsm\&. If the child process is an event manager (gen_event) with a dynamic set of callback modules, \fIModules\fR should be \fIdynamic\fR\&. See \fIOTP Design Principles\fR for more information about release handling\&.
.TP 2
*
Internally, the supervisor also keeps track of the pid \fIChild\fR of the child process, or \fIundefined\fR if no pid exists\&.
.RE
.SH EXPORTS
.LP
.B
start_link(Module, Args) -> Result
.br
.B
start_link(SupName, Module, Args) -> Result
.br
.RS
.TP
Types
SupName = {local, Name} | {global, Name}
.br
 Name = atom()
.br
Module = atom()
.br
Args = term()
.br
Result = {ok, Pid} | ignore | {error, Error}
.br
 Pid = pid()
.br
 Error = {already_started, Pid}} | shutdown | term()
.br
.RE
.RS
.LP
Creates a supervisor process as part of a supervision tree\&. The function will, among other things, ensure that the supervisor is linked to the calling process (its supervisor)\&.
.LP
The created supervisor process calls \fIModule:init/1\fR to find out about restart strategy, maximum restart frequency and child processes\&. To ensure a synchronized start-up procedure, \fIstart_link/2, 3\fR does not return until \fIModule:init/1\fR has returned and all child processes have been started\&.
.LP
If \fISupName={local, Name}\fR the supervisor is registered locally as \fIName\fR using \fIregister/2\fR\&. If \fISupName={global, Name}\fR the supervisor is registered globally as \fIName\fR using \fIglobal:register_name/2\fR\&. If no name is provided, the supervisor is not registered\&.
.LP
\fIModule\fR is the name of the callback module\&.
.LP
\fIArgs\fR is an arbitrary term which is passed as the argument to \fIModule:init/1\fR\&.
.LP
If the supervisor and its child processes are successfully created (i\&.e\&. if all child process start functions return \fI{ok, Child}\fR, \fI{ok, Child, Info}\fR, or \fIignore\fR) the function returns \fI{ok, Pid}\fR, where \fIPid\fR is the pid of the supervisor\&. If there already exists a process with the specified \fISupName\fR the function returns \fI{error, {already_started, Pid}}\fR, where \fIPid\fR is the pid of that process\&.
.LP
If \fIModule:init/1\fR returns \fIignore\fR, this function returns \fIignore\fR as well and the supervisor terminates with reason \fInormal\fR\&. If \fIModule:init/1\fR fails or returns an incorrect value, this function returns \fI{error, Term}\fR where \fITerm\fR is a term with information about the error, and the supervisor terminates with reason \fITerm\fR\&.
.LP
If any child process start function fails or returns an error tuple or an erroneous value, the function returns \fI{error, shutdown}\fR and the supervisor terminates all started child processes and then itself with reason \fIshutdown\fR\&.
.RE
.LP
.B
start_child(SupRef, ChildSpec) -> Result
.br
.RS
.TP
Types
SupRef = Name | {Name, Node} | {global, Name} | pid()
.br
 Name = Node = atom()
.br
ChildSpec = child_spec() | [term()]
.br
Result = {ok, Child} | {ok, Child, Info} | {error, Error}
.br
 Child = pid() | undefined
.br
 Info = term()
.br
 Error = already_present | {already_started, Child} | term()
.br
.RE
.RS
.LP
Dynamically adds a child specification to the supervisor \fISupRef\fR which starts the corresponding child process\&.
.LP
\fISupRef\fR can be:
.RS 2
.TP 2
*
the pid,
.TP 2
*
\fIName\fR, if the supervisor is locally registered,
.TP 2
*
\fI{Name, Node}\fR, if the supervisor is locally registered at another node, or
.TP 2
*
\fI{global, Name}\fR, if the supervisor is globally registered\&.
.RE
.LP
\fIChildSpec\fR should be a valid child specification (unless the supervisor is a \fIsimple_one_for_one\fR supervisor, see below)\&. The child process will be started by using the start function as defined in the child specification\&.
.LP
If the case of a \fIsimple_one_for_one\fR supervisor, the child specification defined in \fIModule:init/1\fR will be used and \fIChildSpec\fR should instead be an arbitrary list of terms \fIList\fR\&. The child process will then be started by appending \fIList\fR to the existing start function arguments, i\&.e\&. by calling \fIapply(M, F, A++List)\fR where \fI{M, F, A}\fR is the start function defined in the child specification\&.
.LP
If there already exists a child specification with the specified \fIId\fR, \fIChildSpec\fR is discarded and the function returns \fI{error, already_present}\fR or \fI{error, {already_started, Child}}\fR, depending on if the corresponding child process is running or not\&.
.LP
If the child process start function returns \fI{ok, Child}\fR or \fI{ok, Child, Info}\fR, the child specification and pid is added to the supervisor and the function returns the same value\&.
.LP
If the child process start function returns \fIignore\fR, the child specification is added to the supervisor, the pid is set to \fIundefined\fR and the function returns \fI{ok, undefined}\fR\&.
.LP
If the child process start function returns an error tuple or an erroneous value, or if it fails, the child specification is discarded and the function returns \fI{error, Error}\fR where \fIError\fR is a term containing information about the error and child specification\&.
.RE
.LP
.B
terminate_child(SupRef, Id) -> Result
.br
.RS
.TP
Types
SupRef = Name | {Name, Node} | {global, Name} | pid()
.br
 Name = Node = atom()
.br
Id = term()
.br
Result = ok | {error, Error}
.br
 Error = not_found | simple_one_for_one
.br
.RE
.RS
.LP
Tells the supervisor \fISupRef\fR to terminate the child process corresponding to the child specification identified by \fIId\fR\&. The process, if there is one, is terminated but the child specification is kept by the supervisor\&. This means that the child process may be later be restarted by the supervisor\&. The child process can also be restarted explicitly by calling \fIrestart_child/2\fR\&. Use \fIdelete_child/2\fR to remove the child specification\&.
.LP
See \fIstart_child/2\fR for a description of \fISupRef\fR\&.
.LP
If successful, the function returns \fIok\fR\&. If there is no child specification with the specified \fIId\fR, the function returns \fI{error, not_found}\fR\&.
.RE
.LP
.B
delete_child(SupRef, Id) -> Result
.br
.RS
.TP
Types
SupRef = Name | {Name, Node} | {global, Name} | pid()
.br
 Name = Node = atom()
.br
Id = term()
.br
Result = ok | {error, Error}
.br
 Error = running | not_found | simple_one_for_one
.br
.RE
.RS
.LP
Tells the supervisor \fISupRef\fR to delete the child specification identified by \fIId\fR\&. The corresponding child process must not be running, use \fIterminate_child/2\fR to terminate it\&.
.LP
See \fIstart_child/2\fR for a description of \fISupRef\fR\&.
.LP
If successful, the function returns \fIok\fR\&. If the child specification identified by \fIId\fR exists but the corresponding child process is running, the function returns \fI{error, running}\fR\&. If the child specification identified by \fIId\fR does not exist, the function returns \fI{error, not_found}\fR\&.
.RE
.LP
.B
restart_child(SupRef, Id) -> Result
.br
.RS
.TP
Types
SupRef = Name | {Name, Node} | {global, Name} | pid()
.br
 Name = Node = atom()
.br
Id = term()
.br
Result = {ok, Child} | {ok, Child, Info} | {error, Error}
.br
 Child = pid() | undefined
.br
 Error = running | not_found | simple_one_for_one | term()
.br
.RE
.RS
.LP
Tells the supervisor \fISupRef\fR to restart a child process corresponding to the child specification identified by \fIId\fR\&. The child specification must exist and the corresponding child process must not be running\&.
.LP
See \fIstart_child/2\fR for a description of \fISupRef\fR\&.
.LP
If the child specification identified by \fIId\fR does not exist, the function returns \fI{error, not_found}\fR\&. If the child specification exists but the corresponding process is already running, the function returns \fI{error, running}\fR\&.
.LP
If the child process start function returns \fI{ok, Child}\fR or \fI{ok, Child, Info}\fR, the pid is added to the supervisor and the function returns the same value\&.
.LP
If the child process start function returns \fIignore\fR, the pid remains set to \fIundefined\fR and the function returns \fI{ok, undefined}\fR\&.
.LP
If the child process start function returns an error tuple or an erroneous value, or if it fails, the function returns \fI{error, Error}\fR where \fIError\fR is a term containing information about the error\&.
.RE
.LP
.B
which_children(SupRef) -> [{Id,Child,Type,Modules}]
.br
.RS
.TP
Types
SupRef = Name | {Name, Node} | {global, Name} | pid()
.br
 Name = Node = atom()
.br
Id = term() | undefined
.br
Child = pid() | undefined
.br
Type = worker | supervisor
.br
Modules = [Module] | dynamic
.br
 Module = atom()
.br
.RE
.RS
.LP
Returns a list with information about all child specifications and child processes belonging to the supervisor \fISupRef\fR\&.
.LP
See \fIstart_child/2\fR for a description of \fISupRef\fR\&.
.LP
The information given for each child specification/process is:
.RS 2
.TP 2
*
\fIId\fR - as defined in the child specification or \fIundefined\fR in the case of a \fIsimple_one_for_one\fR supervisor\&.
.TP 2
*
\fIChild\fR - the pid of the corresponding child process, or \fIundefined\fR if there is no such process\&.
.TP 2
*
\fIType\fR - as defined in the child specification\&.
.TP 2
*
\fIModules\fR - as defined in the child specification\&.
.RE
.RE
.LP
.B
check_childspecs([ChildSpec]) -> Result
.br
.RS
.TP
Types
ChildSpec = child_spec()
.br
Result = ok | {error, Error}
.br
 Error = term()
.br
.RE
.RS
.LP
This function takes a list of child specification as argument and returns \fIok\fR if all of them are syntactically correct, or \fI{error, Error}\fR otherwise\&.
.RE
.SH CALLBACK FUNCTIONS
.LP
The following functions should be exported from a \fIsupervisor\fR callback module\&.
.SH EXPORTS
.LP
.B
Module:init(Args) -> Result
.br
.RS
.TP
Types
Args = term()
.br
Result = {ok, {{RestartStrategy, MaxR, MaxT}, [ChildSpec]}} | ignore
.br
 RestartStrategy = one_for_all | one_for_one | rest_for_one | simple_one_for_one
.br
 MaxR = MaxT = int()>=0
.br
 ChildSpec = child_spec()
.br
.RE
.RS
.LP
Whenever a supervisor is started using \fIsupervisor:start_link/2, 3\fR, this function is called by the new process to find out about restart strategy, maximum restart frequency and child specifications\&.
.LP
\fIArgs\fR is the \fIArgs\fR argument provided to the start function\&.
.LP
\fIRestartStrategy\fR is the restart strategy and \fIMaxR\fR and \fIMaxT\fR defines the maximum restart frequency of the supervisor\&. \fI[ChildSpec]\fR is a list of valid child specifications defining which child processes the supervisor should start and monitor\&. See the discussion about Supervision Principles above\&.
.LP
Note that when the restart strategy is \fIsimple_one_for_one\fR, the list of child specifications must be a list with one child specification only\&. (The \fIId\fR is ignored)\&. No child process is then started during the initialization phase, but all children are assumed to be started dynamically using \fIsupervisor:start_child/2\fR\&.
.LP
The function may also return \fIignore\fR\&.
.RE
.SH SEE ALSO
.LP
gen_event(3), gen_fsm(3), gen_server(3), sys(3)
