.TH release_handler 3 "sasl  2.1.7" "Ericsson AB" "ERLANG MODULE DEFINITION"
.SH MODULE
release_handler \- Unpacking and Installation of Release Packages
.SH DESCRIPTION
.LP
The \fIrelease handler\fR is a process belonging to the SASL application which is responsible for \fIrelease handling\fR, that is, unpacking, installation, and removal of release packages\&.
.LP
An introduction to release handling and a usage example can be found in Design Principles\&. 
.LP
A \fIrelease package\fR is a compressed tar file containing code for a certain version of a release, created by calling systools:make_tar/1,2\&. The release package should be placed in the \fI$ROOT/releases\fR directory of the previous version of the release where \fI$ROOT\fR is the installation root directory, \fIcode:root_dir()\fR\&. Another \fIreleases\fR directory can be specified using the SASL configuration parameter \fIreleases_dir\fR, or the OS environment variable \fIRELDIR\fR\&. The release handler must have write access to this directory in order to install the new release\&. The persistent state of the release handler is stored there in a file called \fIRELEASES\fR\&.
.LP
A release package should always contain the release resource file \fIName\&.rel\fR and a boot script \fIName\&.boot\fR\&. It may contain a release upgrade file \fIrelup\fR and a system configuration file \fIsys\&.config\fR\&. The \fI\&.rel\fR file contains information about the release: its name, version, and which ERTS and application versions it uses\&. The \fIrelup\fR file contains scripts for how to upgrade to, or downgrade from, this version of the release\&.
.LP
The release package can be \fIunpacked\fR, which extracts the files\&. An unpacked release can be \fIinstalled\fR\&. The currently used version of the release is then upgraded or downgraded to the specified version by evaluating the instructions in \fIrelup\fR\&. An installed release can be made \fIpermanent\fR\&. There can only be one permanent release in the system, and this is the release that is used if the system is restarted\&. An installed release, except the permanent one, can be \fIremoved\fR\&. When a release is removed, all files that belong to that release only are deleted\&.
.LP
Each version of the release has a status\&. The status can be \fIunpacked\fR, \fIcurrent\fR, \fIpermanent\fR, or \fIold\fR\&. There is always one latest release which either has status \fIpermanent\fR (normal case), or \fIcurrent\fR (installed, but not yet made permanent)\&. The following table illustrates the meaning of the status values:

.nf
Status     Action                NextStatus
-------------------------------------------
  -        unpack                unpacked
unpacked   install               current
           remove                  -
current    make_permanent        permanent
           install other         old
           remove                  -
permanent  make other permanent  old
           install               permanent
old        reboot_old            permanent
           install               current
           remove                  -
    
.fi
.LP
The release handler process is a locally registered process on each node\&. When a release is installed in a distributed system, the release handler on each node must be called\&. The release installation may be synchronized between nodes\&. From an operator view, it may be unsatisfactory to specify each node\&. The aim is to install one release package in the system, no matter how many nodes there are\&. If this is the case, it is recommended that software management functions are written which take care of this problem\&. Such a function may have knowledge of the system architecture, so it can contact each individual release handler to install the package\&.
.LP
For release handling to work properly, the runtime system needs to have knowledge about which release it is currently running\&. It must also be able to change (in run-time) which boot script and system configuration file should be used if the system is restarted\&. This is taken care of automatically if Erlang is started as an embedded system\&. Read about this in \fIEmbedded System\fR\&. In this case, the system configuration file \fIsys\&.config\fR is mandatory\&.
.LP
A new release may restart the system\&. Which program to use is specified by the SASL configuration parameter \fIstart_prg\fR which defaults to \fI$ROOT/bin/start\fR\&.
.LP
The emulator restart on Windows NT expects that the system is started using the \fIerlsrv\fR program (as a service)\&. Furthermore the release handler expects that the service is named \fINodeName\fR_\fIRelease\fR, where \fINodeName\fR is the first part of the Erlang nodename (up to, but not including the "@") and \fIRelease\fR is the current release of the application\&. The release handler furthermore expects that a program like \fIstart_erl\&.exe\fR is specified as "machine" to \fIerlsrv\fR\&. During upgrading with restart, a new service will be registered and started\&. The new service will be set to automatic and the old service removed as soon as the new release is made permanent\&.
.LP
The release handler at a node which runs on a diskless machine, or with a read-only file system, must be configured accordingly using the following \fIsasl\fR configuration parameters (see sasl(6) for details):
.RS 2
.TP 4
.B
\fImasters\fR:
This node uses a number of master nodes in order to store and fetch release information\&. All master nodes must be up and running whenever release information is written by this node\&.
.TP 4
.B
\fIclient_directory\fR:
The \fIclient_directory\fR in the directory structure of the master nodes must be specified\&.
.TP 4
.B
\fIstatic_emulator\fR:
This parameter specifies if the Erlang emulator is statically installed at the client node\&. A node with a static emulator cannot dynamically switch to a new emulator because the executable files are statically written into memory\&.
.RE
.LP
It is also possible to use the release handler to unpack and install release packages when not running Erlang as an embedded system, but in this case the user must somehow make sure that correct boot scripts and configuration files are used if the system needs to be restarted\&.
.LP
There are additional functions for using another file structure than the structure defined in OTP\&. These functions can be used to test a release upgrade locally\&.

.SH EXPORTS
.LP
.B
check_install_release(Vsn) -> {ok, OtherVsn, Descr} | {error, Reason}
.br
.RS
.TP
Types
Vsn = OtherVsn = string()
.br
Descr = term()
.br
Reason = term()
.br
.RE
.RS
.LP
Checks if the specified version \fIVsn\fR of the release can be installed\&. The release must not have status \fIcurrent\fR\&. Issues warnings if \fIrelup\fR or \fIsys\&.config\fR are not present\&. If \fIrelup\fR is present, its contents are checked and \fI{error, Reason}\fR is returned if an error is found\&. Also checks that all required applications are present and that all new code can be loaded, or \fI{error, Reason}\fR is returned\&.
.LP
This function evaluates all instructions that occur before the \fIpoint_of_no_return\fR instruction in the release upgrade script\&.
.LP
Returns the same as \fIinstall_release/1\fR\&. \fIDescr\fR defaults to "" if no \fIrelup\fR file is found\&.
.RE
.LP
.B
create_RELEASES(Root, RelDir, RelFile, AppDirs) -> ok | {error, Reason}
.br
.RS
.TP
Types
Root = RelDir = RelFile = string()
.br
AppDirs = [{App, Vsn, Dir}]
.br
 App = atom()
.br
 Vsn = Dir = string()
.br
Reason = term()
.br
.RE
.RS
.LP
Creates an initial RELEASES file to be used by the release handler\&. This file must exist in order to install new releases\&.
.LP
\fIRoot\fR is the root of the installation (\fI$ROOT\fR) as described above\&. \fIRelDir\fR is the the directory where the \fIRELEASES\fR file should be created (normally \fI$ROOT/releases\fR)\&. \fIRelFile\fR is the name of the \fI\&.rel\fR file that describes the initial release, including the extension \fI\&.rel\fR\&.
.LP
\fIAppDirs\fR can be used to specify from where the modules for the specified applications should be loaded\&. \fIApp\fR is the name of an application, \fIVsn\fR is the version, and \fIDir\fR is the name of the directory where \fIApp-Vsn\fR is located\&. The corresponding modules should be located under \fIDir/App-Vsn/ebin\fR\&. The directories for applications not specified in \fIAppDirs\fR are assumed to be located in \fI$ROOT/lib\fR\&.
.RE
.LP
.B
install_file(Vsn, File) -> ok | {error, Reason}
.br
.RS
.TP
Types
Vsn = File = string()
.br
Reason = term()
.br
.RE
.RS
.LP
Installs a release dependent file in the release structure\&. A release dependent file is a file that must be in the release structure when a new release is installed: \fIstart\&.boot\fR, \fIrelup\fR and \fIsys\&.config\fR\&.
.LP
The function can be called, for example, when these files are generated at the target\&. It should be called after \fIset_unpacked/2\fR has been called\&.
.RE
.LP
.B
install_release(Vsn) -> {ok, OtherVsn, Descr} | {error, Reason}
.br
.B
install_release(Vsn, [Opt]) -> {ok, OtherVsn, Descr} | {error, Reason}
.br
.RS
.TP
Types
Vsn = OtherVsn = string()
.br
Opt = {error_action, Action} | {code_change_timeout, Timeout}
.br
   | {suspend_timeout, Timeout} | {update_paths, Bool}
.br
 Action = restart | reboot
.br
 Timeout = default | infinity | int()>0
.br
 Bool = boolean()
.br
Descr = term()
.br
Reason = {illegal_option, Opt} | {already_installed, Vsn} | {change_appl_data, term()} | term()
.br
.RE
.RS
.LP
Installs the specfied version \fIVsn\fR of the release\&. Looks first for a \fIrelup\fR file for \fIVsn\fR and a script \fI{UpFromVsn, Descr1, Instructions1}\fR in this file for upgrading from the current version\&. If not found, the function looks for a \fIrelup\fR file for the current version and a script \fI{Vsn, Descr2, Instructions2}\fR in this file for downgrading to \fIVsn\fR\&.
.LP
If a script is found, the first thing that happens is that the applications specifications are updated according to the \fI\&.app\fR files and \fIsys\&.config\fR belonging to the release version \fIVsn\fR\&.
.LP
After the application specifications have been updated, the instructions in the script are evaluated and the function returns \fI{ok, OtherVsn, Descr}\fR if successful\&. \fIOtherVsn\fR and \fIDescr\fR are the version (\fIUpFromVsn\fR or \fIVsn\fR) and description (\fIDescr1\fR or \fIDescr2\fR) as specified in the script\&.
.LP
If a recoverable error occurs, the function returns \fI{error, Reason}\fR and the original application specifications are restored\&. If a non-recoverable error occurs, the system is restarted\&.
.LP
The option \fIerror_action\fR defines if the node should be restarted (\fIinit:restart()\fR) or rebooted (\fIinit:reboot()\fR) in case of an error during the installation\&. Default is \fIrestart\fR\&.
.LP
The option \fIcode_change_timeout\fR defines the timeout for all calls to \fIsys:change_code\fR\&. If no value is specified or \fIdefault\fR is given, the default value defined in \fIsys\fR is used\&.
.LP
The option \fIsuspend_timeout\fR defines the timeout for all calls to \fIsys:suspend\fR\&. If no value is specified, the values defined by the \fITimeout\fR parameter of the \fIupgrade\fR or \fIsuspend\fR instructions are used\&. If \fIdefault\fR is specified, the default value defined in \fIsys\fR is used\&.
.LP
The option \fI{update_paths, Bool}\fR indicates if all application code paths should be updated (\fIBool==true\fR), or if only code paths for modified applications should be updated (\fIBool==false\fR, default)\&. This option only has effect for other application directories than the default \fI$ROOT/lib/App-Vsn\fR, that is, application directories provided in the \fIAppDirs\fR argument in a call to \fIcreate_RELEASES/4\fR or \fIset_unpacked/2\fR\&.
.LP
Example: In the current version \fICurVsn\fR of a release, the application directory of \fImyapp\fR is \fI$ROOT/lib/myapp-1\&.0\fR\&. A new version \fINewVsn\fR is unpacked outside the release handler, and the release handler is informed about this with a call to:

.nf
release_handler:set_unpacked(RelFile, [{myapp,"1\&.0","/home/user"},\&.\&.\&.])\&.
=> {ok,NewVsn}
        
.fi
.LP
If \fINewVsn\fR is installed with the option \fI{update_paths, true}\fR, afterwards \fIcode:lib_dir(myapp)\fR will return \fI/home/user/myapp-1\&.0\fR\&.
.RE
.LP
.B
make_permanent(Vsn) -> ok | {error, Reason}
.br
.RS
.TP
Types
Vsn = string()
.br
Reason = {bad_status, Status} | term()
.br
.RE
.RS
.LP
Makes the specified version \fIVsn\fR of the release permanent\&.
.RE
.LP
.B
remove_release(Vsn) -> ok | {error, Reason}
.br
.RS
.TP
Types
Vsn = string()
.br
Reason = {permanent, Vsn} | client_node | term()
.br
.RE
.RS
.LP
Removes a release and its files from the system\&. The release must not be the permanent release\&. Removes only the files and directories not in use by another release\&.
.RE
.LP
.B
reboot_old_release(Vsn) -> ok | {error, Reason}
.br
.RS
.TP
Types
Vsn = string()
.br
Reason = {bad_status, Status} | term()
.br
.RE
.RS
.LP
Reboots the system by making the old release permanent, and calls \fIinit:reboot()\fR directly\&. The release must have status \fIold\fR\&.
.RE
.LP
.B
set_removed(Vsn) -> ok | {error, Reason}
.br
.RS
.TP
Types
Vsn = string()
.br
Reason = {permanent, Vsn} | term()
.br
.RE
.RS
.LP
Makes it possible to handle removal of releases outside the release handler\&. Tells the release handler that the release is removed from the system\&. This function does not delete any files\&.
.RE
.LP
.B
set_unpacked(RelFile, AppDirs) -> {ok, Vsn} | {error, Reason}
.br
.RS
.TP
Types
RelFile = string()
.br
AppDirs = [{App, Vsn, Dir}]
.br
 App = atom()
.br
 Vsn = Dir = string()
.br
Reason = term()
.br
.RE
.RS
.LP
Makes it possible to handle unpacking of releases outside the release handler\&. Tells the release handler that the release is unpacked\&. \fIVsn\fR is extracted from the release resource file \fIRelFile\fR\&.
.LP
\fIAppDirs\fR can be used to specify from where the modules for the specified applications should be loaded\&. \fIApp\fR is the name of an application, \fIVsn\fR is the version, and \fIDir\fR is the name of the directory where \fIApp-Vsn\fR is located\&. The corresponding modules should be located under \fIDir/App-Vsn/ebin\fR\&. The directories for applications not specified in \fIAppDirs\fR are assumed to be located in \fI$ROOT/lib\fR\&.
.RE
.LP
.B
unpack_release(Name) -> {ok, Vsn} | {error, Reason}
.br
.RS
.TP
Types
Name = Vsn = string()
.br
Reason = client_node | term()
.br
.RE
.RS
.LP
Unpacks a release package \fIName\&.tar\&.gz\fR located in the \fIreleases\fR directory\&.
.LP
Performs some checks on the package - for example checks that all mandatory files are present - and extracts its contents\&.
.RE
.LP
.B
which_releases() -> [{Name, Vsn, Apps, Status}]
.br
.RS
.TP
Types
Name = Vsn = string()
.br
Apps = ["App-Vsn"]
.br
Status = unpacked | current | permanent | old
.br
.RE
.RS
.LP
Returns all releases known to the release handler\&.
.RE
.SH APPLICATION UPGRADE/DOWNGRADE
.LP
The following functions can be used to test upgrade and downgrade of single applications (instead of upgrading/downgrading an entire release)\&. A script corresponding to \fIrelup\fR is created on-the-fly, based on the \fI\&.appup\fR file for the application, and evaluated exactly in the same way as \fIrelease_handler\fR does\&.
.SS Warning:
.LP
These function is primarily intended for simplified testing of of \fI\&.appup\fR files\&. They are not run within the context of the \fIrelease_handler\fR process\&. They must therefore \fInot\fR be used together with calls to \fIinstall_release/1, 2\fR, as this will cause \fIrelease_handler\fR to end up in an inconsistent state\&.
.LP
No persistant information is updated, why these functions can be used on any Erlang node, embedded or not\&. Also, using these functions does not effect which code will be loaded in case of a reboot\&.
.LP
If the upgrade or downgrade fails, the application may end up in an inconsistent state\&.

.SH EXPORTS
.LP
.B
upgrade_app(App, Dir) -> {ok, Unpurged} | restart_new_emulator | {error, Reason}
.br
.RS
.TP
Types
App = atom()
.br
Dir = string()
.br
Unpurged = [Module]
.br
 Module = atom()
.br
Reason = term()
.br
.RE
.RS
.LP
Upgrades an application \fIApp\fR from the current version to a new version located in \fIDir\fR according to the \fI\&.appup\fR script\&.
.LP
\fIApp\fR is the name of the application, which must be started\&. \fIDir\fR is the new library directory of \fIApp\fR, the corresponding modules as well as the \fI\&.app\fR and \fI\&.appup\fR files should be located under \fIDir/ebin\fR\&.
.LP
The function looks in the \fI\&.appup\fR file and tries to find an upgrade script from the current version of the application using upgrade_script/2\&. This script is evaluated using eval_appup_script/4, exactly in the same way as install_release/1,2 does\&.
.LP
Returns \fI{ok, Unpurged}\fR if evaluating the script is successful, where \fIUnpurged\fR is a list of unpurged modules, or \fIrestart_new_emulator\fR if this instruction is encountered in the script, or \fI{error, Reason}\fR if an error occurred when finding or evaluating the script\&.
.RE
.LP
.B
downgrade_app(App, Dir) ->
.br
.B
downgrade_app(App, OldVsn, Dir) -> {ok, Unpurged} | restart_new_emulator | {error, Reason}
.br
.RS
.TP
Types
App = atom()
.br
Dir = OldVsn = string()
.br
Unpurged = [Module]
.br
 Module = atom()
.br
Reason = term()
.br
.RE
.RS
.LP
Downgrades an application \fIApp\fR from the current version to a previous version \fIOldVsn\fR located in \fIDir\fR according to the \fI\&.appup\fR script\&.
.LP
\fIApp\fR is the name of the application, which must be started\&. \fIOldVsn\fR is the previous version of the application and can be omitted if \fIDir\fR is of the format \fI"App-OldVsn"\fR\&. \fIDir\fR is the library directory of this previous version of \fIApp\fR, the corresponding modules as well as the old \fI\&.app\fR file should be located under \fIDir/ebin\fR\&. The \fI\&.appup\fR file should be located in the \fIebin\fR directory of the \fIcurrent\fR library directory of the application (\fIcode:lib_dir(App)\fR)\&.
.LP
The function looks in the \fI\&.appup\fR file and tries to find an downgrade script to the previous version of the application using downgrade_script/3\&. This script is evaluated using eval_appup_script/4, exactly in the same way as install_release/1,2 does\&.
.LP
Returns \fI{ok, Unpurged}\fR if evaluating the script is successful, where \fIUnpurged\fR is a list of unpurged modules, or \fIrestart_new_emulator\fR if this instruction is encountered in the script, or \fI{error, Reason}\fR if an error occurred when finding or evaluating the script\&.
.RE
.LP
.B
upgrade_script(App, Dir) -> {ok, NewVsn, Script}
.br
.RS
.TP
Types
App = atom()
.br
Dir = string()
.br
NewVsn = string()
.br
Script = Instructions -- see appup(4)
.br
.RE
.RS
.LP
Tries to find an application upgrade script for \fIApp\fR from the current version to a new version located in \fIDir\fR\&.
.LP
The upgrade script can then be evaluated using eval_appup_script/4\&. It is recommended to use upgrade_app/2 instead, but this function is useful in order to inspect the contents of the script\&.
.LP
\fIApp\fR is the name of the application, which must be started\&. \fIDir\fR is the new library directory of \fIApp\fR, the corresponding modules as well as the \fI\&.app\fR and \fI\&.appup\fR files should be located under \fIDir/ebin\fR\&.
.LP
The function looks in the \fI\&.appup\fR file and tries to find an upgrade script from the current version of the application\&. High-level instructions are translated to low-level instructions and the instructions are sorted in the same manner as when generating a \fIrelup\fR script\&.
.LP
Returns \fI{ok, NewVsn, Script}\fR if successful, where \fINewVsn\fR is the new application version\&.
.LP
Failure: If a script cannot be found, the function fails with an appropriate error reason\&.
.RE
.LP
.B
downgrade_script(App, OldVsn, Dir) -> {ok, Script}
.br
.RS
.TP
Types
App = atom()
.br
OldVsn = Dir = string()
.br
Script = Instructions -- see appup(4)
.br
.RE
.RS
.LP
Tries to find an application downgrade script for \fIApp\fR from the current version to a previous version \fIOldVsn\fR located in \fIDir\fR\&.
.LP
The downgrade script can then be evaluated using eval_appup_script/4\&. It is recommended to use downgrade_app/2,3 instead, but this function is useful in order to inspect the contents of the script\&.
.LP
\fIApp\fR is the name of the application, which must be started\&. \fIDir\fR is the previous library directory of \fIApp\fR, the corresponding modules as well as the old \fI\&.app\fR file should be located under \fIDir/ebin\fR\&. The \fI\&.appup\fR file should be located in the \fIebin\fR directory of the \fIcurrent\fR library directory of the application (\fIcode:lib_dir(App)\fR)\&.
.LP
The function looks in the \fI\&.appup\fR file and tries to find an downgrade script from the current version of the application\&. High-level instructions are translated to low-level instructions and the instructions are sorted in the same manner as when generating a \fIrelup\fR script\&.
.LP
Returns \fI{ok, Script}\fR if successful\&.
.LP
Failure: If a script cannot be found, the function fails with an appropriate error reason\&.
.RE
.LP
.B
eval_appup_script(App, ToVsn, ToDir, Script) -> {ok, Unpurged} | restart_new_emulator | {error, Reason}
.br
.RS
.TP
Types
App = atom()
.br
ToVsn = ToDir = string()
.br
Script -- see upgrade_script/2, downgrade_script/3
.br
Unpurged = [Module]
.br
 Module = atom()
.br
Reason = term()
.br
.RE
.RS
.LP
Evaluates an application upgrade or downgrade script \fIScript\fR, the result from calling upgrade_app/2 or downgrade_app/2,3, exactly in the same way as install_release/1,2 does\&.
.LP
\fIApp\fR is the name of the application, which must be started\&. \fIToVsn\fR is the version to be upgraded/downgraded to, and \fIToDir\fR is the library directory of this version\&. The corresponding modules as well as the \fI\&.app\fR and \fI\&.appup\fR files should be located under \fIDir/ebin\fR\&.
.LP
Returns \fI{ok, Unpurged}\fR if evaluating the script is successful, where \fIUnpurged\fR is a list of unpurged modules, or \fIrestart_new_emulator\fR if this instruction is encountered in the script, or \fI{error, Reason}\fR if an error occurred when evaluating the script\&.
.RE
.SH TYPICAL ERROR REASONS
.RS 2
.TP 2
*
\fI{bad_masters, Masters}\fR - The master nodes \fIMasters\fR are not alive\&.
.TP 2
*
\fI{bad_rel_file, File}\fR - Specified \fI\&.rel\fR file \fIFile\fR can not be read, or does not contain a single term\&.
.TP 2
*
\fI{bad_rel_data, Data}\fR - Specified \fI\&.rel\fR file does not contain a recognized release specification, but another term \fIData\fR\&.
.TP 2
*
\fI{bad_relup_file, File}\fR - Specified \fIrelup\fR file \fIRelup\fR contains bad data\&.
.TP 2
*
\fI{cannot_extract_file, Name, Reason}\fR - Problems when extracting from a tar file, \fIerl_tar:extract/2\fR returned \fI{error, {Name, Reason}}\fR\&.
.TP 2
*
\fI{existing_release, Vsn}\fR - Specified release version \fIVsn\fR is already in use\&.
.TP 2
*
\fI{Master, Reason, When}\fR - Some operation, indicated by the term \fIWhen\fR, failed on the master node \fIMaster\fR with the specified error reason \fIReason\fR\&.
.TP 2
*
\fI{no_matching_relup, Vsn, CurrentVsn}\fR - Cannot find a script for up/downgrading between \fICurrentVsn\fR and \fIVsn\fR\&.
.TP 2
*
\fI{no_such_directory, Path}\fR - The directory \fIPath\fR does not exist\&.
.TP 2
*
\fI{no_such_file, Path}\fR - The path \fIPath\fR (file or directory) does not exist\&.
.TP 2
*
\fI{no_such_file, {Master, Path}}\fR - The path \fIPath\fR (file or directory) does not exist at the master node \fIMaster\fR\&.
.TP 2
*
\fI{no_such_release, Vsn}\fR - The specified version \fIVsn\fR of the release does not exist\&.
.TP 2
*
\fI{not_a_directory, Path}\fR - \fIPath\fR exists, but is not a directory\&.
.TP 2
*
\fI{Posix, File}\fR - Some file operation failed for \fIFile\fR\&. \fIPosix\fR is an atom named from the Posix error codes, such as \fIenoent\fR, \fIeacces\fR or \fIeisdir\fR\&. See \fIfile(3)\fR\&.
.TP 2
*
\fIPosix\fR - Some file operation failed, as above\&.
.RE
.SH SEE ALSO
.LP
OTP Design Principles, config(4), relup(4), rel(4), script(4), sys(3), systools(3)
