.TH beam_lib 3 "stdlib  1.16.3" "Ericsson AB" "ERLANG MODULE DEFINITION"
.SH MODULE
beam_lib \- An Interface To the BEAM File Format
.SH DESCRIPTION
.LP
\fIbeam_lib\fR provides an interface to files created by the BEAM compiler ("BEAM files")\&. The format used, a variant of "EA IFF 1985" Standard for Interchange Format Files, divides data into chunks\&.
.LP
Chunk data can be returned as binaries or as compound terms\&. Compound terms are returned when chunks are referenced by names (atoms) rather than identifiers (strings)\&. The names recognized and the corresponding identifiers are:
.RS 2
.TP 2
*
\fIabstract_code ("Abst")\fR
.TP 2
*
\fIattributes ("Attr")\fR
.TP 2
*
\fIcompile_info ("CInf")\fR
.TP 2
*
\fIexports ("ExpT")\fR
.TP 2
*
\fIlabeled_exports ("ExpT")\fR
.TP 2
*
\fIimports ("ImpT")\fR
.TP 2
*
\fIindexed_imports ("ImpT")\fR
.TP 2
*
\fIlocals ("LocT")\fR
.TP 2
*
\fIlabeled_locals ("LocT")\fR
.TP 2
*
\fIatoms ("Atom")\fR
.RE

.SH DEBUG INFORMATION/ABSTRACT CODE
.LP
The option \fIdebug_info\fR can be given to the compiler (see compile(3)) in order to have debug information in the form of abstract code (see The Abstract Format in ERTS User\&'s Guide) stored in the \fIabstract_code\fR chunk\&. Tools such as Debugger and Xref require the debug information to be included\&.
.SS Warning:
.LP
Source code can be reconstructed from the debug information\&. Use encrypted debug information (see below) to prevent this\&.

.LP
The debug information can also be removed from BEAM files using strip/1, strip_files/1 and/or strip_release/1\&.
.LP
\fIReconstructing source code\fR
.LP
Here is an example of how to reconstruct source code from the debug information in a BEAM file \fIBeam\fR:

.nf
      {ok,{_,[{abstract_code,{_,AC}}]}} = beam_lib:chunks(Beam,[abstract_code])\&.
      io:fwrite("~s~n", [erl_prettypr:format(erl_syntax:form_list(AC))])\&.
.fi
.LP
\fIEncrypted debug information\fR
.LP
The debug information can be encrypted in order to keep the source code secret, but still being able to use tools such as Xref or Debugger\&. 
.LP
To use encrypted debug information, a key must be provided to the compiler and \fIbeam_lib\fR\&. The key is given as a string and it is recommended that it contains at least 32 characters and that both upper and lower case letters as well as digits and special characters are used\&.
.LP

.LP
The default type -- and currently the only type -- of crypto algorithm is \fIdes3_cbc\fR, three rounds of DES\&. The key string will be scrambled using \fIerlang:md5/1\fR to generate the actual keys used for \fIdes3_cbc\fR\&.
.SS Note:
.LP
As far as we know by the time of writing, it is infeasible to break \fIdes3_cbc\fR encryption without any knowledge of the key\&. Therefore, as long as the key is kept safe and is unguessable, the encrypted debug information \fIshould\fR be safe from intruders\&.

.LP
There are two ways to provide the key:
.RS 2
.TP 4
1.
Use the compiler option \fI{debug_info, Key}\fR, see compile(3), and the function crypto_key_fun/1 to register a fun which returns the key whenever \fIbeam_lib\fR needs to decrypt the debug information\&.
.RS 4
.LP

.LP
If no such fun is registered, \fIbeam_lib\fR will instead search for a \fI\&.erlang\&.crypt\fR file, see below\&.
.RE
.TP 4
2.
Store the key in a text file named \fI\&.erlang\&.crypt\fR\&.
.RS 4
.LP

.LP
In this case, the compiler option \fIencrypt_debug_info\fR can be used, see compile(3)\&.
.RE
.RE
.LP
\fI\&.erlang\&.crypt\fR
.LP
\fIbeam_lib\fR searches for \fI\&.erlang\&.crypt\fR in the current directory and then the home directory for the current user\&. If the file is found and contains a key, \fIbeam_lib\fR will implicitly create a crypto key fun and register it\&.
.LP
The \fI\&.erlang\&.crypt\fR file should contain a single list of tuples:

.nf
      {debug_info, Mode, Module, Key}
.fi
.LP
\fIMode\fR is the type of crypto algorithm; currently, the only allowed value thus is \fIdes3_cbc\fR\&. \fIModule\fR is either an atom, in which case \fIKey\fR will only be used for the module \fIModule\fR, or \fI[]\fR, in which case \fIKey\fR will be used for all modules\&. \fIKey\fR is the non-empty key string\&.
.LP
The \fIKey\fR in the first tuple where both \fIMode\fR and \fIModule\fR matches will be used\&.
.LP
Here is an example of an \fI\&.erlang\&.crypt\fR file that returns the same key for all modules:

.nf
[{debug_info, des3_cbc, [], "%>7}|pc/DM6Cga*68$Mw]L#&_Gejr]G^"}]\&.
.fi
.LP
And here is a slightly more complicated example of an \fI\&.erlang\&.crypt\fR which provides one key for the module \fIt\fR, and another key for all other modules:

.nf
[{debug_info, des3_cbc, t, "My KEY"},
 {debug_info, des3_cbc, [], "%>7}|pc/DM6Cga*68$Mw]L#&_Gejr]G^"}]\&.
.fi
.SS Note:
.LP
Do not use any of the keys in these examples\&. Use your own keys\&.

.SH DATA TYPES

.nf
beam() -> Module | Filename | binary()
  Module = atom()
  Filename = string() | atom()
.fi
.LP
Each of the functions described below accept either the module name, the filename, or a binary containing the beam module\&.

.nf
chunkdata() = {ChunkId, DataB} | {ChunkName, DataT}
  ChunkId = chunkid()
  DataB = binary()
  {ChunkName, DataT} =
        {abstract_code, AbstractCode}
      | {attributes, [{Attribute, [AttributeValue]}]}
      | {compile_info, [{InfoKey, [InfoValue]}]}
      | {exports, [{Function, Arity}]}
      | {labeled_exports, [{Function, Arity, Label}]}
      | {imports, [{Module, Function, Arity}]}
      | {indexed_imports, [{Index, Module, Function, Arity}]}
      | {locals, [{Function, Arity}]}]}
      | {labeled_locals, [{Function, Arity, Label}]}]}
      | {atoms, [{integer(), atom()}]}
  AbstractCode = {AbstVersion, Forms} | no_abstract_code
    AbstVersion = atom()
  Attribute = atom()
  AttributeValue = term()
  Module = Function = atom()
  Arity = int()
  Label = int()
.fi
.LP
It is not checked that the forms conform to the abstract format indicated by \fIAbstVersion\fR\&. \fIno_abstract_code\fR means that the \fI"Abst"\fR chunk is present, but empty\&.
.LP
The list of attributes is sorted on \fIAttribute\fR, and each attribute name occurs once in the list\&. The attribute values occur in the same order as in the file\&. The lists of functions are also sorted\&.

.nf
chunkid() = "Abst" | "Attr" | "CInf"
            | "ExpT" | "ImpT" | "LocT"
            | "Atom"

chunkname() = abstract_code | attributes | compile_info
            | exports | labeled_exports
            | imports | indexed_imports
            | locals | labeled_locals
            | atoms
      
chunkref() = chunkname() | chunkid()
.fi
.SH EXPORTS
.LP
.B
chunks(Beam, [ChunkRef]) -> {ok, {Module, [ChunkData]}} | {error, beam_lib, Reason}
.br
.RS
.TP
Types
Beam = beam()
.br
ChunkRef = chunkref()
.br
Module = atom()
.br
ChunkData = chunkdata()
.br
Reason = {unknown_chunk, Filename, atom()}
.br
  | {key_missing_or_invalid, Filename, abstract_code}
.br
  | Reason1 -- see info/1
.br
 Filename = string()
.br
.RE
.RS
.LP
Reads chunk data for selected chunks refs\&. The order of the returned list of chunk data is determined by the order of the list of chunks references\&.
.RE
.LP
.B
chunks(Beam, [ChunkRef], [Option]) -> {ok, {Module, [ChunkResult]}} | {error, beam_lib, Reason}
.br
.RS
.TP
Types
Beam = beam()
.br
ChunkRef = chunkref()
.br
Module = atom()
.br
Option = allow_missing_chunks
.br
ChunkResult = {chunkref(), ChunkContents} | {chunkref(), missing_chunk}
.br
Reason = {missing_chunk, Filename, atom()}
.br
  | {key_missing_or_invalid, Filename, abstract_code}
.br
  | Reason1 -- see info/1
.br
 Filename = string()
.br
.RE
.RS
.LP
Reads chunk data for selected chunks refs\&. The order of the returned list of chunk data is determined by the order of the list of chunks references\&.
.LP
By default, if any requested chunk is missing in \fIBeam\fR, an \fIerror\fR tuple is returned\&. However, if the option \fIallow_missing_chunks\fR has been given, a result will be returned even if chunks are missing\&. In the result list, any missing chunks will be represented as \fI{ChunkRef, missing_chunk}\fR\&. Note, however, that if the \fI"Atom"\fR chunk if missing, that is considered a fatal error and the return value will be an \fIerror\fR tuple\&.
.RE
.LP
.B
version(Beam) -> {ok, {Module, [Version]}} | {error, beam_lib, Reason}
.br
.RS
.TP
Types
Beam = beam()
.br
Module = atom()
.br
Version = term()
.br
Reason -- see chunks/2
.br
.RE
.RS
.LP
Returns the module version(s)\&. A version is defined by the module attribute \fI-vsn(Vsn)\fR\&. If this attribute is not specified, the version defaults to the checksum of the module\&. Note that if the version \fIVsn\fR is not a list, it is made into one, that is \fI{ok, {Module, [Vsn]}}\fR is returned\&. If there are several \fI-vsn\fR module attributes, the result is the concatenated list of versions\&. Examples:

.nf
1> beam_lib:version(a)\&.
 % -vsn(1)\&.
{ok,{a,[1]}}
2> beam_lib:version(b)\&.
 % -vsn([1])\&.
{ok,{b,[1]}}
3> beam_lib:version(c)\&.
 % -vsn([1])\&. -vsn(2)\&.
{ok,{c,[1,2]}}
4> beam_lib:version(d)\&.
 % no -vsn attribute
{ok,{d,[275613208176997377698094100858909383631]}}
.fi
.RE
.LP
.B
md5(Beam) -> {ok, {Module, MD5}} | {error, beam_lib, Reason}
.br
.RS
.TP
Types
Beam = beam()
.br
Module = atom()
.br
MD5 = binary()
.br
Reason -- see chunks/2
.br
.RE
.RS
.LP
Calculates an MD5 redundancy check for the code of the module (compilation date and other attributes are not included)\&.
.RE
.LP
.B
info(Beam) -> [{Item, Info}] | {error, beam_lib, Reason1}
.br
.RS
.TP
Types
Beam = beam()
.br
Item, Info -- see below
.br
Reason1 = {chunk_too_big, Filename, ChunkId, ChunkSize, FileSize}
.br
  | {invalid_beam_file, Filename, Pos}
.br
  | {invalid_chunk, Filename, ChunkId}
.br
  | {missing_chunk, Filename, ChunkId}
.br
  | {not_a_beam_file, Filename}
.br
  | {file_error, Filename, Posix}
.br
 Filename = string()
.br
 ChunkId = chunkid()
.br
 ChunkSize = FileSize = int()
.br
 Pos = int()
.br
 Posix = posix() -- see file(3)
.br
.RE
.RS
.LP
Returns a list containing some information about a BEAM file as tuples \fI{Item, Info}\fR:
.RS 2
.TP 4
.B
\fI{file, Filename} | {binary, Binary}\fR:
The name (string) of the BEAM file, or the binary from which the information was extracted\&.
.TP 4
.B
\fI{module, Module}\fR:
The name (atom) of the module\&.
.TP 4
.B
\fI{chunks, [{ChunkId, Pos, Size}]}\fR:
For each chunk, the identifier (string) and the position and size of the chunk data, in bytes\&.
.RE
.RE
.LP
.B
cmp(Beam1, Beam2) -> ok | {error, beam_lib, Reason}
.br
.RS
.TP
Types
Beam1 = Beam2 = beam()
.br
Reason = {modules_different, Module1, Module2}
.br
  | {chunks_different, ChunkId}
.br
  | Reason1 -- see info/1
.br
 Module1 = Module2 = atom()
.br
 ChunkId = chunkid()
.br
.RE
.RS
.LP
Compares the contents of two BEAM files\&. If the module names are the same, and the chunks with the identifiers \fI"Code"\fR, \fI"ExpT"\fR, \fI"ImpT"\fR, \fI"StrT"\fR, and \fI"Atom"\fR have the same contents in both files, \fIok\fR is returned\&. Otherwise an error message is returned\&.
.RE
.LP
.B
cmp_dirs(Dir1, Dir2) -> {Only1, Only2, Different} | {error, beam_lib, Reason1}
.br
.RS
.TP
Types
Dir1 = Dir2 = string() | atom()
.br
Different = [{Filename1, Filename2}]
.br
Only1 = Only2 = [Filename]
.br
Filename = Filename1 = Filename2 = string()
.br
Reason1 = {not_a_directory, term()} | -- see info/1
.br
.RE
.RS
.LP
The \fIcmp_dirs/2\fR function compares the BEAM files in two directories\&. Only files with extension \fI"\&.beam"\fR are compared\&. BEAM files that exist in directory \fIDir1\fR (\fIDir2\fR) only are returned in \fIOnly1\fR (\fIOnly2\fR)\&. BEAM files that exist on both directories but are considered different by \fIcmp/2\fR are returned as pairs {\fIFilename1\fR, \fIFilename2\fR} where \fIFilename1\fR (\fIFilename2\fR) exists in directory \fIDir1\fR (\fIDir2\fR)\&.
.RE
.LP
.B
diff_dirs(Dir1, Dir2) -> ok | {error, beam_lib, Reason1}
.br
.RS
.TP
Types
Dir1 = Dir2 = string() | atom()
.br
Reason1 = {not_a_directory, term()} | -- see info/1
.br
.RE
.RS
.LP
The \fIdiff_dirs/2\fR function compares the BEAM files in two directories the way \fIcmp_dirs/2\fR does, but names of files that exist in only one directory or are different are presented on standard output\&.
.RE
.LP
.B
strip(Beam1) -> {ok, {Module, Beam2}} | {error, beam_lib, Reason1}
.br
.RS
.TP
Types
Beam1 = Beam2 = beam()
.br
Module = atom()
.br
Reason1 -- see info/1
.br
.RE
.RS
.LP
The \fIstrip/1\fR function removes all chunks from a BEAM file except those needed by the loader\&. In particular, the debug information (\fIabstract_code\fR chunk) is removed\&.
.RE
.LP
.B
strip_files(Files) -> {ok, [{Module, Beam2}]} | {error, beam_lib, Reason1}
.br
.RS
.TP
Types
Files = [Beam1]
.br
 Beam1 = beam()
.br
Module = atom()
.br
Beam2 = beam()
.br
Reason1 -- see info/1
.br
.RE
.RS
.LP
The \fIstrip_files/1\fR function removes all chunks except those needed by the loader from BEAM files\&. In particular, the debug information (\fIabstract_code\fR chunk) is removed\&. The returned list contains one element for each given file name, in the same order as in \fIFiles\fR\&.
.RE
.LP
.B
strip_release(Dir) -> {ok, [{Module, Filename]}} | {error, beam_lib, Reason1}
.br
.RS
.TP
Types
Dir = string() | atom()
.br
Module = atom()
.br
Filename = string()
.br
Reason1 = {not_a_directory, term()} | -- see info/1
.br
.RE
.RS
.LP
The \fIstrip_release/1\fR function removes all chunks except those needed by the loader from the BEAM files of a release\&. \fIDir\fR should be the installation root directory\&. For example, the current OTP release can be stripped with the call \fIbeam_lib:strip_release(code:root_dir())\fR\&.
.RE
.LP
.B
format_error(Reason) -> Chars
.br
.RS
.TP
Types
Reason -- see other functions
.br
Chars = [char() | Chars]
.br
.RE
.RS
.LP
Given the error returned by any function in this module, the function \fIformat_error\fR returns a descriptive string of the error in English\&. For file errors, the function \fIfile:format_error(Posix)\fR should be called\&.
.RE
.LP
.B
crypto_key_fun(CryptoKeyFun) -> ok | {error, Reason}
.br
.RS
.TP
Types
CryptoKeyFun = fun() -- see below
.br
Reason = badfun | exists | term()
.br
.RE
.RS
.LP
The \fIcrypto_key_fun/1\fR function registers a unary fun that will be called if \fIbeam_lib\fR needs to read an \fIabstract_code\fR chunk that has been encrypted\&. The fun is held in a process that is started by the function\&.
.LP
If there already is a fun registered when attempting to register a fun, \fI{error, exists}\fR is returned\&.
.LP
The fun must handle the following arguments:

.nf
          CryptoKeyFun(init) -> ok | {ok, NewCryptoKeyFun} | {error, Term}
.fi
.LP
Called when the fun is registered, in the process that holds the fun\&. Here the crypto key fun can do any necessary initializations\&. If \fI{ok, NewCryptoKeyFun}\fR is returned then \fINewCryptoKeyFun\fR will be registered instead of \fICryptoKeyFun\fR\&. If \fI{error, Term}\fR is returned, the registration is aborted and \fIcrypto_key_fun/1\fR returns \fI{error, Term}\fR as well\&.

.nf
          CryptoKeyFun({debug_info, Mode, Module, Filename}) -> Key
.fi
.LP
Called when the key is needed for the module \fIModule\fR in the file named \fIFilename\fR\&. \fIMode\fR is the type of crypto algorithm; currently, the only possible value thus is \fIdes3_cbc\fR\&. The call should fail (raise an exception) if there is no key available\&.

.nf
          CryptoKeyFun(clear) -> term()
.fi
.LP
Called before the fun is unregistered\&. Here any cleaning up can be done\&. The return value is not important, but is passed back to the caller of \fIclear_crypto_key_fun/0\fR as part of its return value\&.
.RE
.LP
.B
clear_crypto_key_fun() -> {ok, Result}
.br
.RS
.TP
Types
Result = undefined | term()
.br
.RE
.RS
.LP
Unregisters the crypto key fun and terminates the process holding it, started by \fIcrypto_key_fun/1\fR\&.
.LP
The \fIclear_crypto_key_fun/1\fR either returns \fI{ok, undefined}\fR if there was no crypto key fun registered, or \fI{ok, Term}\fR, where \fITerm\fR is the return value from \fICryptoKeyFun(clear)\fR, see \fIcrypto_key_fun/1\fR\&.
.RE
