.TH dialyzer 3 "dialyzer  2.0.0" "Ericsson AB" "ERLANG MODULE DEFINITION"
.SH MODULE
dialyzer \- The Dialyzer, a DIscrepancy AnalYZer for ERlang programs
.SH DESCRIPTION
.LP
The Dialyzer is a static analysis tool that identifies software discrepancies such as definite type errors, code which has become dead or unreachable due to some programming error, unnecessary tests, etc\&. in single Erlang modules or entire (sets of) applications\&. Dialyzer starts its analysis from either debug-compiled BEAM bytecode or from Erlang source code\&. The file and line number of a discrepancy is reported along with an indication of what the discrepancy is about\&. Dialyzer bases its analysis on the concept of success typings which allows for sound warnings (no false positives)\&.
.LP
Read more about Dialyzer and about how to use it from the GUI in Dialyzer User\&'s Guide\&.

.SH USING THE DIALYZER FROM THE COMMAND LINE
.LP
Dialyzer also has a command line version for automated use\&. Below is a brief description of the list of its options\&. The same information can be obtained by writing

.nf
      dialyzer --help
    
.fi
.LP
in a shell\&. Please refer to the GUI description for more details on the operation of Dialyzer\&.
.LP
The exit status of the command line version is:

.nf
      0 - No problems were encountered during the analysis and no
          warnings were emitted\&.
      1 - Problems were encountered during the analysis\&.
      2 - No problems were encountered, but warnings were emitted\&.
    
.fi
.LP
Usage:

.nf
   dialyzer [--help] [--version] [--shell] [--quiet] [--verbose]
                [-pa dir]* [--plt plt] [-Ddefine]* [-I include_dir]* 
                [--output_plt file] [-Wwarn]* [--src] 
                [-c applications] [-r applications] [-o outfile]
                [--build_plt] [--add_to_plt] [--remove_from_plt]
                [--check_plt] [--no_check_plt] [--plt_info] [--get_warnings]
    
.fi
.LP
Options:
.RS 2
.TP 4
.B
\fI-c applications\fR(or \fI--command-line applications\fR):
use Dialyzer from the command line (no GUI) to detect defects in the specified applications (directories or \fI\&.erl\fR or \fI\&.beam\fR files)
.TP 4
.B
\fI-r applications\fR:
same as \fI-c\fR only that directories are searched recursively for subdirectories containing \fI\&.erl\fR or \fI\&.beam\fR files (depending on the type of analysis)
.TP 4
.B
\fI-o outfile\fR(or \fI--output outfile\fR):
when using Dialyzer from the command line, send the analysis results in the specified \fIoutfile\fR rather than in stdout
.TP 4
.B
\fI--src\fR:
overide the default, which is to analyze debug compiled BEAM bytecode, and analyze starting from Erlang source code instead
.TP 4
.B
\fI--raw\fR:
When using Dialyzer from the command line, output the raw analysis results (Erlang terms) instead of the formatted result\&. The raw format is easier to post-process (for instance, to filter warnings or to output HTML pages)\&.
.TP 4
.B
\fI-Dname\fR(or \fI-Dname=value\fR):
when analyzing from source, pass the define to Dialyzer (**)
.TP 4
.B
\fI-I include_dir\fR:
when analyzing from source, pass the \fIinclude_dir\fR to Dialyzer (**)
.TP 4
.B
\fI-pa dir\fR:
Include \fIdir\fR in the path for Erlang\&. Useful when analyzing files that have \fI-include_lib()\fR directives\&.
.TP 4
.B
\fI--output_plt file\fR:
Store the PLT at the specified location after building it\&.
.TP 4
.B
\fI--plt plt\fR:
Use the specified plt as the initial persistent lookup table\&.
.TP 4
.B
\fI-Wwarn\fR:
a family of option which selectively turn on/off warnings\&. (for help on the names of warnings use \fIdialyzer -Whelp\fR)
.TP 4
.B
\fI--shell\fR:
do not disable the Erlang shell while running the GUI
.TP 4
.B
\fI--version (or -v)\fR:
prints the Dialyzer version and some more information and exits
.TP 4
.B
\fI--help (or -h)\fR:
prints this message and exits
.TP 4
.B
\fI--quiet (or -q)\fR:
makes Dialyzer a bit more quiet
.TP 4
.B
\fI--verbose\fR:
makes Dialyzer a bit more verbose
.TP 4
.B
\fI--check_plt\fR:
Only checks if the initial PLT is up to date and rebuilds it if this is not the case
.TP 4
.B
\fI--no_check_plt (or -n)\fR:
Skip the PLT integrity check when running Dialyzer\&. Useful when working with installed PLTs that never change\&.
.TP 4
.B
\fI--build_plt\fR:
The analysis starts from an empty PLT and creates a new one from the files specified with -c and -r\&. Only works for beam files\&. Use --plt or --output_plt to override the default PLT location\&.
.TP 4
.B
\fI--add_to_plt\fR:
The PLT is extended to also include the files specified with -c and -r\&. Use --plt to specify wich PLT to start from, and --output_plt to specify where to put the PLT\&. Note that the analysis might include files from the PLT if they depend on the new files\&. This option only works with beam files\&.
.TP 4
.B
\fI--remove_from_plt\fR:
The information from the files specified with -c and -r is removed from the PLT\&. Note that this may cause a re-analysis of the remaining dependent files\&.
.TP 4
.B
\fI--get_warnings\fR:
Makes Dialyzer emit warnings even when manipulating the PLT\&. Only emits warnings for files that are actually analyzed\&. The default is to not emit any warnings when manipulating the PLT\&. This option has no effect when performing a normal analysis\&.
.RE
.SS Note:
.LP
* denotes that multiple occurrences of these options are possible\&.
.LP
** options \fI-D\fR and \fI-I\fR work both from command-line and in the Dialyzer GUI; the syntax of defines and includes is the same as that used by \fIerlc\fR\&.

.LP
Warning options:
.RS 2
.TP 4
.B
\fI-Wno_return\fR:
Suppress warnings for functions of no return\&.
.TP 4
.B
\fI-Wno_unused\fR:
Suppress warnings for unused functions\&.
.TP 4
.B
\fI-Wno_improper_lists\fR:
Suppress warnings for construction of improper lists\&.
.TP 4
.B
\fI-Wno_fun_app\fR:
Suppress warnings for fun applications that will fail\&.
.TP 4
.B
\fI-Wno_match\fR:
Suppress warnings for patterns that are unused or cannot match\&.
.TP 4
.B
\fI-Werror_handling\fR***:
Include warnings for functions that only return by means of an exception\&.
.TP 4
.B
\fI-Wunmatched_returns\fR***:
Include warnings for function calls which ignore a structured return value or do not match against one of many possible return value(s)\&.
.TP 4
.B
\fI-Wunderspecs\fR***:
Warn about underspecified functions (the -spec is strictly more allowing than the success typing)
.TP 4
.B
\fI-Woverspecs\fR***:
Warn about overspecified functions (the -spec is strictly less allowing than the success typing)
.TP 4
.B
\fI-Wspecdiffs\fR***:
Warn when the -spec is different than the success typing
.RE
.SS Note:
.LP
*** These are options that turn on warnings rather than turning them off\&.

.SH USING THE DIALYZER FROM ERLANG
.LP
You can also use Dialyzer directly from Erlang\&. Both the GUI and the command line version are available\&. The options are similar to the ones given from the command line, so please refer to the sections above for a description of these\&.
.SH EXPORTS
.LP
.B
gui() -> ok | {error, Msg}
.br
.B
gui(OptList) -> ok | {error, Msg}
.br
.RS
.TP
Types
OptList -- see below
.br
.RE
.RS
.LP
Dialyzer GUI version\&.

.nf
OptList  : [Option]
Option   : {files,          [Filename : string()]}
         | {files_rec,      [DirName : string()]}
         | {defines,        [{Macro: atom(), Value : term()}]}
         | {from,           src_code | byte_code} %% Defaults to byte_code
         | {init_plt,       FileName : string()}  %% If changed from default
         | {include_dirs,   [DirName : string()]} 
         | {output_file,    FileName : string()}
         | {output_plt,     FileName :: string()}
         | {analysis_type,  \&'success_typings\&' | \&'plt_add\&' | \&'plt_build\&' | \&'plt_check\&' | \&'plt_remove\&'}
         | {warnings,       [WarnOpts]}
         | {get_warnings,   bool()}

WarnOpts : no_return
         | no_unused
         | no_improper_lists
         | no_fun_app
         | no_match
         | no_fail_call
         | error_handling
         | unmatched_returns
         | overspecs
         | underspecs
         | specdiffs
        
.fi
.RE
.LP
.B
run(OptList) -> Warnings
.br
.RS
.TP
Types
OptList -- see gui/0, 1
.br
Warnings -- see below 
.br
.RE
.RS
.LP
Dialyzer command line version\&.

.nf
Warnings :: [{Tag, Id, Msg}]
Tag : \&'warn_return_no_exit\&' | \&'warn_return_only_exit\&'
         | \&'warn_not_called\&' | \&'warn_non_proper_list\&'
         | \&'warn_fun_app\&' | \&'warn_matching\&'
         | \&'warn_failing_call\&' | \&'warn_contract_types\&'
         | \&'warn_contract_syntax\&' | \&'warn_contract_not_equal\&'
         | \&'warn_contract_subtype\&' | \&'warn_contract_supertype\&'
Id = {File :: string(), Line :: integer()}
Msg = msg() -- Undefined

.fi
.RE
.LP
.B
format_warning(Msg) -> string()
.br
.RS
.TP
Types
Msg = {Tag, Id, msg()} -- See run/1
.br
.RE
.RS
.LP
Get a string from warnings as returned by dialyzer:run/1\&.
.RE
.LP
.B
plt_info(string()) -> {\&'ok\&', [{atom(), any()}]} | {\&'error\&', atom()}
.br
.RS
.LP
Returns information about the specified plt\&.
.RE
