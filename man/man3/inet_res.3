.TH inet_res 3 "kernel  2.13.3" "Ericsson AB" "ERLANG MODULE DEFINITION"
.SH MODULE
inet_res \- A Rudimentary DNS Client
.SH DESCRIPTION
.LP
Performs DNS name resolving towards recursive name servers
.LP
See also  ERTS User\&'s Guide: Inet configuration  for more information on how to configure an Erlang runtime system for IP communication and how to enable this DNS client by defining \fI\&'dns\&'\fR as a lookup method\&. It then acts as a backend for the resolving functions in inet\&.
.LP
This DNS client can resolve DNS records even if it is not used for normal name resolving in the node\&.
.LP
This is not a full-fledged resolver\&. It is just a DNS client that relies on asking trusted recursive nameservers\&.

.SH NAME RESOLVING
.LP
UDP queries are used unless resolver option \fIusevc\fR is \fItrue\fR, which forces TCP queries\&. If the query is to large for UDP, TCP is used instead\&. For regular DNS queries 512 bytes is the size limit\&. When EDNS is enabled (resolver option \fIedns\fR is set to the EDNS version i\&.e \fI0\fR instead of \fIfalse\fR), resolver option \fIudp_payload_size\fR sets the limit\&. If a nameserver replies with the TC bit set (truncation), indicating the answer is incomplete, the query is retried to that nameserver using TCP\&. The resolver option \fIudp_payload_size\fR also sets the advertised size for the max allowed reply size, if EDNS is enabled, otherwise the nameserver uses the limit 512 byte\&. If the reply is larger it gets truncated, forcing a TCP re-query\&.
.LP
For UDP queries, the resolver options \fItimeout\fR and \fIretry\fR control retransmission\&. Each nameserver in the \fInameservers\fR list is tried with a timeout of \fItimeout\fR / \fIretry\fR\&. Then all nameservers are tried again doubling the timeout, for a total of \fIretry\fR times\&.
.LP
For queries that not use the \fIsearch\fR list, if the query to all \fInameservers\fR results in \fI{error, nxdomain}\fRor an empty answer, the same query is tried for the \fIalt_nameservers\fR\&.
.SH DATA TYPES
.LP
As defined in the module inet:

.nf
hostent() = #hostent{}
posix() = some atom()s
ip_address() = tuple of integers of arity 4 or 8
.fi
.LP
Resolver types:

.nf
These correspond to resolver options:

res_option() =
    [ {alt_nameservers, [ nameserver() ]}
    | {edns, 0 | false}               % Use EDNS
    | {inet6, bool()}                 % Return IPv6 addresses
    | {nameservers, [ nameserver() ]} % List of nameservers
    | {recurse, bool()}               % Request server recursion
    | {retry, integer()}              % UDP retries
    | {timeout, integer()}            % UDP query timeout
    | {udp_payload_size, integer()}   % EDNS payload size
    | {usevc, bool()} ]               % Use TCP (Virtual Circuit)

nameserver() = {ip_address(),Port}
    Port = integer(1\&.\&.65535)

res_error() =
    formerr |
    qfmterror |
    servfail |
    nxdomain |
    notimp |
    refused |
    badvers |
    timeout

.fi
.LP
DNS types:

.nf
dns_name() = string() with no adjacent dots

rr_type() = a | aaaa | cname | gid | hinfo | ns | mb | md | mg | mf
          | minfo | mx | naptr | null | ptr | soa | spf | srv | txt
          | uid | uinfo | unspec | wks

query_type() = axfr | mailb | maila | any | rr_type()

dns_class() = in | chaos | hs | any

dns_msg() = DnsMsg
    This is the start of a hiearchy of opaque data structures
    that can be examined with access functions in inet_dns
    that return lists of {Field,Value} tuples\&. The arity 2
    functions just return the value for a given field\&.

    inet_dns:msg(DnsMsg) ->
        [ {header, dns_header()}
        | {qdlist, dns_query()}
        | {anlist, dns_rr()}
        | {nslist, dns_rr()}
        | {arlist, dns_rr()} ]
    inet_dns:msg(DnsMsg, header) -> dns_header() % for example
    inet_dns:msg(DnsMsg, Field) -> Value

dhs_header() = DnsHeader
    inet_dns:header(DnsHeader) ->
        [ {id, integer()}
        | {qr, bool()}
        | {opcode, \&'query\&' | iquery | status | integer()}
        | {aa, bool()}
        | {tc, bool()}
        | {rd, bool()}
        | {ra, bool()}
        | {pr, bool()}
        | {rcode, integer(0\&.\&.16)} ]
    inet_dns:header(DnsHeader, Field) -> Value

dns_query() = DnsQuery
    inet_dns:dns_query(DnsQuery) ->
        [ {domain, dns_name()}
        | {type, query_type()}
        | {class, dns_class()} ]
    inet_dns:dns_query(DnsQuery, Field) -> Value

dns_rr() = DnsRr
    inet_dns:rr(DnsRr) -> DnsRrFields | DnsRrOptFields
    DnsRrFields = [ {domain, dns_name()}
                  | {type, rr_type()}
                  | {class, dns_class()}
                  | {ttl, integer()}
                  | {data, dns_data()} ]
    DnsRrOptFields = [ {domain, dns_name()}
                     | {type, opt}
                     | {udp_payload_size, integer()}
                     | {ext_rcode, integer()}
                     | {version, integer()}
                     | {z, integer()}
                     | {data, dns_data()} ]
    inet_dns:rr(DnsRr, Field) -> Value

dns_data() =             % for dns_type()
    [ dns_name()         % ns, md, mf, cname, mb, mg, mr, ptr
    | ip_address(v4)     % a
    | ip_address(v6)     % aaaa
    | {MName,RName,Serial,Refresh,Retry,Expiry,Minimum} % soa
    | {ip_address(v4),Proto,BitMap} % wks
    | {CpuString,OsString} % hinfo
    | {RM,EM}            % minfo
    | {Prio,dns_name()}  % mx
    | {Prio,Weight,Port,dns_name()} % srv
    | {Order,Preference,Flags,Services,Regexp,dns_name()} % naptr
    | [ string() ]         % txt, spf
    | binary() ]           % null, integer()
MName, RName = dns_name()
Serial, Refresh, Retry, Expiry, Minimum = integer(),
Proto = integer()
BitMap = binary()
CpuString, OsString = string()
RM = EM = dns_name()
Prio, Weight, Port = integer()
Order, Preference = integer()
Flags, Services = string(),
Regexp = string(utf8)



There is an info function for the types above:

inet_dns:record_type(dns_msg()) -> msg;
inet_dns:record_type(dns_header()) -> header;
inet_dns:record_type(dns_query()) -> dns_query;
inet_dns:record_type(dns_rr()) -> rr;
inet_dns:record_type(_) -> undefined\&.

So; inet_dns:(inet_dns:record_type(X))(X) will convert
any of these data structures into a {Field,Value} list\&.
.fi
.SH EXPORTS
.LP
.B
getbyname(Name, Type) -> {ok,hostent()} | {error,Reason}
.br
.B
getbyname(Name, Type, Timeout) -> {ok,hostent()} | {error,Reason} 
.br
.RS
.TP
Types
Name = dns_name()
.br
Type = rr_type()
.br
Timeout = integer() >= 0 | infinity
.br
Reason = posix() | res_error()
.br
.RE
.RS
.LP
Resolve a DNS record of the given type for the given host, of class \fIin\fR\&. On success returns a \fIhostent()\fR record with \fIdns_data()\fR elements in the address list field\&. 
.LP
This function uses the resolver option \fIsearch\fR that is a list of domain names\&. If the name to resolve contains no dots, it is prepended to each domain name in the search list, and they are tried in order\&. If the name contains dots, it is first tried as an absolute name and if that fails the search list is used\&. If the name has a trailing dot it is simply supposed to be an absolute name and the search list is not used\&. 
.RE
.LP
.B
gethostbyaddr(Address) -> {ok,hostent()} | {error,Reason}
.br
.B
gethostbyaddr(Address, Timeout) -> {ok,hostent()} | {error,Reason} 
.br
.RS
.TP
Types
Address = ip_address()
.br
Timeout = integer() >= 0 | infinity
.br
Reason = posix() | res_error()
.br
.RE
.RS
.LP
Backend functions used by  inet:gethostbyaddr/1 \&. 
.RE
.LP
.B
gethostbyname(Name) -> {ok,hostent()} | Reason}
.br
.B
gethostbyname(Name, Family) -> {ok,hostent()} | {error,Reason}} 
.br
.B
gethostbyname(Name, Family, Timeout) -> {ok,hostent()} | {error,Reason} 
.br
.RS
.TP
Types
Name = dns_name()
.br
Timeout = integer() >= 0 | infinity
.br
Reason = posix() | res_error()
.br
.RE
.RS
.LP
Backend functions used by  inet:gethostbyname/1,2 \&. 
.LP
This function uses the resolver option \fIsearch\fR just like getbyname/2,3\&. 
.LP
If the resolver option \fIinet6\fR is \fItrue\fR, an IPv6 address is looked up, and if that fails the IPv4 address is looked up and returned on IPv6 mapped IPv4 format\&. 
.RE
.LP
.B
lookup(Name, Class, Type) -> [ dns_data() ] 
.br
.B
lookup(Name, Class, Type, Opts) -> [ dns_data() ] 
.br
.B
lookup(Name, Class, Type, Opts, Timeout) -> [ dns_data() ] 
.br
.RS
.TP
Types
Name = dns_name() | ip_address()
.br
Type = rr_type()
.br
Opts = res_option() | verbose
.br
Timeout = integer() >= 0 | infinity
.br
Reason = posix() | res_error()
.br
.RE
.RS
.LP
Resolve the DNS data for the record of the given type and class for the given name\&. On success filters out the answer records with the correct \fIClass\fR and \fIType\fR and returns a list of their data fields\&. So a lookup for type \fIany\fR will give an empty answer since the answer records have specific types that are not \fIany\fR\&. An empty answer as well as a failed lookup returns an empty list\&. 
.LP
Calls resolve/2\&.\&.4 with the same arguments and filters the result, so \fIOpts\fR is explained there\&. 
.RE
.LP
.B
resolve(Name, Class, Type) -> {ok,dns_msg()} | Error 
.br
.B
resolve(Name, Class, Type, Opts) -> {ok,dns_msg()} | Error 
.br
.B
resolve(Name, Class, Type, Opts, Timeout) -> {ok,dns_msg()} | Error 
.br
.RS
.TP
Types
Name = dns_name() | ip_address()
.br
Type = rr_type()
.br
Opts = res_option() | verbose | atom()
.br
Timeout = integer() >= 0 | infinity
.br
Error = {error, Reason} | {error, {Reason, dns_msg()}}
.br
Reason = posix() | res_error()
.br
.RE
.RS
.LP
Resolve a DNS record of the given type and class for the given name\&. The returned \fIdns_msg()\fR can be examined using access functions in \fIinet_db\fR as described in DNS types\&. 
.LP
If \fIName\fR is an \fIip_address()\fR, the domain name to query for is generated as the standard reverse "\&.IN-ADDR\&.ARPA\&." name for an IPv4 address, or the "\&.IP6\&.ARPA\&." name for an IPv6 address\&. In this case you most probably want to use \fIClass = in\fR and \fIType = ptr\fR but it is not done automatically\&. 
.LP
\fIOpts\fR override the corresponding resolver options\&. If the option \fInameservers\fR is given, it is also assumed that it is the complete list of nameserves, so the resolver option \fIalt_nameserves\fR is ignored\&. Of course, if that option is also given to this function, it is used\&. 
.LP
The \fIverbose\fR option (or rather \fI{verbose, true}\fR), causes diagnostics printout through io:format/2 of queries, replies retransmissions, etc, similar to from utilities like \fIdig\fR, \fInslookup\fR et\&.al\&. 
.LP
If \fIOpt\fR is an arbitrary atom it is interpreted as \fI{Opt, true}\fR unless the atom string starts with \fI"no"\fR making the interpretation \fI{Opt, false}\fR\&. For example: \fIusevc\fR is an alias for \fI{usevc, true}\fR, and \fInousevc\fR an alias for \fI{usevc, false}\fR\&. 
.LP
The \fIinet6\fR option currently has no effect on this function\&. You probably want to use \fIType = a | aaaa\fR instead\&. 
.RE
.SH EXAMPLES
.LP
Access functions example: how lookup/3 could have been implemented using resolve/3 from outside the module\&. 

.nf
    example_lookup(Name, Class, Type) ->
        case inet_res:resolve(Name, Class, Type) of
            {ok,Msg} ->
                [inet_dns:rr(RR, data)
                 || RR <- inet_dns:msg(Msg, anlist),
                    inet_dns:rr(RR, type) =:= Type,
                    inet_dns:rr(RR, class) =:= Class];
            {error,_} ->
                []
        end\&.
.fi
.SH LEGACY FUNCTIONS
.LP
These have been deprecated due to the annoying double meaning of the nameservers/timeout argument, and because they had no decent place for a resolver options list\&.
.SH EXPORTS
.LP
.B
nslookup(Name, Class, Type) -> {ok,dns_msg()} | {error,Reason} 
.br
.B
nslookup(Name, Class, Type, Timeout) -> {ok,dns_msg()} | {error,Reason} 
.br
.B
nslookup(Name, Class, Type, Nameservers) -> {ok,dns_msg()} | {error,Reason} 
.br
.RS
.TP
Types
Name = dns_name() | ip_address()
.br
Type = rr_type()
.br
Nameservers = [ nameserver() ]
.br
Timeout = integer() >= 0 | infinity
.br
Reason = posix() | res_error()
.br
.RE
.RS
.LP
Resolve a DNS record of the given type and class for the given name\&. 
.RE
.LP
.B
nnslookup(Name, Class, Type, Nameservers) -> {ok,dns_msg()} | {error,posix()} 
.br
.B
nnslookup(Name, Class, Type, Nameservers, Timeout) -> {ok,dns_msg()} | {error,posix()} 
.br
.RS
.TP
Types
Name = dns_name() | ip_address()
.br
Type = rr_type()
.br
Nameservers = [ nameserver() ]
.br
Timeout = integer() >= 0 | infinity
.br
Reason = posix() | res_error()
.br
.RE
.RS
.LP
Resolve a DNS record of the given type and class for the given name\&. 
.RE
