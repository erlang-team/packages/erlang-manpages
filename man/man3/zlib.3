.TH zlib 3 "erts  5.7.3" "Ericsson AB" "ERLANG MODULE DEFINITION"
.SH MODULE
zlib \- Zlib Compression interface\&.
.SH DESCRIPTION
.LP
The zlib module provides an API for the zlib library (http://www\&.zlib\&.org)\&. It is used to compress and decompress data\&. The data format is described by RFCs 1950 to 1952\&.
.LP
A typical (compress) usage looks like:

.nf
Z = zlib:open(),
ok = zlib:deflateInit(Z,default),

Compress = fun(end_of_data, _Cont) -> [];
              (Data, Cont) ->
                 [zlib:deflate(Z, Data)|Cont(Read(),Cont)]
           end,
Compressed = Compress(Read(),Compress),
Last = zlib:deflate(Z, [], finish),
ok = zlib:deflateEnd(Z),
zlib:close(Z),
list_to_binary([Compressed|Last])
.fi
.LP
In all functions errors, \fI{\&'EXIT\&', {Reason, Backtrace}}\fR, might be thrown, where \fIReason\fR describes the error\&. Typical reasons are:
.RS 2
.TP 4
.B
\fIbadarg\fR:
Bad argument
.TP 4
.B
\fIdata_error\fR:
The data contains errors
.TP 4
.B
\fIstream_error\fR:
Inconsistent stream state
.TP 4
.B
\fIeinval\fR:
Bad value or wrong function called
.TP 4
.B
\fI{need_dictionary, Adler32}\fR:
See \fIinflate/2\fR
.RE

.SH DATA TYPES

.nf
iodata = iolist() | binary()

iolist = [char() | binary() | iolist()]
  a binary is allowed as the tail of the list

zstream = a zlib stream, see open/0
.fi
.SH EXPORTS
.LP
.B
open() -> Z 
.br
.RS
.TP
Types
Z = zstream()
.br
.RE
.RS
.LP
Open a zlib stream\&.
.RE
.LP
.B
close(Z) -> ok
.br
.RS
.TP
Types
Z = zstream()
.br
.RE
.RS
.LP
Closes the stream referenced by \fIZ\fR\&.
.RE
.LP
.B
deflateInit(Z) -> ok
.br
.RS
.TP
Types
Z = zstream()
.br
.RE
.RS
.LP
Same as \fIzlib:deflateInit(Z, default)\fR\&.
.RE
.LP
.B
deflateInit(Z, Level) -> ok
.br
.RS
.TP
Types
Z = zstream()
.br
Level = none | default | best_speed | best_compression | 0\&.\&.9
.br
.RE
.RS
.LP
Initialize a zlib stream for compression\&.
.LP
\fILevel\fR decides the compression level to be used, 0 (\fInone\fR), gives no compression at all, 1 (\fIbest_speed\fR) gives best speed and 9 (\fIbest_compression\fR) gives best compression\&.
.RE
.LP
.B
deflateInit(Z, Level, Method, WindowBits, MemLevel, Strategy) -> ok
.br
.RS
.TP
Types
Z = zstream()
.br
Level = none | default | best_speed | best_compression | 0\&.\&.9
.br
Method = deflated
.br
WindowBits = 9\&.\&.15|-9\&.\&.-15
.br
MemLevel = 1\&.\&.9
.br
Strategy = default|filtered|huffman_only
.br
.RE
.RS
.LP
Initiates a zlib stream for compression\&.
.LP
The \fILevel\fR parameter decides the compression level to be used, 0 (\fInone\fR), gives no compression at all, 1 (\fIbest_speed\fR) gives best speed and 9 (\fIbest_compression\fR) gives best compression\&.
.LP
The \fIMethod\fR parameter decides which compression method to use, currently the only supported method is \fIdeflated\fR\&.
.LP
The \fIWindowBits\fR parameter is the base two logarithm of the window size (the size of the history buffer)\&. It should be in the range 9 through 15\&. Larger values of this parameter result in better compression at the expense of memory usage\&. The default value is 15 if \fIdeflateInit/2\fR\&. A negative \fIWindowBits\fR value suppresses the zlib header (and checksum) from the stream\&. Note that the zlib source mentions this only as a undocumented feature\&.
.LP
The \fIMemLevel\fR parameter specifies how much memory should be allocated for the internal compression state\&. \fIMemLevel\fR=1 uses minimum memory but is slow and reduces compression ratio; \fIMemLevel\fR=9 uses maximum memory for optimal speed\&. The default value is 8\&.
.LP
The \fIStrategy\fR parameter is used to tune the compression algorithm\&. Use the value \fIdefault\fR for normal data, \fIfiltered\fR for data produced by a filter (or predictor), or \fIhuffman_only\fR to force Huffman encoding only (no string match)\&. Filtered data consists mostly of small values with a somewhat random distribution\&. In this case, the compression algorithm is tuned to compress them better\&. The effect of \fIfiltered\fRis to force more Huffman coding and less string matching; it is somewhat intermediate between \fIdefault\fR and \fIhuffman_only\fR\&. The \fIStrategy\fR parameter only affects the compression ratio but not the correctness of the compressed output even if it is not set appropriately\&.
.RE
.LP
.B
deflate(Z, Data) -> Compressed
.br
.RS
.TP
Types
Z = zstream()
.br
Data = iodata()
.br
Compressed = iolist()
.br
.RE
.RS
.LP
Same as \fIdeflate(Z, Data, none)\fR\&.
.RE
.LP
.B
deflate(Z, Data, Flush) -> 
.br
.RS
.TP
Types
Z = zstream()
.br
Data = iodata()
.br
Flush = none | sync | full | finish
.br
Compressed = iolist()
.br
.RE
.RS
.LP
\fIdeflate/3\fR compresses as much data as possible, and stops when the input buffer becomes empty\&. It may introduce some output latency (reading input without producing any output) except when forced to flush\&.
.LP
If the parameter \fIFlush\fR is set to \fIsync\fR, all pending output is flushed to the output buffer and the output is aligned on a byte boundary, so that the decompressor can get all input data available so far\&. Flushing may degrade compression for some compression algorithms and so it should be used only when necessary\&.
.LP
If \fIFlush\fR is set to \fIfull\fR, all output is flushed as with \fIsync\fR, and the compression state is reset so that decompression can restart from this point if previous compressed data has been damaged or if random access is desired\&. Using \fIfull\fR too often can seriously degrade the compression\&.
.LP
If the parameter \fIFlush\fR is set to \fIfinish\fR, pending input is processed, pending output is flushed and \fIdeflate/3\fR returns\&. Afterwards the only possible operations on the stream are \fIdeflateReset/1\fR or \fIdeflateEnd/1\fR\&.
.LP
\fIFlush\fR can be set to \fIfinish\fR immediately after \fIdeflateInit\fR if all compression is to be done in one step\&.

.nf
zlib:deflateInit(Z),
B1 = zlib:deflate(Z,Data),
B2 = zlib:deflate(Z,<< >>,finish),
zlib:deflateEnd(Z),
list_to_binary([B1,B2])
.fi
.RE
.LP
.B
deflateSetDictionary(Z, Dictionary) -> Adler32
.br
.RS
.TP
Types
Z = zstream()
.br
Dictionary = binary()
.br
Adler32 = integer()
.br
.RE
.RS
.LP
Initializes the compression dictionary from the given byte sequence without producing any compressed output\&. This function must be called immediately after \fIdeflateInit/[1|2|6]\fR or \fIdeflateReset/1\fR, before any call of \fIdeflate/3\fR\&. The compressor and decompressor must use exactly the same dictionary (see \fIinflateSetDictionary/2\fR)\&. The adler checksum of the dictionary is returned\&.
.RE
.LP
.B
deflateReset(Z) -> ok
.br
.RS
.TP
Types
Z = zstream()
.br
.RE
.RS
.LP
This function is equivalent to \fIdeflateEnd/1\fR followed by \fIdeflateInit/[1|2|6]\fR, but does not free and reallocate all the internal compression state\&. The stream will keep the same compression level and any other attributes\&.
.RE
.LP
.B
deflateParams(Z, Level, Strategy) -> ok 
.br
.RS
.TP
Types
Z = zstream()
.br
Level = none | default | best_speed | best_compression | 0\&.\&.9
.br
Strategy = default|filtered|huffman_only
.br
.RE
.RS
.LP
Dynamically update the compression level and compression strategy\&. The interpretation of \fILevel\fR and \fIStrategy\fR is as in \fIdeflateInit/6\fR\&. This can be used to switch between compression and straight copy of the input data, or to switch to a different kind of input data requiring a different strategy\&. If the compression level is changed, the input available so far is compressed with the old level (and may be flushed); the new level will take effect only at the next call of \fIdeflate/3\fR\&.
.LP
Before the call of deflateParams, the stream state must be set as for a call of \fIdeflate/3\fR, since the currently available input may have to be compressed and flushed\&.
.RE
.LP
.B
deflateEnd(Z) -> ok
.br
.RS
.TP
Types
Z = zstream()
.br
.RE
.RS
.LP
End the deflate session and cleans all data used\&. Note that this function will throw an \fIdata_error\fR exception if the last call to \fIdeflate/3\fR was not called with \fIFlush\fR set to \fIfinish\fR\&.
.RE
.LP
.B
inflateInit(Z) -> ok 
.br
.RS
.TP
Types
Z = zstream()
.br
.RE
.RS
.LP
Initialize a zlib stream for decompression\&.
.RE
.LP
.B
inflateInit(Z, WindowBits) -> ok 
.br
.RS
.TP
Types
Z = zstream()
.br
WindowBits = 9\&.\&.15|-9\&.\&.-15
.br
.RE
.RS
.LP
Initialize decompression session on zlib stream\&.
.LP
The \fIWindowBits\fR parameter is the base two logarithm of the maximum window size (the size of the history buffer)\&. It should be in the range 9 through 15\&. The default value is 15 if \fIinflateInit/1\fR is used\&. If a compressed stream with a larger window size is given as input, inflate() will throw the \fIdata_error\fR exception\&. A negative \fIWindowBits\fR value makes zlib ignore the zlib header (and checksum) from the stream\&. Note that the zlib source mentions this only as a undocumented feature\&.
.RE
.LP
.B
inflate(Z, Data) -> DeCompressed 
.br
.RS
.TP
Types
Z = zstream()
.br
Data = iodata()
.br
DeCompressed = iolist()
.br
.RE
.RS
.LP
\fIinflate/2\fR decompresses as much data as possible\&. It may some introduce some output latency (reading input without producing any output)\&.
.LP
If a preset dictionary is needed at this point (see \fIinflateSetDictionary\fR below), \fIinflate/2\fR throws a \fI{need_dictionary, Adler}\fR exception where \fIAdler\fR is the adler32 checksum of the dictionary chosen by the compressor\&.
.RE
.LP
.B
inflateSetDictionary(Z, Dictionary) -> ok
.br
.RS
.TP
Types
Z = zstream()
.br
Dictionary = binary()
.br
.RE
.RS
.LP
Initializes the decompression dictionary from the given uncompressed byte sequence\&. This function must be called immediately after a call of \fIinflate/2\fR if this call threw a \fI{need_dictionary, Adler}\fR exception\&. The dictionary chosen by the compressor can be determined from the Adler value thrown by the call to \fIinflate/2\fR\&. The compressor and decompressor must use exactly the same dictionary (see \fIdeflateSetDictionary/2\fR)\&.
.LP
Example:

.nf
unpack(Z, Compressed, Dict) ->
     case catch zlib:inflate(Z, Compressed) of
          {\&'EXIT\&',{{need_dictionary,DictID},_}} ->
                 zlib:inflateSetDictionary(Z, Dict),
                 Uncompressed = zlib:inflate(Z, []);
          Uncompressed ->
                 Uncompressed
     end\&.
.fi
.RE
.LP
.B
inflateReset(Z) -> ok
.br
.RS
.TP
Types
Z = zstream()
.br
.RE
.RS
.LP
This function is equivalent to \fIinflateEnd/1\fR followed by \fIinflateInit/1\fR, but does not free and reallocate all the internal decompression state\&. The stream will keep attributes that may have been set by \fIinflateInit/[1|2]\fR\&.
.RE
.LP
.B
inflateEnd(Z) -> ok
.br
.RS
.TP
Types
Z = zstream()
.br
.RE
.RS
.LP
End the inflate session and cleans all data used\&. Note that this function will throw a \fIdata_error\fR exception if no end of stream was found (meaning that not all data has been uncompressed)\&.
.RE
.LP
.B
setBufSize(Z, Size) -> ok
.br
.RS
.TP
Types
Z = zstream()
.br
Size = integer()
.br
.RE
.RS
.LP
Sets the intermediate buffer size\&.
.RE
.LP
.B
getBufSize(Z) -> Size
.br
.RS
.TP
Types
Z = zstream()
.br
Size = integer()
.br
.RE
.RS
.LP
Get the size of intermediate buffer\&.
.RE
.LP
.B
crc32(Z) -> CRC
.br
.RS
.TP
Types
Z = zstream()
.br
CRC = integer()
.br
.RE
.RS
.LP
Get the current calculated CRC checksum\&.
.RE
.LP
.B
crc32(Z, Binary) -> CRC
.br
.RS
.TP
Types
Z = zstream()
.br
Binary = binary()
.br
CRC = integer()
.br
.RE
.RS
.LP
Calculate the CRC checksum for \fIBinary\fR\&.
.RE
.LP
.B
crc32(Z, PrevCRC, Binary) -> CRC 
.br
.RS
.TP
Types
Z = zstream()
.br
PrevCRC = integer()
.br
Binary = binary()
.br
CRC = integer()
.br
.RE
.RS
.LP
Update a running CRC checksum for \fIBinary\fR\&. If \fIBinary\fR is the empty binary, this function returns the required initial value for the crc\&.

.nf
Crc = lists:foldl(fun(Bin,Crc0) ->  
                      zlib:crc32(Z, Crc0, Bin),
                  end, zlib:crc32(Z,<< >>), Bins)
.fi
.RE
.LP
.B
crc32_combine(Z, CRC1, CRC2, Size2) -> CRC 
.br
.RS
.TP
Types
Z = zstream()
.br
CRC = integer()
.br
CRC1 = integer()
.br
CRC2 = integer()
.br
Size2 = integer()
.br
.RE
.RS
.LP
Combine two CRC checksums into one\&. For two binaries, \fIBin1\fR and \fIBin2\fR with sizes of \fISize1\fR and \fISize2\fR, with CRC checksums \fICRC1\fR and \fICRC2\fR\&. \fIcrc32_combine/4\fR returns the \fICRC\fR checksum of \fI<<Bin1/binary, Bin2/binary>>\fR, requiring only \fICRC1\fR, \fICRC2\fR, and \fISize2\fR\&. 
.RE
.LP
.B
adler32(Z, Binary) -> Checksum
.br
.RS
.TP
Types
Z = zstream()
.br
Binary = binary()
.br
Checksum = integer()
.br
.RE
.RS
.LP
Calculate the Adler-32 checksum for \fIBinary\fR\&.
.RE
.LP
.B
adler32(Z, PrevAdler, Binary) -> Checksum
.br
.RS
.TP
Types
Z = zstream()
.br
PrevAdler = integer()
.br
Binary = binary()
.br
Checksum = integer()
.br
.RE
.RS
.LP
Update a running Adler-32 checksum for \fIBinary\fR\&. If \fIBinary\fR is the empty binary, this function returns the required initial value for the checksum\&.

.nf
Crc = lists:foldl(fun(Bin,Crc0) ->  
                      zlib:adler32(Z, Crc0, Bin),
                  end, zlib:adler32(Z,<< >>), Bins)
.fi
.RE
.LP
.B
adler32_combine(Z, Adler1, Adler2, Size2) -> Adler 
.br
.RS
.TP
Types
Z = zstream()
.br
Adler = integer()
.br
Adler1 = integer()
.br
Adler2 = integer()
.br
Size2 = integer()
.br
.RE
.RS
.LP
Combine two Adler-32 checksums into one\&. For two binaries, \fIBin1\fR and \fIBin2\fR with sizes of \fISize1\fR and \fISize2\fR, with Adler-32 checksums \fIAdler1\fR and \fIAdler2\fR\&. \fIadler32_combine/4\fR returns the \fIAdler\fR checksum of \fI<<Bin1/binary, Bin2/binary>>\fR, requiring only \fIAdler1\fR, \fIAdler2\fR, and \fISize2\fR\&. 
.RE
.LP
.B
compress(Binary) -> Compressed 
.br
.RS
.TP
Types
Binary = Compressed = binary()
.br
.RE
.RS
.LP
Compress a binary (with zlib headers and checksum)\&.
.RE
.LP
.B
uncompress(Binary) -> Decompressed
.br
.RS
.TP
Types
Binary = Decompressed = binary()
.br
.RE
.RS
.LP
Uncompress a binary (with zlib headers and checksum)\&.
.RE
.LP
.B
zip(Binary) -> Compressed
.br
.RS
.TP
Types
Binary = Compressed = binary()
.br
.RE
.RS
.LP
Compress a binary (without zlib headers and checksum)\&.
.RE
.LP
.B
unzip(Binary) -> Decompressed
.br
.RS
.TP
Types
Binary = Decompressed = binary()
.br
.RE
.RS
.LP
Uncompress a binary (without zlib headers and checksum)\&.
.RE
.LP
.B
gzip(Data) -> Compressed
.br
.RS
.TP
Types
Binary = Compressed = binary()
.br
.RE
.RS
.LP
Compress a binary (with gz headers and checksum)\&.
.RE
.LP
.B
gunzip(Bin) -> Decompressed
.br
.RS
.TP
Types
Binary = Decompressed = binary()
.br
.RE
.RS
.LP
Uncompress a binary (with gz headers and checksum)\&.
.RE
