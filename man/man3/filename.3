.TH filename 3 "stdlib  1.16.3" "Ericsson AB" "ERLANG MODULE DEFINITION"
.SH MODULE
filename \- Filename Manipulation Functions
.SH DESCRIPTION
.LP
The module \fIfilename\fR provides a number of useful functions for analyzing and manipulating file names\&. These functions are designed so that the Erlang code can work on many different platforms with different formats for file names\&. With file name is meant all strings that can be used to denote a file\&. They can be short relative names like \fIfoo\&.erl\fR, very long absolute name which include a drive designator and directory names like \fID:\eusr/local\ebin\eerl/lib\etools\efoo\&.erl\fR, or any variations in between\&.
.LP
In Windows, all functions return file names with forward slashes only, even if the arguments contain back slashes\&. Use \fIjoin/1\fR to normalize a file name by removing redundant directory separators\&.

.SH DATA TYPES

.nf
name() = string() | atom() | DeepList
  DeepList = [char() | atom() | DeepList]
.fi
.SH EXPORTS
.LP
.B
absname(Filename) -> string()
.br
.RS
.TP
Types
Filename = name()
.br
.RE
.RS
.LP
Converts a relative \fIFilename\fR and returns an absolute name\&. No attempt is made to create the shortest absolute name, because this can give incorrect results on file systems which allow links\&.
.LP
Unix examples:

.nf
1> pwd()\&.

"/usr/local"
2> filename:absname("foo")\&.

"/usr/local/foo"
3> filename:absname("\&.\&./x")\&.

"/usr/local/\&.\&./x"
4> filename:absname("/")\&.

"/"
.fi
.LP
Windows examples:

.nf
1> pwd()\&.

"D:/usr/local"
2> filename:absname("foo")\&.

"D:/usr/local/foo"
3> filename:absname("\&.\&./x")\&.

"D:/usr/local/\&.\&./x"
4> filename:absname("/")\&.

"D:/"
.fi
.RE
.LP
.B
absname(Filename, Dir) -> string()
.br
.RS
.TP
Types
Filename = name()
.br
Dir = string()
.br
.RE
.RS
.LP
This function works like \fIabsname/1\fR, except that the directory to which the file name should be made relative is given explicitly in the \fIDir\fR argument\&.
.RE
.LP
.B
absname_join(Dir, Filename) -> string()
.br
.RS
.TP
Types
Dir = string()
.br
Filename = name()
.br
.RE
.RS
.LP
Joins an absolute directory with a relative filename\&. Similar to \fIjoin/2\fR, but on platforms with tight restrictions on raw filename length and no support for symbolic links (read: VxWorks), leading parent directory components in \fIFilename\fR are matched against trailing directory components in \fIDir\fR so they can be removed from the result - minimizing its length\&.
.RE
.LP
.B
basename(Filename) -> string()
.br
.RS
.TP
Types
Filename = name()
.br
.RE
.RS
.LP
Returns the last component of \fIFilename\fR, or \fIFilename\fR itself if it does not contain any directory separators\&.

.nf
5> filename:basename("foo")\&.

"foo"
6> filename:basename("/usr/foo")\&.

"foo"
7> filename:basename("/")\&.

[]
.fi
.RE
.LP
.B
basename(Filename, Ext) -> string()
.br
.RS
.TP
Types
Filename = Ext = name()
.br
.RE
.RS
.LP
Returns the last component of \fIFilename\fR with the extension \fIExt\fR stripped\&. This function should be used to remove a specific extension which might, or might not, be there\&. Use \fIrootname(basename(Filename))\fR to remove an extension that exists, but you are not sure which one it is\&.

.nf
8> filename:basename("~/src/kalle\&.erl", "\&.erl")\&.

"kalle"
9> filename:basename("~/src/kalle\&.beam", "\&.erl")\&.

"kalle\&.beam"
10> filename:basename("~/src/kalle\&.old\&.erl", "\&.erl")\&.

"kalle\&.old"
11> filename:rootname(filename:basename("~/src/kalle\&.erl"))\&.

"kalle"
12> filename:rootname(filename:basename("~/src/kalle\&.beam"))\&.

"kalle"
.fi
.RE
.LP
.B
dirname(Filename) -> string()
.br
.RS
.TP
Types
Filename = name()
.br
.RE
.RS
.LP
Returns the directory part of \fIFilename\fR\&.

.nf
13> filename:dirname("/usr/src/kalle\&.erl")\&.

"/usr/src"
14> filename:dirname("kalle\&.erl")\&.

"\&."

5> filename:dirname("\e\eusr\e\esrc/kalle\&.erl")\&.
 % Windows
"/usr/src"
.fi
.RE
.LP
.B
extension(Filename) -> string()
.br
.RS
.TP
Types
Filename = name()
.br
.RE
.RS
.LP
Returns the file extension of \fIFilename\fR, including the period\&. Returns an empty string if there is no extension\&.

.nf
15> filename:extension("foo\&.erl")\&.

"\&.erl"
16> filename:extension("beam\&.src/kalle")\&.

[]
.fi
.RE
.LP
.B
flatten(Filename) -> string()
.br
.RS
.TP
Types
Filename = name()
.br
.RE
.RS
.LP
Converts a possibly deep list filename consisting of characters and atoms into the corresponding flat string filename\&.
.RE
.LP
.B
join(Components) -> string()
.br
.RS
.TP
Types
Components = [string()]
.br
.RE
.RS
.LP
Joins a list of file name \fIComponents\fR with directory separators\&. If one of the elements of \fIComponents\fR includes an absolute path, for example \fI"/xxx"\fR, the preceding elements, if any, are removed from the result\&.
.LP
The result is "normalized":
.RS 2
.TP 2
*
Redundant directory separators are removed\&.
.TP 2
*
In Windows, all directory separators are forward slashes and the drive letter is in lower case\&.
.RE

.nf
17> filename:join(["/usr", "local", "bin"])\&.

"/usr/local/bin"
18> filename:join(["a/b///c/"])\&.

"a/b/c"

6> filename:join(["B:a\e\eb///c/"])\&.
 % Windows
"b:a/b/c"
.fi
.RE
.LP
.B
join(Name1, Name2) -> string()
.br
.RS
.TP
Types
Name1 = Name2 = string()
.br
.RE
.RS
.LP
Joins two file name components with directory separators\&. Equivalent to \fIjoin([Name1, Name2])\fR\&.
.RE
.LP
.B
nativename(Path) -> string()
.br
.RS
.TP
Types
Path = string()
.br
.RE
.RS
.LP
Converts \fIPath\fR to a form accepted by the command shell and native applications on the current platform\&. On Windows, forward slashes is converted to backward slashes\&. On all platforms, the name is normalized as done by \fIjoin/1\fR\&.

.nf
19> filename:nativename("/usr/local/bin/")\&.
 % Unix
"/usr/local/bin"

7> filename:nativename("/usr/local/bin/")\&.
 % Windows
"\e\eusr\e\elocal\e\ebin"
.fi
.RE
.LP
.B
pathtype(Path) -> absolute | relative | volumerelative
.br
.RS
.LP
Returns the type of path, one of \fIabsolute\fR, \fIrelative\fR, or \fIvolumerelative\fR\&.
.RS 2
.TP 4
.B
\fIabsolute\fR:
The path name refers to a specific file on a specific volume\&.
.RS 4
.LP

.LP
Unix example: \fI/usr/local/bin\fR
.LP

.LP
Windows example: \fID:/usr/local/bin\fR
.RE
.TP 4
.B
\fIrelative\fR:
The path name is relative to the current working directory on the current volume\&.
.RS 4
.LP

.LP
Example: \fIfoo/bar, \&.\&./src\fR
.RE
.TP 4
.B
\fIvolumerelative\fR:
The path name is relative to the current working directory on a specified volume, or it is a specific file on the current working volume\&.
.RS 4
.LP

.LP
Windows example: \fID:bar\&.erl, /bar/foo\&.erl\fR
.RE
.RE
.RE
.LP
.B
rootname(Filename) -> string()
.br
.B
rootname(Filename, Ext) -> string()
.br
.RS
.TP
Types
Filename = Ext = name()
.br
.RE
.RS
.LP
Remove a filename extension\&. \fIrootname/2\fR works as \fIrootname/1\fR, except that the extension is removed only if it is \fIExt\fR\&.

.nf
20> filename:rootname("/beam\&.src/kalle")\&.

/beam\&.src/kalle"
21> filename:rootname("/beam\&.src/foo\&.erl")\&.

"/beam\&.src/foo"
22> filename:rootname("/beam\&.src/foo\&.erl", "\&.erl")\&.

"/beam\&.src/foo"
23> filename:rootname("/beam\&.src/foo\&.beam", "\&.erl")\&.

"/beam\&.src/foo\&.beam"
.fi
.RE
.LP
.B
split(Filename) -> Components
.br
.RS
.TP
Types
Filename = name()
.br
Components = [string()]
.br
.RE
.RS
.LP
Returns a list whose elements are the path components of \fIFilename\fR\&.

.nf
24> filename:split("/usr/local/bin")\&.

["/","usr","local","bin"]
25> filename:split("foo/bar")\&.

["foo","bar"]
26> filename:split("a:\e\emsdev\e\einclude")\&.

["a:/","msdev","include"]
.fi
.RE
.LP
.B
find_src(Beam) -> {SourceFile, Options} | {error,{ErrorReason,Module}}
.br
.B
find_src(Beam, Rules) -> {SourceFile, Options} | {error,{ErrorReason,Module}}
.br
.RS
.TP
Types
Beam = Module | Filename
.br
 Module = atom()
.br
 Filename = string() | atom()
.br
SourceFile = string()
.br
Options = [Opt]
.br
 Opt = {i, string()} | {outdir, string()} | {d, atom()}
.br
ErrorReason = non_existing | preloaded | interpreted
.br
.RE
.RS
.LP
Finds the source filename and compiler options for a module\&. The result can be fed to \fIcompile:file/2\fR in order to compile the file again\&.
.LP
The \fIBeam\fR argument, which can be a string or an atom, specifies either the module name or the path to the source code, with or without the \fI"\&.erl"\fR extension\&. In either case, the module must be known by the code server, i\&.e\&. \fIcode:which(Module)\fR must succeed\&.
.LP
\fIRules\fR describes how the source directory can be found, when the object code directory is known\&. It is a list of tuples \fI{BinSuffix, SourceSuffix}\fR and is interpreted as follows: If the end of the directory name where the object is located matches \fIBinSuffix\fR, then the source code directory has the same name, but with \fIBinSuffix\fR replaced by \fISourceSuffix\fR\&. \fIRules\fR defaults to:

.nf
[{"", ""}, {"ebin", "src"}, {"ebin", "esrc"}]
.fi
.LP
If the source file is found in the resulting directory, then the function returns that location together with \fIOptions\fR\&. Otherwise, the next rule is tried, and so on\&.
.LP
The function returns \fI{SourceFile, Options}\fR if it succeeds\&. \fISourceFile\fR is the absolute path to the source file without the \fI"\&.erl"\fR extension\&. \fIOptions\fR include the options which are necessary to recompile the file with \fIcompile:file/2\fR, but excludes options such as \fIreport\fR or \fIverbose\fR which do not change the way code is generated\&. The paths in the \fI{outdir, Path}\fR and \fI{i, Path}\fR options are guaranteed to be absolute\&.
.RE
