.TH erl_scan 3 "stdlib  1.16.3" "Ericsson AB" "ERLANG MODULE DEFINITION"
.SH MODULE
erl_scan \- The Erlang Token Scanner
.SH DESCRIPTION
.LP
This module contains functions for tokenizing characters into Erlang tokens\&.

.SH DATA TYPES

.nf
category() = atom()
column() = integer() > 0
line() = integer()
location() = line() | {line(), column()}
reserved_word_fun() -> fun(atom()) -> bool()
set_attribute_fun() -> fun(term()) -> term()
symbol() = atom() | float() | integer() | string()
token() = {category(), attributes()} | {category(), attributes(), symbol()}
attributes() = line() | list() | tuple()
.fi
.SH EXPORTS
.LP
.B
string(String) -> Return
.br
.B
string(String, StartLocation) -> Return
.br
.B
string(String, StartLocation, Options) -> Return
.br
.RS
.TP
Types
String = string()
.br
Return = {ok, Tokens, EndLocation} | Error
.br
Tokens = [token()]
.br
Error = {error, ErrorInfo, EndLocation}
.br
StartLocation = EndLocation = location()
.br
Options = Option | [Option]
.br
Option = {reserved_word_fun, reserved_word_fun()} | return_comments | return_white_spaces | return | text
.br
.RE
.RS
.LP
Takes the list of characters \fIString\fR and tries to scan (tokenize) them\&. Returns \fI{ok, Tokens, EndLocation}\fR, where \fITokens\fR are the Erlang tokens from \fIString\fR\&. \fIEndLocation\fR is the first location after the last token\&.
.LP
\fI{error, ErrorInfo, EndLocation}\fR is returned if an error occurs\&. \fIEndLocation\fR is the first location after the erroneous token\&.
.LP
\fIstring(String)\fR is equivalent to \fIstring(String, 1)\fR, and \fIstring(String, StartLocation)\fR is equivalent to \fIstring(String, StartLocation, [])\fR\&.
.LP
\fIStartLocation\fR indicates the initial location when scanning starts\&. If \fIStartLocation\fR is a line \fIattributes()\fR as well as \fIEndLocation\fR and \fIErrorLocation\fR will be lines\&. If \fIStartLocation\fR is a pair of a line and a column \fIattributes()\fR takes the form of an opaque compound data type, and \fIEndLocation\fR and \fIErrorLocation\fR will be pairs of a line and a column\&. The \fItoken attributes\fR contain information about the column and the line where the token begins, as well as the text of the token (if the \fItext\fR option is given), all of which can be accessed by calling token_info/1,2 or attributes_info/1,2\&.
.LP
A \fItoken\fR is a tuple containing information about syntactic category, the token attributes, and the actual terminal symbol\&. For punctuation characters (e\&.g\&. \fI;\fR, \fI|\fR) and reserved words, the category and the symbol coincide, and the token is represented by a two-tuple\&. Three-tuples have one of the following forms: \fI{atom, Info, atom()}\fR, \fI{char, Info, integer()}\fR, \fI{comment, Info, string()}\fR, \fI{float, Info, float()}\fR, \fI{integer, Info, integer()}\fR, \fI{var, Info, atom()}\fR, and \fI{white_space, Info, string()}\fR\&.
.LP
The valid options are:
.RS 2
.TP 4
.B
\fI{reserved_word_fun, reserved_word_fun()}\fR:
A callback function that is called when the scanner has found an unquoted atom\&. If the function returns \fItrue\fR, the unquoted atom itself will be the category of the token; if the function returns \fIfalse\fR, \fIatom\fR will be the category of the unquoted atom\&.
.TP 4
.B
\fIreturn_comments\fR:
Return comment tokens\&.
.TP 4
.B
\fIreturn_white_spaces\fR:
Return white space tokens\&. By convention, if there is a newline character, it is always the first character of the text (there cannot be more than one newline in a white space token)\&.
.TP 4
.B
\fIreturn\fR:
Short for \fI[return_comments, return_white_spaces]\fR\&.
.TP 4
.B
\fItext\fR:
Include the token\&'s text in the token attributes\&. The text is the part of the input corresponding to the token\&.
.RE
.RE
.LP
.B
tokens(Continuation, CharSpec, StartLocation) -> Return
.br
.B
tokens(Continuation, CharSpec, StartLocation, Options) -> Return
.br
.RS
.TP
Types
Continuation = [] | Continuation1
.br
Return = {done, Result, LeftOverChars} | {more, Continuation1}
.br
LeftOverChars = CharSpec
.br
CharSpec = string() | eof
.br
Continuation1 = tuple()
.br
Result = {ok, Tokens, EndLocation} | {eof, EndLocation} | Error
.br
Tokens = [token()]
.br
Error = {error, ErrorInfo, EndLocation}
.br
StartLocation = EndLocation = location()
.br
Options = Option | [Option]
.br
Option = {reserved_word_fun, reserved_word_fun()} | return_comments | return_white_spaces | return
.br
.RE
.RS
.LP
This is the re-entrant scanner which scans characters until a \fIdot\fR (\&'\&.\&' followed by a white space) or \fIeof\fR has been reached\&. It returns:
.RS 2
.TP 4
.B
\fI{done, Result, LeftOverChars}\fR:
This return indicates that there is sufficient input data to get a result\&. \fIResult\fR is:
.RS 4
.LP

.RS 2
.TP 4
.B
\fI{ok, Tokens, EndLocation}\fR:
The scanning was successful\&. \fITokens\fR is the list of tokens including \fIdot\fR\&.
.TP 4
.B
\fI{eof, EndLocation}\fR:
End of file was encountered before any more tokens\&.
.TP 4
.B
\fI{error, ErrorInfo, EndLocation}\fR:
An error occurred\&. \fILeftOverChars\fR is the remaining characters of the input data, starting from \fIEndLocation\fR\&.
.RE
.RE
.TP 4
.B
\fI{more, Continuation1}\fR:
More data is required for building a term\&. \fIContinuation1\fR must be passed in a new call to \fItokens/3, 4\fR when more data is available\&.
.RE
.LP
The \fICharSpec\fR \fIeof\fR signals end of file\&. \fILeftOverChars\fR will then take the value \fIeof\fR as well\&.
.LP
\fItokens(Continuation, CharSpec, StartLocation)\fR is equivalent to \fItokens(Continuation, CharSpec, StartLocation, [])\fR\&.
.LP
See string/3 for a description of the various options\&.
.RE
.LP
.B
reserved_word(Atom) -> bool()
.br
.RS
.TP
Types
Atom = atom()
.br
.RE
.RS
.LP
Returns \fItrue\fR if \fIAtom\fR is an Erlang reserved word, otherwise \fIfalse\fR\&.
.RE
.LP
.B
token_info(Token) -> TokenInfo
.br
.RS
.TP
Types
Token = token()
.br
TokenInfo = [TokenInfoTuple]
.br
TokenInfoTuple = {TokenItem, Info}
.br
TokenItem = atom()
.br
Info = term()
.br
.RE
.RS
.LP
Returns a list containing information about the token \fIToken\fR\&. The order of the \fITokenInfoTuple\fRs is not defined\&. The following \fITokenItem\fRs are returned: \fIcategory\fR, \fIcolumn\fR, \fIlength\fR, \fIline\fR, \fIsymbol\fR, and \fItext\fR\&. See token_info/2 for information about specific \fITokenInfoTuple\fRs\&.
.LP
Note that if \fItoken_info(Token, TokenItem)\fR returns \fIundefined\fR for some \fITokenItem\fR in the list above, the item is not included in \fITokenInfo\fR\&.
.RE
.LP
.B
token_info(Token, TokenItemSpec) -> TokenInfo
.br
.RS
.TP
Types
Token = token()
.br
TokenItemSpec = TokenItem | [TokenItem]
.br
TokenInfo = TokenInfoTuple | undefined | [TokenInfoTuple]
.br
TokenInfoTuple = {TokenItem, Info}
.br
TokenItem = atom()
.br
Info = term()
.br
.RE
.RS
.LP
Returns a list containing information about the token \fIToken\fR\&. If \fITokenItemSpec\fR is a single \fITokenItem\fR, the returned value is the corresponding \fITokenInfoTuple\fR, or \fIundefined\fR if the \fITokenItem\fR has no value\&. If \fITokenItemSpec\fR is a list of \fITokenItem\fR, the result is a list of \fITokenInfoTuple\fR\&. The \fITokenInfoTuple\fRs will appear with the corresponding \fITokenItem\fRs in the same order as the \fITokenItem\fRs appeared in the list of \fITokenItem\fRs\&. \fITokenItem\fRs with no value are not included in the list of \fITokenInfoTuple\fR\&.
.LP
The following \fITokenInfoTuple\fRs with corresponding \fITokenItem\fRs are valid:
.RS 2
.TP 4
.B
\fI{category, category()}\fR:
The category of the token\&.
.TP 4
.B
\fI{column, column()}\fR:
The column where the token begins\&.
.TP 4
.B
\fI{length, integer() > 0}\fR:
The length of the token\&'s text\&.
.TP 4
.B
\fI{line, line()}\fR:
The line where the token begins\&.
.TP 4
.B
\fI{location, location()}\fR:
The line and column where the token begins, or just the line if the column unknown\&.
.TP 4
.B
\fI{symbol, symbol()}\fR:
The token\&'s symbol\&.
.TP 4
.B
\fI{text, string()}\fR:
The token\&'s text\&.\&.
.RE
.RE
.LP
.B
attributes_info(Attributes) -> AttributesInfo
.br
.RS
.TP
Types
Attributes = attributes()
.br
AttributesInfo = [AttributeInfoTuple]
.br
AttributeInfoTuple = {AttributeItem, Info}
.br
AttributeItem = atom()
.br
Info = term()
.br
.RE
.RS
.LP
Returns a list containing information about the token attributes \fIAttributes\fR\&. The order of the \fIAttributeInfoTuple\fRs is not defined\&. The following \fIAttributeItem\fRs are returned: \fIcolumn\fR, \fIlength\fR, \fIline\fR, and \fItext\fR\&. See attributes_info/2 for information about specific \fIAttributeInfoTuple\fRs\&.
.LP
Note that if \fIattributes_info(Token, AttributeItem)\fR returns \fIundefined\fR for some \fIAttributeItem\fR in the list above, the item is not included in \fIAttributesInfo\fR\&.
.RE
.LP
.B
attributes_info(Attributes, AttributeItemSpec) -> AttributesInfo
.br
.RS
.TP
Types
Attributes = attributes()
.br
AttributeItemSpec = AttributeItem | [AttributeItem]
.br
AttributesInfo = AttributeInfoTuple | undefined | [AttributeInfoTuple]
.br
AttributeInfoTuple = {AttributeItem, Info}
.br
AttributeItem = atom()
.br
Info = term()
.br
.RE
.RS
.LP
Returns a list containing information about the token attributes \fIAttributes\fR\&. If \fIAttributeItemSpec\fR is a single \fIAttributeItem\fR, the returned value is the corresponding \fIAttributeInfoTuple\fR, or \fIundefined\fR if the \fIAttributeItem\fR has no value\&. If \fIAttributeItemSpec\fR is a list of \fIAttributeItem\fR, the result is a list of \fIAttributeInfoTuple\fR\&. The \fIAttributeInfoTuple\fRs will appear with the corresponding \fIAttributeItem\fRs in the same order as the \fIAttributeItem\fRs appeared in the list of \fIAttributeItem\fRs\&. \fIAttributeItem\fRs with no value are not included in the list of \fIAttributeInfoTuple\fR\&.
.LP
The following \fIAttributeInfoTuple\fRs with corresponding \fIAttributeItem\fRs are valid:
.RS 2
.TP 4
.B
\fI{column, column()}\fR:
The column where the token begins\&.
.TP 4
.B
\fI{length, integer() > 0}\fR:
The length of the token\&'s text\&.
.TP 4
.B
\fI{line, line()}\fR:
The line where the token begins\&.
.TP 4
.B
\fI{location, location()}\fR:
The line and column where the token begins, or just the line if the column unknown\&.
.TP 4
.B
\fI{text, string()}\fR:
The token\&'s text\&.\&.
.RE
.RE
.LP
.B
set_attribute(AttributeItem, Attributes, SetAttributeFun) -> AttributesInfo
.br
.RS
.TP
Types
AttributeItem = line
.br
Attributes = attributes()
.br
SetAttributeFun = set_attribute_fun()
.br
.RE
.RS
.LP
Sets the value of the \fIline\fR attribute of the token attributes \fIAttributes\fR\&.
.LP
The \fISetAttributeFun\fR is called with the value of the \fIline\fR attribute, and is to return the new value of the \fIline\fR attribute\&.
.RE
.LP
.B
format_error(ErrorDescriptor) -> string()
.br
.RS
.TP
Types
ErrorDescriptor = errordesc()
.br
.RE
.RS
.LP
Takes an \fIErrorDescriptor\fR and returns a string which describes the error or warning\&. This function is usually called implicitly when processing an \fIErrorInfo\fR structure (see below)\&.
.RE
.SH ERROR INFORMATION
.LP
The \fIErrorInfo\fR mentioned above is the standard \fIErrorInfo\fR structure which is returned from all IO modules\&. It has the following format:

.nf
{ErrorLocation, Module, ErrorDescriptor}
.fi
.LP
A string which describes the error is obtained with the following call:

.nf
Module:format_error(ErrorDescriptor)
.fi
.SH NOTES
.LP
The continuation of the first call to the re-entrant input functions must be \fI[]\fR\&. Refer to Armstrong, Virding and Williams, \&'Concurrent Programming in Erlang\&', Chapter 13, for a complete description of how the re-entrant input scheme works\&.
.SH SEE ALSO
.LP
io(3), erl_parse(3)
