.TH inet 3 "kernel  2.13.3" "Ericsson AB" "ERLANG MODULE DEFINITION"
.SH MODULE
inet \- Access to TCP/IP Protocols
.SH DESCRIPTION
.LP
Provides access to TCP/IP protocols\&.
.LP
See also \fIERTS User\&'s Guide, Inet configuration\fR for more information on how to configure an Erlang runtime system for IP communication\&.
.LP
Two Kernel configuration parameters affect the behaviour of all sockets opened on an Erlang node: \fIinet_default_connect_options\fR can contain a list of default options used for all sockets returned when doing \fIconnect\fR, and \fIinet_default_listen_options\fR can contain a list of default options used when issuing a \fIlisten\fR call\&. When \fIaccept\fR is issued, the values of the listensocket options are inherited, why no such application variable is needed for \fIaccept\fR\&.
.LP
Using the Kernel configuration parameters mentioned above, one can set default options for all TCP sockets on a node\&. This should be used with care, but options like \fI{delay_send, true}\fR might be specified in this way\&. An example of starting an Erlang node with all sockets using delayed send could look like this:

.nf
$ erl -sname test -kernel \e

inet_default_connect_options \&'[{delay_send,true}]\&' \e

inet_default_listen_options \&'[{delay_send,true}]\&'

.fi
.LP
Note that the default option \fI{active, true}\fR currently cannot be changed, for internal reasons\&.

.SH DATA TYPES

.nf
#hostent{h_addr_list = [ip_address()]  % list of addresses for this host
         h_addrtype  = inet | inet6
         h_aliases = [hostname()]      % list of aliases
         h_length = int()              % length of address in bytes
         h_name = hostname()           % official name for host
  The record is defined in the Kernel include file "inet\&.hrl"
  Add the following directive to the module:
    -include_lib("kernel/include/inet\&.hrl")\&.

hostname() = atom() | string()

ip_address() = {N1,N2,N3,N4}              % IPv4
             | {K1,K2,K3,K4,K5,K6,K7,K8}  % IPv6
  Ni = 0\&.\&.255
  Ki = 0\&.\&.65535

posix()
  an atom which is named from the Posix error codes used in
  Unix, and in the runtime libraries of most C compilers

socket()
  see gen_tcp(3), gen_udp(3)
.fi
.LP
Addresses as inputs to functions can be either a string or a tuple\&. For instance, the IP address 150\&.236\&.20\&.73 can be passed to \fIgethostbyaddr/1\fR either as the string "150\&.236\&.20\&.73" or as the tuple \fI{150, 236, 20, 73}\fR\&.
.LP
IPv4 address examples:

.nf
Address          ip_address()
-------          ------------
127\&.0\&.0\&.1        {127,0,0,1}
192\&.168\&.42\&.2     {192,168,42,2}
.fi
.LP
IPv6 address examples:

.nf
Address          ip_address()
-------          ------------
::1             {0,0,0,0,0,0,0,1}
::192\&.168\&.42\&.2  {0,0,0,0,0,0,(192 bsl 8) bor 168,(42 bsl 8) bor 2}
FFFF::192\&.168\&.42\&.2
                {16#FFFF,0,0,0,0,0,(192 bsl 8) bor 168,(42 bsl 8) bor 2}
3ffe:b80:1f8d:2:204:acff:fe17:bf38
                {16#3ffe,16#b80,16#1f8d,16#2,16#204,16#acff,16#fe17,16#bf38}
fe80::204:acff:fe17:bf38
                {16#fe80,0,0,0,0,16#204,16#acff,16#fe17,16#bf38}
.fi
.LP
A function that may be useful is \fIinet_parse:address/1\fR:

.nf
1> inet_parse:address("192\&.168\&.42\&.2")\&.

{ok,{192,168,42,2}}
2> inet_parse:address("FFFF::192\&.168\&.42\&.2")\&.

{ok,{65535,0,0,0,0,0,49320,10754}}
.fi
.SH EXPORTS
.LP
.B
close(Socket) -> ok
.br
.RS
.TP
Types
Socket = socket()
.br
.RE
.RS
.LP
Closes a socket of any type\&.
.RE
.LP
.B
get_rc() -> [{Par, Val}]
.br
.RS
.TP
Types
Par, Val -- see below
.br
.RE
.RS
.LP
Returns the state of the Inet configuration database in form of a list of recorded configuration parameters\&. (See the ERTS User\&'s Guide, Inet configuration, for more information)\&. Only parameters with other than default values are returned\&.
.RE
.LP
.B
format_error(Posix) -> string()
.br
.RS
.TP
Types
Posix = posix()
.br
.RE
.RS
.LP
Returns a diagnostic error string\&. See the section below for possible \fIPosix\fR values and the corresponding strings\&.
.RE
.LP
.B
getaddr(Host, Family) -> {ok, Address} | {error, posix()}
.br
.RS
.TP
Types
Host = ip_address() | string() | atom()
.br
Family = inet | inet6
.br
Address = ip_address()
.br
posix() = term()
.br
.RE
.RS
.LP
Returns the IP-address for \fIHost\fR as a tuple of integers\&. \fIHost\fR can be an IP-address, a single hostname or a fully qualified hostname\&.
.RE
.LP
.B
getaddrs(Host, Family) -> {ok, Addresses} | {error, posix()}
.br
.RS
.TP
Types
Host = ip_address() | string() | atom()
.br
Addresses = [ip_address()]
.br
Family = inet | inet6
.br
.RE
.RS
.LP
Returns a list of all IP-addresses for \fIHost\fR\&. \fIHost\fR can be an IP-adress, a single hostname or a fully qualified hostname\&.
.RE
.LP
.B
gethostbyaddr(Address) -> {ok, Hostent} | {error, posix()}
.br
.RS
.TP
Types
Address = string() | ip_address()
.br
Hostent = #hostent{}
.br
.RE
.RS
.LP
Returns a \fIhostent\fR record given an address\&.
.RE
.LP
.B
gethostbyname(Name) -> {ok, Hostent} | {error, posix()}
.br
.RS
.TP
Types
Hostname = hostname()
.br
Hostent = #hostent{}
.br
.RE
.RS
.LP
Returns a \fIhostent\fR record given a hostname\&.
.RE
.LP
.B
gethostbyname(Name, Family) -> {ok, Hostent} | {error, posix()}
.br
.RS
.TP
Types
Hostname = hostname()
.br
Family = inet | inet6
.br
Hostent = #hostent{}
.br
.RE
.RS
.LP
Returns a \fIhostent\fR record given a hostname, restricted to the given address family\&.
.RE
.LP
.B
gethostname() -> {ok, Hostname}
.br
.RS
.TP
Types
Hostname = string()
.br
.RE
.RS
.LP
Returns the local hostname\&. Will never fail\&.
.RE
.LP
.B
getopts(Socket, Options) -> OptionValues | {error, posix()}
.br
.RS
.TP
Types
Socket = term()
.br
Options = [Opt | RawOptReq]
.br
Opt = atom()
.br
RawOptReq = {raw, Protocol, OptionNum, ValueSpec}
.br
Protocol = int()
.br
OptionNum = int()
.br
ValueSpec = ValueSize | ValueBin
.br
ValueSize = int()
.br
ValueBin = binary()
.br
OptionValues = [{Opt, Val} | {raw, Protocol, OptionNum, ValueBin}]
.br
.RE
.RS
.LP
Gets one or more options for a socket\&. See setopts/2 for a list of available options\&.
.LP
The number of elements in the returned \fIOptionValues\fR list does not necessarily correspond to the number of options asked for\&. If the operating system fails to support an option, it is simply left out in the returned list\&. An error tuple is only returned when getting options for the socket is impossible (i\&.e\&. the socket is closed or the buffer size in a raw request is too large)\&. This behavior is kept for backward compatibility reasons\&.
.LP
A \fIRawOptReq\fR can be used to get information about socket options not (explicitly) supported by the emulator\&. The use of raw socket options makes the code non portable, but allows the Erlang programmer to take advantage of unusual features present on the current platform\&.
.LP
The \fIRawOptReq\fR consists of the tag \fIraw\fR followed by the protocol level, the option number and either a binary or the size, in bytes, of the buffer in which the option value is to be stored\&. A binary should be used when the underlying \fIgetsockopt\fR requires \fIinput\fR in the argument field, in which case the size of the binary should correspond to the required buffer size of the return value\&. The supplied values in a \fIRawOptReq\fR correspond to the second, third and fourth/fifth parameters to the \fIgetsockopt\fR call in the C socket API\&. The value stored in the buffer is returned as a binary \fIValueBin\fR where all values are coded in the native endianess\&.
.LP
Asking for and inspecting raw socket options require low level information about the current operating system and TCP stack\&.
.LP
As an example, consider a Linux machine where the \fITCP_INFO\fR option could be used to collect TCP statistics for a socket\&. Lets say we\&'re interested in the \fItcpi_sacked\fR field of the \fIstruct tcp_info\fR filled in when asking for \fITCP_INFO\fR\&. To be able to access this information, we need to know both the numeric value of the protocol level \fIIPPROTO_TCP\fR, the numeric value of the option \fITCP_INFO\fR, the size of the \fIstruct tcp_info\fR and the size and offset of the specific field\&. By inspecting the headers or writing a small C program, we found \fIIPPROTO_TCP\fR to be 6, \fITCP_INFO\fR to be 11, the structure size to be 92 (bytes), the offset of \fItcpi_sacked\fR to be 28 bytes and the actual value to be a 32 bit integer\&. We could use the following code to retrieve the value:

.nf
        get_tcpi_sacked(Sock) -> 
            {ok,[{raw,_,_,Info}]} = inet:getopts(Sock,[{raw,6,11,92}]),
            <<_:28/binary,TcpiSacked:32/native,_/binary>> = Info,
            TcpiSacked\&.
.fi
.LP
Preferably, you would check the machine type, the OS and the kernel version prior to executing anything similar to the code above\&.
.RE
.LP
.B
getstat(Socket)
.br
.B
getstat(Socket, Options) -> {ok, OptionValues} | {error, posix()}
.br
.RS
.TP
Types
Socket = term()
.br
Options = [Opt]
.br
OptionValues = [{Opt, Val}]
.br
 Opt, Val -- see below
.br
.RE
.RS
.LP
Gets one or more statistic options for a socket\&.
.LP
\fIgetstat(Socket)\fR is equivalent to \fIgetstat(Socket, [recv_avg, recv_cnt, recv_dvi, recv_max, recv_oct, send_avg, send_cnt, send_dvi, send_max, send_oct])\fR
.LP
The following options are available:
.RS 2
.TP 4
.B
\fIrecv_avg\fR:
Average size of packets in bytes received to the socket\&.
.TP 4
.B
\fIrecv_cnt\fR:
Number of packets received to the socket\&.
.TP 4
.B
\fIrecv_dvi\fR:
Average packet size deviation in bytes received to the socket\&.
.TP 4
.B
\fIrecv_max\fR:
The size of the largerst packet in bytes received to the socket\&.
.TP 4
.B
\fIrecv_oct\fR:
Number of bytes received to the socket\&.
.TP 4
.B
\fIsend_avg\fR:
Average size of packets in bytes sent from the socket\&.
.TP 4
.B
\fIsend_cnt\fR:
Number of packets sent from the socket\&.
.TP 4
.B
\fIsend_dvi\fR:
Average packet size deviation in bytes received sent from the socket\&.
.TP 4
.B
\fIsend_max\fR:
The size of the largerst packet in bytes sent from the socket\&.
.TP 4
.B
\fIsend_oct\fR:
Number of bytes sent from the socket\&.
.RE
.RE
.LP
.B
peername(Socket) -> {ok, {Address, Port}} | {error, posix()}
.br
.RS
.TP
Types
Socket = socket()
.br
Address = ip_address()
.br
Port = int()
.br
.RE
.RS
.LP
Returns the address and port for the other end of a connection\&.
.RE
.LP
.B
port(Socket) -> {ok, Port}
.br
.RS
.TP
Types
Socket = socket()
.br
Port = int()
.br
.RE
.RS
.LP
Returns the local port number for a socket\&.
.RE
.LP
.B
sockname(Socket) -> {ok, {Address, Port}} | {error, posix()}
.br
.RS
.TP
Types
Socket = socket()
.br
Address = ip_address()
.br
Port = int()
.br
.RE
.RS
.LP
Returns the local address and port number for a socket\&.
.RE
.LP
.B
setopts(Socket, Options) -> ok | {error, posix()}
.br
.RS
.TP
Types
Socket = term()
.br
Options = [{Opt, Val} | {raw, Protocol, Option, ValueBin}]
.br
Protocol = int()
.br
OptionNum = int()
.br
ValueBin = binary()
.br
 Opt, Val -- see below
.br
.RE
.RS
.LP
Sets one or more options for a socket\&. The following options are available:
.RS 2
.TP 4
.B
\fI{active, true | false | once}\fR:
If the value is \fItrue\fR, which is the default, everything received from the socket will be sent as messages to the receiving process\&. If the value is \fIfalse\fR (passive mode), the process must explicitly receive incoming data by calling \fIgen_tcp:recv/2, 3\fR or \fIgen_udp:recv/2, 3\fR (depending on the type of socket)\&.
.RS 4
.LP

.LP
If the value is \fIonce\fR (\fI{active, once}\fR), \fIone\fR data message from the socket will be sent to the process\&. To receive one more message, \fIsetopts/2\fR must be called again with the \fI{active, once}\fR option\&.
.LP

.LP
When using \fI{active, once}\fR, the socket changes behaviour automatically when data is received\&. This can sometimes be confusing in combination with connection oriented sockets (i\&.e\&. \fIgen_tcp\fR) as a socket with \fI{active, false}\fR behaviour reports closing differently than a socket with \fI{active, true}\fR behaviour\&. To make programming easier, a socket where the peer closed and this was detected while in \fI{active, false}\fR mode, will still generate the message \fI{tcp_closed, Socket}\fR when set to \fI{active, once}\fR or \fI{active, true}\fR mode\&. It is therefore safe to assume that the message \fI{tcp_closed, Socket}\fR, possibly followed by socket port termination (depending on the \fIexit_on_close\fR option) will eventually appear when a socket changes back and forth between \fI{active, true}\fR and \fI{active, false}\fR mode\&. However, \fIwhen\fR peer closing is detected is all up to the underlying TCP/IP stack and protocol\&.
.LP

.LP
Note that \fI{active, true}\fR mode provides no flow control; a fast sender could easily overflow the receiver with incoming messages\&. Use active mode only if your high-level protocol provides its own flow control (for instance, acknowledging received messages) or the amount of data exchanged is small\&. \fI{active, false}\fR mode or use of the \fI{active, once}\fR mode provides flow control; the other side will not be able send faster than the receiver can read\&.
.RE
.TP 4
.B
\fI{broadcast, Boolean}\fR(UDP sockets):
Enable/disable permission to send broadcasts\&.
.TP 4
.B
\fI{delay_send, Boolean}\fR:
Normally, when an Erlang process sends to a socket, the driver will try to immediately send the data\&. If that fails, the driver will use any means available to queue up the message to be sent whenever the operating system says it can handle it\&. Setting \fI{delay_send, true}\fR will make \fIall\fR messages queue up\&. This makes the messages actually sent onto the network be larger but fewer\&. The option actually affects the scheduling of send requests versus Erlang processes instead of changing any real property of the socket\&. Needless to say it is an implementation specific option\&. Default is \fIfalse\fR\&.
.TP 4
.B
\fI{dontroute, Boolean}\fR:
Enable/disable routing bypass for outgoing messages\&.
.TP 4
.B
\fI{exit_on_close, Boolean}\fR:
By default this option is set to \fItrue\fR\&.
.RS 4
.LP

.LP
The only reason to set it to \fIfalse\fR is if you want to continue sending data to the socket after a close has been detected, for instance if the peer has used gen_tcp:shutdown/2 to shutdown the write side\&.
.RE
.TP 4
.B
\fI{header, Size}\fR:
This option is only meaningful if the \fIbinary\fR option was specified when the socket was created\&. If the \fIheader\fR option is specified, the first \fISize\fR number bytes of data received from the socket will be elements of a list, and the rest of the data will be a binary given as the tail of the same list\&. If for example \fISize == 2\fR, the data received will match \fI[Byte1, Byte2|Binary]\fR\&.
.TP 4
.B
\fI{keepalive, Boolean}\fR(TCP/IP sockets):
Enables/disables periodic transmission on a connected socket, when no other data is being exchanged\&. If the other end does not respond, the connection is considered broken and an error message will be sent to the controlling process\&. Default disabled\&.
.TP 4
.B
\fI{nodelay, Boolean}\fR(TCP/IP sockets):
If \fIBoolean == true\fR, the \fITCP_NODELAY\fR option is turned on for the socket, which means that even small amounts of data will be sent immediately\&.
.TP 4
.B
\fI{packet, PacketType}\fR(TCP/IP sockets):
Defines the type of packets to use for a socket\&. The following values are valid:
.RS 4
.LP

.RS 2
.TP 4
.B
\fIraw | 0\fR:
No packaging is done\&.
.TP 4
.B
\fI1 | 2 | 4\fR:
Packets consist of a header specifying the number of bytes in the packet, followed by that number of bytes\&. The length of header can be one, two, or four bytes; containing an unsigned integer in big-endian byte order\&. Each send operation will generate the header, and the header will be stripped off on each receive operation\&.
.RS 4
.LP

.LP
In current implementation the 4-byte header is limited to 2Gb\&.
.RE
.TP 4
.B
\fIasn1 | cdr | sunrm | fcgi | tpkt | line\fR:
These packet types only have effect on receiving\&. When sending a packet, it is the responsibility of the application to supply a correct header\&. On receiving, however, there will be one message sent to the controlling process for each complete packet received, and, similarly, each call to \fIgen_tcp:recv/2, 3\fR returns one complete packet\&. The header is \fInot\fR stripped off\&.
.RS 4
.LP

.LP
The meanings of the packet types are as follows: 
.br
\fIasn1\fR - ASN\&.1 BER, 
.br
\fIsunrm\fR - Sun\&'s RPC encoding, 
.br
\fIcdr\fR - CORBA (GIOP 1\&.1), 
.br
\fIfcgi\fR - Fast CGI, 
.br
\fItpkt\fR - TPKT format [RFC1006], 
.br
\fIline\fR - Line mode, a packet is a line terminated with newline, lines longer than the receive buffer are truncated\&.
.RE
.TP 4
.B
\fIhttp | http_bin\fR:
The Hypertext Transfer Protocol\&. The packets are returned with the format according to \fIHttpPacket\fR described in  erlang:decode_packet/3\&. A socket in passive mode will return \fI{ok, HttpPacket}\fR from \fIgen_tcp:recv\fR while an active socket will send messages like \fI{http, Socket, HttpPacket}\fR\&.
.RS 4
.LP

.LP
Note that the packet type \fIhttph\fR is not needed when reading from a socket\&.
.RE
.RE
.RE
.TP 4
.B
\fI{packet_size, Integer}\fR(TCP/IP sockets):
Sets the max allowed length of the packet body\&. If the packet header indicates that the length of the packet is longer than the max allowed length, the packet is considered invalid\&. The same happens if the packet header is too big for the socket receive buffer\&.
.TP 4
.B
\fI{read_packets, Integer}\fR(UDP sockets):
Sets the max number of UDP packets to read without intervention from the socket when data is available\&. When this many packets have been read and delivered to the destination process, new packets are not read until a new notification of available data has arrived\&. The default is 5, and if this parameter is set too high the system can become unresponsive due to UDP packet flooding\&.
.TP 4
.B
\fI{recbuf, Integer}\fR:
Gives the size of the receive buffer to use for the socket\&.
.TP 4
.B
\fI{reuseaddr, Boolean}\fR:
Allows or disallows local reuse of port numbers\&. By default, reuse is disallowed\&.
.TP 4
.B
\fI{send_timeout, Integer}\fR:
Only allowed for connection oriented sockets\&.
.RS 4
.LP

.LP
Specifies a longest time to wait for a send operation to be accepted by the underlying TCP stack\&. When the limit is exceeded, the send operation will return \fI{error, timeout}\fR\&. How much of a packet that actually got sent is unknown, why the socket should be closed whenever a timeout has occured (see \fIsend_timeout_close\fR)\&. Default is \fIinfinity\fR\&.
.RE
.TP 4
.B
\fI{send_timeout_close, Boolean}\fR:
Only allowed for connection oriented sockets\&.
.RS 4
.LP

.LP
Used together with \fIsend_timeout\fR to specify whether the socket will be automatically closed when the send operation returns \fI{error, timeout}\fR\&. The recommended setting is \fItrue\fR which will automatically close the socket\&. Default is \fIfalse\fR due to backward compatibility\&.
.RE
.TP 4
.B
\fI{sndbuf, Integer}\fR:
Gives the size of the send buffer to use for the socket\&.
.TP 4
.B
\fI{priority, Integer}\fR:
Sets the SO_PRIORITY socket level option on platforms where this is implemented\&. The behaviour and allowed range varies on different systems\&. The option is ignored on platforms where the option is not implemented\&. Use with caution\&.
.TP 4
.B
\fI{tos, Integer}\fR:
Sets IP_TOS IP level options on platforms where this is implemented\&. The behaviour and allowed range varies on different systems\&. The option is ignored on platforms where the option is not implemented\&. Use with caution\&. 
.RE
.LP
In addition to the options mentioned above, \fIraw\fR option specifications can be used\&. The raw options are specified as a tuple of arity four, beginning with the tag \fIraw\fR, followed by the protocol level, the option number and the actual option value specified as a binary\&. This corresponds to the second, third and fourth argument to the \fIsetsockopt\fR call in the C socket API\&. The option value needs to be coded in the native endianess of the platform and, if a structure is required, needs to follow the struct alignment conventions on the specific platform\&.
.LP
Using raw socket options require detailed knowledge about the current operating system and TCP stack\&.
.LP
As an example of the usage of raw options, consider a Linux system where you want to set the \fITCP_LINGER2\fR option on the \fIIPPROTO_TCP\fR protocol level in the stack\&. You know that on this particular system it defaults to 60 (seconds), but you would like to lower it to 30 for a particular socket\&. The \fITCP_LINGER2\fR option is not explicitly supported by inet, but you know that the protocol level translates to the number 6, the option number to the number 8 and the value is to be given as a 32 bit integer\&. You can use this line of code to set the option for the socket named \fISock\fR:

.nf
        inet:setopts(Sock,[{raw,6,8,<<30:32/native>>}]),
.fi
.LP
As many options are silently discarded by the stack if they are given out of range, it could be a good idea to check that a raw option really got accepted\&. This code places the value in the variable TcpLinger2:

.nf
        {ok,[{raw,6,8,<<TcpLinger2:32/native>>}]}=inet:getopts(Sock,[{raw,6,8,4}]),
.fi
.LP
Code such as the examples above is inherently non portable, even different versions of the same OS on the same platform may respond differently to this kind of option manipulation\&. Use with care\&.
.LP
Note that the default options for TCP/IP sockets can be changed with the Kernel configuration parameters mentioned in the beginning of this document\&.
.RE
.SH POSIX ERROR CODES
.RS 2
.TP 2
*
\fIe2big\fR - argument list too long
.TP 2
*
\fIeacces\fR - permission denied
.TP 2
*
\fIeaddrinuse\fR - address already in use
.TP 2
*
\fIeaddrnotavail\fR - cannot assign requested address
.TP 2
*
\fIeadv\fR - advertise error
.TP 2
*
\fIeafnosupport\fR - address family not supported by protocol family
.TP 2
*
\fIeagain\fR - resource temporarily unavailable
.TP 2
*
\fIealign\fR - EALIGN
.TP 2
*
\fIealready\fR - operation already in progress
.TP 2
*
\fIebade\fR - bad exchange descriptor
.TP 2
*
\fIebadf\fR - bad file number
.TP 2
*
\fIebadfd\fR - file descriptor in bad state
.TP 2
*
\fIebadmsg\fR - not a data message
.TP 2
*
\fIebadr\fR - bad request descriptor
.TP 2
*
\fIebadrpc\fR - RPC structure is bad
.TP 2
*
\fIebadrqc\fR - bad request code
.TP 2
*
\fIebadslt\fR - invalid slot
.TP 2
*
\fIebfont\fR - bad font file format
.TP 2
*
\fIebusy\fR - file busy
.TP 2
*
\fIechild\fR - no children
.TP 2
*
\fIechrng\fR - channel number out of range
.TP 2
*
\fIecomm\fR - communication error on send
.TP 2
*
\fIeconnaborted\fR - software caused connection abort
.TP 2
*
\fIeconnrefused\fR - connection refused
.TP 2
*
\fIeconnreset\fR - connection reset by peer
.TP 2
*
\fIedeadlk\fR - resource deadlock avoided
.TP 2
*
\fIedeadlock\fR - resource deadlock avoided
.TP 2
*
\fIedestaddrreq\fR - destination address required
.TP 2
*
\fIedirty\fR - mounting a dirty fs w/o force
.TP 2
*
\fIedom\fR - math argument out of range
.TP 2
*
\fIedotdot\fR - cross mount point
.TP 2
*
\fIedquot\fR - disk quota exceeded
.TP 2
*
\fIeduppkg\fR - duplicate package name
.TP 2
*
\fIeexist\fR - file already exists
.TP 2
*
\fIefault\fR - bad address in system call argument
.TP 2
*
\fIefbig\fR - file too large
.TP 2
*
\fIehostdown\fR - host is down
.TP 2
*
\fIehostunreach\fR - host is unreachable
.TP 2
*
\fIeidrm\fR - identifier removed
.TP 2
*
\fIeinit\fR - initialization error
.TP 2
*
\fIeinprogress\fR - operation now in progress
.TP 2
*
\fIeintr\fR - interrupted system call
.TP 2
*
\fIeinval\fR - invalid argument
.TP 2
*
\fIeio\fR - I/O error
.TP 2
*
\fIeisconn\fR - socket is already connected
.TP 2
*
\fIeisdir\fR - illegal operation on a directory
.TP 2
*
\fIeisnam\fR - is a named file
.TP 2
*
\fIel2hlt\fR - level 2 halted
.TP 2
*
\fIel2nsync\fR - level 2 not synchronized
.TP 2
*
\fIel3hlt\fR - level 3 halted
.TP 2
*
\fIel3rst\fR - level 3 reset
.TP 2
*
\fIelbin\fR - ELBIN
.TP 2
*
\fIelibacc\fR - cannot access a needed shared library
.TP 2
*
\fIelibbad\fR - accessing a corrupted shared library
.TP 2
*
\fIelibexec\fR - cannot exec a shared library directly
.TP 2
*
\fIelibmax\fR - attempting to link in more shared libraries than system limit
.TP 2
*
\fIelibscn\fR - \&.lib section in a\&.out corrupted
.TP 2
*
\fIelnrng\fR - link number out of range
.TP 2
*
\fIeloop\fR - too many levels of symbolic links
.TP 2
*
\fIemfile\fR - too many open files
.TP 2
*
\fIemlink\fR - too many links
.TP 2
*
\fIemsgsize\fR - message too long
.TP 2
*
\fIemultihop\fR - multihop attempted
.TP 2
*
\fIenametoolong\fR - file name too long
.TP 2
*
\fIenavail\fR - not available
.TP 2
*
\fIenet\fR - ENET
.TP 2
*
\fIenetdown\fR - network is down
.TP 2
*
\fIenetreset\fR - network dropped connection on reset
.TP 2
*
\fIenetunreach\fR - network is unreachable
.TP 2
*
\fIenfile\fR - file table overflow
.TP 2
*
\fIenoano\fR - anode table overflow
.TP 2
*
\fIenobufs\fR - no buffer space available
.TP 2
*
\fIenocsi\fR - no CSI structure available
.TP 2
*
\fIenodata\fR - no data available
.TP 2
*
\fIenodev\fR - no such device
.TP 2
*
\fIenoent\fR - no such file or directory
.TP 2
*
\fIenoexec\fR - exec format error
.TP 2
*
\fIenolck\fR - no locks available
.TP 2
*
\fIenolink\fR - link has be severed
.TP 2
*
\fIenomem\fR - not enough memory
.TP 2
*
\fIenomsg\fR - no message of desired type
.TP 2
*
\fIenonet\fR - machine is not on the network
.TP 2
*
\fIenopkg\fR - package not installed
.TP 2
*
\fIenoprotoopt\fR - bad proocol option
.TP 2
*
\fIenospc\fR - no space left on device
.TP 2
*
\fIenosr\fR - out of stream resources or not a stream device
.TP 2
*
\fIenosym\fR - unresolved symbol name
.TP 2
*
\fIenosys\fR - function not implemented
.TP 2
*
\fIenotblk\fR - block device required
.TP 2
*
\fIenotconn\fR - socket is not connected
.TP 2
*
\fIenotdir\fR - not a directory
.TP 2
*
\fIenotempty\fR - directory not empty
.TP 2
*
\fIenotnam\fR - not a named file
.TP 2
*
\fIenotsock\fR - socket operation on non-socket
.TP 2
*
\fIenotsup\fR - operation not supported
.TP 2
*
\fIenotty\fR - inappropriate device for ioctl
.TP 2
*
\fIenotuniq\fR - name not unique on network
.TP 2
*
\fIenxio\fR - no such device or address
.TP 2
*
\fIeopnotsupp\fR - operation not supported on socket
.TP 2
*
\fIeperm\fR - not owner
.TP 2
*
\fIepfnosupport\fR - protocol family not supported
.TP 2
*
\fIepipe\fR - broken pipe
.TP 2
*
\fIeproclim\fR - too many processes
.TP 2
*
\fIeprocunavail\fR - bad procedure for program
.TP 2
*
\fIeprogmismatch\fR - program version wrong
.TP 2
*
\fIeprogunavail\fR - RPC program not available
.TP 2
*
\fIeproto\fR - protocol error
.TP 2
*
\fIeprotonosupport\fR - protocol not supported
.TP 2
*
\fIeprototype\fR - protocol wrong type for socket
.TP 2
*
\fIerange\fR - math result unrepresentable
.TP 2
*
\fIerefused\fR - EREFUSED
.TP 2
*
\fIeremchg\fR - remote address changed
.TP 2
*
\fIeremdev\fR - remote device
.TP 2
*
\fIeremote\fR - pathname hit remote file system
.TP 2
*
\fIeremoteio\fR - remote i/o error
.TP 2
*
\fIeremoterelease\fR - EREMOTERELEASE
.TP 2
*
\fIerofs\fR - read-only file system
.TP 2
*
\fIerpcmismatch\fR - RPC version is wrong
.TP 2
*
\fIerremote\fR - object is remote
.TP 2
*
\fIeshutdown\fR - cannot send after socket shutdown
.TP 2
*
\fIesocktnosupport\fR - socket type not supported
.TP 2
*
\fIespipe\fR - invalid seek
.TP 2
*
\fIesrch\fR - no such process
.TP 2
*
\fIesrmnt\fR - srmount error
.TP 2
*
\fIestale\fR - stale remote file handle
.TP 2
*
\fIesuccess\fR - Error 0
.TP 2
*
\fIetime\fR - timer expired
.TP 2
*
\fIetimedout\fR - connection timed out
.TP 2
*
\fIetoomanyrefs\fR - too many references
.TP 2
*
\fIetxtbsy\fR - text file or pseudo-device busy
.TP 2
*
\fIeuclean\fR - structure needs cleaning
.TP 2
*
\fIeunatch\fR - protocol driver not attached
.TP 2
*
\fIeusers\fR - too many users
.TP 2
*
\fIeversion\fR - version mismatch
.TP 2
*
\fIewouldblock\fR - operation would block
.TP 2
*
\fIexdev\fR - cross-domain link
.TP 2
*
\fIexfull\fR - message tables full
.TP 2
*
\fInxdomain\fR - the hostname or domain name could not be found
.RE
