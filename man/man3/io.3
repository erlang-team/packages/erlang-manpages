.TH io 3 "stdlib  1.16.3" "Ericsson AB" "ERLANG MODULE DEFINITION"
.SH MODULE
io \- Standard IO Server Interface Functions
.SH DESCRIPTION
.LP
This module provides an interface to standard Erlang IO servers\&. The output functions all return \fIok\fR if they are successful, or exit if they are not\&.
.LP
In the following description, all functions have an optional parameter \fIIoDevice\fR\&. If included, it must be the pid of a process which handles the IO protocols\&. Normally, it is the \fIIoDevice\fR returned by file:open/2\&.
.LP
For a description of the IO protocols refer to the STDLIB Users Guide\&.
.SS Warning:
.LP
As of R13A, data supplied to the put_chars function should be in the \fIchardata()\fR format described below\&. This means that programs supplying binaries to this function need to convert them to UTF-8 before trying to output the data on an \fIio_device()\fR\&.
.LP
If an io_device() is set in binary mode, the functions get_chars and get_line may return binaries instead of lists\&. The binaries will, as of R13A, be encoded in UTF-8\&.
.LP
To work with binaries in ISO-latin-1 encoding, use the file module instead\&.
.LP
For conversion functions between character encodings, see the unicode module\&.


.SH DATA TYPES

.nf
io_device()
  as returned by file:open/2, a process handling IO protocols
.fi

.nf
unicode_binary() = binary() with characters encoded in UTF-8 coding standard
unicode_char() = integer() representing valid unicode codepoint

chardata() = charlist() | unicode_binary()

charlist() = [unicode_char() | unicode_binary() | charlist()]
  a unicode_binary is allowed as the tail of the list
.fi
.SH EXPORTS
.LP
.B
columns([IoDevice]) -> {ok,int()} | {error, enotsup}
.br
.RS
.TP
Types
IoDevice = io_device()
.br
.RE
.RS
.LP
Retrieves the number of columns of the \fIIoDevice\fR (i\&.e\&. the width of a terminal)\&. The function only succeeds for terminal devices, for all other devices the function returns \fI{error, enotsup}\fR
.RE
.LP
.B
put_chars([IoDevice,] IoData) -> ok
.br
.RS
.TP
Types
IoDevice = io_device()
.br
IoData = chardata()
.br
.RE
.RS
.LP
Writes the characters of \fIIoData\fR to the io_server() (\fIIoDevice\fR)\&.
.RE
.LP
.B
nl([IoDevice]) -> ok
.br
.RS
.TP
Types
IoDevice = io_device()
.br
.RE
.RS
.LP
Writes new line to the standard output (\fIIoDevice\fR)\&.
.RE
.LP
.B
get_chars([IoDevice,] Prompt, Count) -> Data | eof
.br
.RS
.TP
Types
IoDevice = io_device()
.br
Prompt = atom() | string()
.br
Count = int()
.br
Data = [ unicode_char() ] | unicode_binary()
.br
.RE
.RS
.LP
Reads \fICount\fR characters from standard input (\fIIoDevice\fR), prompting it with \fIPrompt\fR\&. It returns:
.RS 2
.TP 4
.B
\fIData\fR:
The input characters\&. If the device supports Unicode, the data may represent codepoints larger than 255 (the latin1 range)\&. If the io_server() is set to deliver binaries, they will be encoded in UTF-8 (regardless of if the device actually supports Unicode or not)\&.
.TP 4
.B
\fIeof\fR:
End of file was encountered\&.
.TP 4
.B
\fI{error, Reason}\fR:
Other (rare) error condition, for instance \fI{error, estale}\fR if reading from an NFS file system\&.
.RE
.RE
.LP
.B
get_line([IoDevice,] Prompt) -> Data | eof | {error,Reason}
.br
.RS
.TP
Types
IoDevice = io_device()
.br
Prompt = atom() | string()
.br
Data = [ unicode_char() ] | unicode_binary()
.br
.RE
.RS
.LP
Reads a line from the standard input (\fIIoDevice\fR), prompting it with \fIPrompt\fR\&. It returns:
.RS 2
.TP 4
.B
\fIData\fR:
The characters in the line terminated by a LF (or end of file)\&. If the device supports Unicode, the data may represent codepoints larger than 255 (the latin1 range)\&. If the io_server() is set to deliver binaries, they will be encoded in UTF-8 (regardless of if the device actually supports Unicode or not)\&.
.TP 4
.B
\fIeof\fR:
End of file was encountered\&.
.TP 4
.B
\fI{error, Reason}\fR:
Other (rare) error condition, for instance \fI{error, estale}\fR if reading from an NFS file system\&.
.RE
.RE
.LP
.B
getopts([IoDevice]) -> Opts
.br
.RS
.TP
Types
IoDevice = io_device()
.br
Opts = [Opt]
.br
  Opt = {atom(), Value}
.br
  Value = term()
.br
.RE
.RS
.LP
This function requests all available options and their current values for a specific io_device()\&. Example:

.nf
1> {ok,F} = file:open("/dev/null",[read])\&.

{ok,<0\&.42\&.0>}
2> io:getopts(F)\&.

[{binary,false},{encoding,latin1}]
.fi
.LP
Here the file I/O-server returns all available options for a file, which are the expected ones, \fIencoding\fR and \fIbinary\fR\&. The standard shell however has some more options:

.nf
3> io:getopts()\&.
[{expand_fun,#Fun<group\&.0\&.120017273>},
 {echo,true},
 {binary,false},
 {encoding,unicode}]
.fi
.LP
This example is, as can be seen, run in an environment where the terminal supports Unicode input and output\&.
.RE
.LP
.B
setopts([IoDevice,] Opts) -> ok | {error, Reason}
.br
.RS
.TP
Types
IoDevice = io_device()
.br
Opts = [Opt]
.br
  Opt = atom() | {atom(), Value}
.br
  Value = term()
.br
Reason = term()
.br
.RE
.RS
.LP
Set options for the io_device() (\fIIoDevice\fR)\&.
.LP
Possible options and values vary depending on the actual io_device()\&. For a list of supported options and their current values on a specific device, use the getopts/1 function\&.
.LP
The options and values supported by the current OTP io_devices are:
.RS 2
.TP 4
.B
\fIbinary, list or {binary, bool()}\fR:
If set in binary mode (binary or {binary,true}), the io_server() sends binary data (encoded in UTF-8) as answers to the get_line, get_chars and, if possible, get_until requests (see the I/O protocol description in STDLIB User\&'s Guide for details)\&. The immediate effect is that \fIget_chars/2, 3\fR and \fIget_line/1, 2\fR return UTF-8 binaries instead of lists of chars for the affected device\&.
.RS 4
.LP

.LP
By default, all io_devices in OTP are set in list mode, but the io functions can handle any of these modes and so should other, user written, modules behaving as clients to I/O-servers\&.
.LP

.LP
This option is supported by the standard shell (group\&.erl), the \&'oldshell\&' (user\&.erl) and the file I/O servers\&.
.RE
.TP 4
.B
\fI{echo, bool()}\fR:
Denotes if the terminal should echo input\&. Only supported for the standard shell I/O-server (group\&.erl)
.TP 4
.B
\fI{expand_fun, fun()}\fR:
Provide a function for tab-completion (expansion) like the erlang shell\&. This function is called when the user presses the Tab key\&. The expansion is active when calling line-reading functions such as \fIget_line/1, 2\fR\&.
.RS 4
.LP

.LP
The function is called with the current line, upto the cursor, as a reversed string\&. It should return a three-tuple: \fI{yes|no, string(), [string(), \&.\&.\&.]}\fR\&. The first element gives a beep if \fIno\fR, otherwise the expansion is silent, the second is a string that will be entered at the cursor position, and the third is a list of possible expansions\&. If this list is non-empty, the list will be printed and the current input line will be written once again\&.
.LP

.LP
Trivial example (beep on anything except empty line, which is expanded to "quit"):
.LP


.nf
 fun("") -> {yes, "quit", []};
    (_) -> {no, "", ["quit"]} end
.fi
.LP

.LP
This option is supported by the standard shell only (group\&.erl)\&.
.RE
.TP 4
.B
\fI{encoding, latin1 | unicode}\fR:
Specifies how characters are input or output from or to the actual device, implying that i\&.e\&. a terminal is set to handle Unicode input and output or a file is set to handle UTF-8 data encoding\&.
.RS 4
.LP

.LP
The option \fIdoes not\fR affect how data is returned from the io-functions or how it is sent in the I/O-protocol, it only affects how the io_device() is to handle Unicode characters towards the "physical" device\&.
.LP

.LP
The standard shell will be set for either unicode or latin1 encoding when the system is started\&. The actual encoding is set with the help of the "LANG" or "LC_CTYPE" environment variables on Unix-like system or by other means on other systems\&. The bottom line is that the user can input Unicode characters and the device will be in {encoding, unicode} mode if the device supports it\&. The mode can be changed, if the assumption of the runtime system is wrong, by setting this option\&.
.LP

.LP
The io_device() used when Erlang is started with the "-oldshell" or "-noshell" flags is by default set to latin1 encoding, meaning that any characters beyond codepoint 255 will be escaped and that input is expected to be plain 8-bit ISO-latin-1\&. If the encoding is changed to Unicode, input and output from the standard file descriptors will be in UTF-8 (regardless of operating system)\&.
.LP

.LP
Files can also be set in {encoding, unicode}, meaning that data is written and read as UTF-8\&. More encodings are possible for files, see below\&.
.LP

.LP
{encoding, unicode | latin1} is supported by both the standard shell (group\&.erl including werl on windows), the \&'oldshell\&' (user\&.erl) and the file I/O servers\&.
.RE
.TP 4
.B
\fI{encoding, utf8 | utf16 | utf32 | {utf16, big} | {utf16, little} | {utf32, big} | {utf32, little}}\fR:
For disk files, the encoding can be set to various UTF variants\&. This will have the effect that data is expected to be read as the specified encoding from the file and the data will be wriiten in the specified encoding to the disk file\&.
.RS 4
.LP

.LP
{encoding, utf8} will have the same effect as {encoding,unicode} on files\&.
.LP

.LP
The extended encodings are only supported on disk files (opened by the file:open/2 function)
.RE
.RE
.RE
.LP
.B
write([IoDevice,] Term) -> ok
.br
.RS
.TP
Types
IoDevice = io_device()
.br
Term = term()
.br
.RE
.RS
.LP
Writes the term \fITerm\fR to the standard output (\fIIoDevice\fR)\&.
.RE
.LP
.B
read([IoDevice,] Prompt) -> Result
.br
.RS
.TP
Types
IoDevice = io_device()
.br
Prompt = atom() | string()
.br
Result = {ok, Term} | eof | {error, ErrorInfo}
.br
 Term = term()
.br
 ErrorInfo -- see section Error Information below
.br
.RE
.RS
.LP
Reads a term \fITerm\fR from the standard input (\fIIoDevice\fR), prompting it with \fIPrompt\fR\&. It returns:
.RS 2
.TP 4
.B
\fI{ok, Term}\fR:
The parsing was successful\&.
.TP 4
.B
\fIeof\fR:
End of file was encountered\&.
.TP 4
.B
\fI{error, ErrorInfo}\fR:
The parsing failed\&.
.RE
.RE
.LP
.B
read(IoDevice, Prompt, StartLine) -> Result
.br
.RS
.TP
Types
IoDevice = io_device()
.br
Prompt = atom() | string()
.br
StartLine = int()
.br
Result = {ok, Term, EndLine} | {eof, EndLine} | {error, ErrorInfo, EndLine}
.br
 Term = term()
.br
 EndLine = int()
.br
 ErrorInfo -- see section Error Information below
.br
.RE
.RS
.LP
Reads a term \fITerm\fR from \fIIoDevice\fR, prompting it with \fIPrompt\fR\&. Reading starts at line number \fIStartLine\fR\&. It returns:
.RS 2
.TP 4
.B
\fI{ok, Term, EndLine}\fR:
The parsing was successful\&.
.TP 4
.B
\fI{eof, EndLine}\fR:
End of file was encountered\&.
.TP 4
.B
\fI{error, ErrorInfo, EndLine}\fR:
The parsing failed\&.
.RE
.RE
.LP
.B
fwrite(Format) ->
.br
.B
fwrite([IoDevice,] Format, Data) -> ok
.br
.B
format(Format) ->
.br
.B
format([IoDevice,] Format, Data) -> ok
.br
.RS
.TP
Types
IoDevice = io_device()
.br
Format = atom() | string() | binary()
.br
Data = [term()]
.br
.RE
.RS
.LP
Writes the items in \fIData\fR (\fI[]\fR) on the standard output (\fIIoDevice\fR) in accordance with \fIFormat\fR\&. \fIFormat\fR contains plain characters which are copied to the output device, and control sequences for formatting, see below\&. If \fIFormat\fR is an atom or a binary, it is first converted to a list with the aid of \fIatom_to_list/1\fR or \fIbinary_to_list/1\fR\&.

.nf
1> io:fwrite("Hello world!~n", [])\&.

Hello world!
ok
.fi
.LP
The general format of a control sequence is \fI~F\&.P\&.PadModC\fR\&. The character \fIC\fR determines the type of control sequence to be used, \fIF\fR and \fIP\fR are optional numeric arguments\&. If \fIF\fR, \fIP\fR, or \fIPad\fR is \fI*\fR, the next argument in \fIData\fR is used as the numeric value of \fIF\fR or \fIP\fR\&.
.LP
\fIF\fR is the \fIfield width\fR of the printed argument\&. A negative value means that the argument will be left justified within the field, otherwise it will be right justified\&. If no field width is specified, the required print width will be used\&. If the field width specified is too small, then the whole field will be filled with \fI*\fR characters\&.
.LP
\fIP\fR is the \fIprecision\fR of the printed argument\&. A default value is used if no precision is specified\&. The interpretation of precision depends on the control sequences\&. Unless otherwise specified, the argument \fIwithin\fR is used to determine print width\&.
.LP
\fIPad\fR is the padding character\&. This is the character used to pad the printed representation of the argument so that it conforms to the specified field width and precision\&. Only one padding character can be specified and, whenever applicable, it is used for both the field width and precision\&. The default padding character is \fI\&' \&'\fR (space)\&.
.LP
\fIMod\fR is the control sequence modifier\&. It is either a single character (currently only \&'t\&', for unicode translation, is supported) that changes the interpretation of Data\&.
.LP
The following control sequences are available:
.RS 2
.TP 4
.B
\fI~\fR:
The character \fI~\fR is written\&.
.TP 4
.B
\fIc\fR:
The argument is a number that will be interpreted as an ASCII code\&. The precision is the number of times the character is printed and it defaults to the field width, which in turn defaults to 1\&. The following example illustrates:
.RS 4
.LP


.nf
2> io:fwrite("|~10\&.5c|~-10\&.5c|~5c|~n", [$a, $b, $c])\&.

|     aaaaa|bbbbb     |ccccc|
ok
.fi
.LP

.LP
If the Unicode translation modifier (\&'t\&') is in effect, the integer argument can be any number representing a valid unicode codepoint, otherwise it should be an integer less than or equal to 255, otherwise it is masked with 16#FF:
.LP


.nf
1> io:fwrite("~tc~n",[1024])\&.

\ex{400}
ok
2> io:fwrite("~c~n",[1024])\&.

^@
ok
.fi
.RE
.TP 4
.B
\fIf\fR:
The argument is a float which is written as \fI[-]ddd\&.ddd\fR, where the precision is the number of digits after the decimal point\&. The default precision is 6 and it cannot be less than 1\&.
.TP 4
.B
\fIe\fR:
The argument is a float which is written as \fI[-]d\&.ddde+-ddd\fR, where the precision is the number of digits written\&. The default precision is 6 and it cannot be less than 2\&.
.TP 4
.B
\fIg\fR:
The argument is a float which is written as \fIf\fR, if it is >= 0\&.1 and < 10000\&.0\&. Otherwise, it is written in the \fIe\fR format\&. The precision is the number of significant digits\&. It defaults to 6 and should not be less than 2\&. If the absolute value of the float does not allow it to be written in the \fIf\fR format with the desired number of significant digits, it is also written in the \fIe\fR format\&.
.TP 4
.B
\fIs\fR:
Prints the argument with the \fIstring\fR syntax\&. The argument is, if no Unicode translation modifier is present, an I/O list, a binary, or an atom\&. If the Unicode translation modifier (\&'t\&') is in effect, the argument is chardata(), meaning that binaries are in UTF-8\&. The characters are printed without quotes\&. In this format, the printed argument is truncated to the given precision and field width\&.
.RS 4
.LP

.LP
This format can be used for printing any object and truncating the output so it fits a specified field:
.LP


.nf
3> io:fwrite("|~10w|~n", [{hey, hey, hey}])\&.

|**********|
ok
4> io:fwrite("|~10s|~n", [io_lib:write({hey, hey, hey})])\&.

|{hey,hey,h|
ok
.fi
.LP

.LP
A list with integers larger than 255 is considered an error if the Unicode translation modifier is not given:
.LP


.nf
1> io:fwrite("~ts~n",[[1024]])\&.

\ex{400}
ok
2> io:fwrite("~s~n",[[1024]])\&.
** exception exit: {badarg,[{io,format,[<0\&.26\&.0>,"~s~n",[[1024]]]},
   \&.\&.\&.
.fi
.RE
.TP 4
.B
\fIw\fR:
Writes data with the standard syntax\&. This is used to output Erlang terms\&. Atoms are printed within quotes if they contain embedded non-printable characters, and floats are printed accurately as the shortest, correctly rounded string\&.
.TP 4
.B
\fIp\fR:
Writes the data with standard syntax in the same way as \fI~w\fR, but breaks terms whose printed representation is longer than one line into many lines and indents each line sensibly\&. It also tries to detect lists of printable characters and to output these as strings\&. For example:
.RS 4
.LP


.nf
5> T = [{attributes,[[{id,age,1\&.50000},{mode,explicit},

{typename,"INTEGER"}], [{id,cho},{mode,explicit},{typename,\&'Cho\&'}]]},

{typename,\&'Person\&'},{tag,{\&'PRIVATE\&',3}},{mode,implicit}]\&.

\&.\&.\&.
6> io:fwrite("~w~n", [T])\&.

[{attributes,[[{id,age,1\&.5},{mode,explicit},{typename,
[73,78,84,69,71,69,82]}],[{id,cho},{mode,explicit},{typena
me,\&'Cho\&'}]]},{typename,\&'Person\&'},{tag,{\&'PRIVATE\&',3}},{mode
,implicit}]
ok
7> io:fwrite("~62p~n", [T])\&.

[{attributes,[[{id,age,1\&.5},
               {mode,explicit},
               {typename,"INTEGER"}],
              [{id,cho},{mode,explicit},{typename,\&'Cho\&'}]]},
 {typename,\&'Person\&'},
 {tag,{\&'PRIVATE\&',3}},
 {mode,implicit}]
ok
.fi
.LP

.LP
The field width specifies the maximum line length\&. It defaults to 80\&. The precision specifies the initial indentation of the term\&. It defaults to the number of characters printed on this line in the \fIsame\fR call to \fIio:fwrite\fR or \fIio:format\fR\&. For example, using \fIT\fR above:
.LP


.nf
8> io:fwrite("Here T = ~62p~n", [T])\&.

Here T = [{attributes,[[{id,age,1\&.5},
                        {mode,explicit},
                        {typename,"INTEGER"}],
                       [{id,cho},
                        {mode,explicit},
                        {typename,\&'Cho\&'}]]},
          {typename,\&'Person\&'},
          {tag,{\&'PRIVATE\&',3}},
          {mode,implicit}]
ok
.fi
.RE
.TP 4
.B
\fIW\fR:
Writes data in the same way as \fI~w\fR, but takes an extra argument which is the maximum depth to which terms are printed\&. Anything below this depth is replaced with \fI\&.\&.\&.\fR\&. For example, using \fIT\fR above:
.RS 4
.LP


.nf
9> io:fwrite("~W~n", [T,9])\&.

[{attributes,[[{id,age,1\&.5},{mode,explicit},{typename,\&.\&.\&.}],
[{id,cho},{mode,\&.\&.\&.},{\&.\&.\&.}]]},{typename,\&'Person\&'},
{tag,{\&'PRIVATE\&',3}},{mode,implicit}]
ok
.fi
.LP

.LP
If the maximum depth has been reached, then it is impossible to read in the resultant output\&. Also, the \fI, \&.\&.\&.\fR form in a tuple denotes that there are more elements in the tuple but these are below the print depth\&.
.RE
.TP 4
.B
\fIP\fR:
Writes data in the same way as \fI~p\fR, but takes an extra argument which is the maximum depth to which terms are printed\&. Anything below this depth is replaced with \fI\&.\&.\&.\fR\&. For example:
.RS 4
.LP


.nf
10> io:fwrite("~62P~n", [T,9])\&.

[{attributes,[[{id,age,1\&.5},{mode,explicit},{typename,\&.\&.\&.}],
              [{id,cho},{mode,\&.\&.\&.},{\&.\&.\&.}]]},
 {typename,\&'Person\&'},
 {tag,{\&'PRIVATE\&',3}},
 {mode,implicit}]
ok
.fi
.RE
.TP 4
.B
\fIB\fR:
Writes an integer in base 2\&.\&.36, the default base is 10\&. A leading dash is printed for negative integers\&.
.RS 4
.LP

.LP
The precision field selects base\&. For example:
.LP


.nf
11> io:fwrite("~\&.16B~n", [31])\&.

1F
ok
12> io:fwrite("~\&.2B~n", [-19])\&.

-10011
ok
13> io:fwrite("~\&.36B~n", [5*36+35])\&.

5Z
ok
.fi
.RE
.TP 4
.B
\fIX\fR:
Like \fIB\fR, but takes an extra argument that is a prefix to insert before the number, but after the leading dash, if any\&.
.RS 4
.LP

.LP
The prefix can be a possibly deep list of characters or an atom\&.
.LP


.nf
14> io:fwrite("~X~n", [31,"10#"])\&.

10#31
ok
15> io:fwrite("~\&.16X~n", [-31,"0x"])\&.

-0x1F
ok
.fi
.RE
.TP 4
.B
\fI#\fR:
Like \fIB\fR, but prints the number with an Erlang style \&'#\&'-separated base prefix\&.
.RS 4
.LP


.nf
16> io:fwrite("~\&.10#~n", [31])\&.

10#31
ok
17> io:fwrite("~\&.16#~n", [-31])\&.

-16#1F
ok
.fi
.RE
.TP 4
.B
\fIb\fR:
Like \fIB\fR, but prints lowercase letters\&.
.TP 4
.B
\fIx\fR:
Like \fIX\fR, but prints lowercase letters\&.
.TP 4
.B
\fI+\fR:
Like \fI#\fR, but prints lowercase letters\&.
.TP 4
.B
\fIn\fR:
Writes a new line\&.
.TP 4
.B
\fIi\fR:
Ignores the next term\&.
.RE
.LP
Returns:
.RS 2
.TP 4
.B
\fIok\fR:
The formatting succeeded\&.
.RE
.LP
If an error occurs, there is no output\&. For example:

.nf
18> io:fwrite("~s ~w ~i ~w ~c ~n",[\&'abc def\&', \&'abc def\&', {foo, 1},{foo, 1}, 65])\&.

abc def \&'abc def\&'  {foo,1} A
ok
19> io:fwrite("~s", [65])\&.

** exception exit: {badarg,[{io,format,[<0\&.22\&.0>,"~s","A"]},
                            {erl_eval,do_apply,5},
                            {shell,exprs,6},
                            {shell,eval_exprs,6},
                            {shell,eval_loop,3}]}
     in function  io:o_request/2
.fi
.LP
In this example, an attempt was made to output the single character \&'65\&' with the aid of the string formatting directive "~s"\&.
.RE
.LP
.B
fread([IoDevice,] Prompt, Format) -> Result
.br
.RS
.TP
Types
IoDevice = io_device()
.br
Prompt = atom() | string()
.br
Format = string()
.br
Result = {ok, Terms} | eof | {error, What}
.br
 Terms = [term()]
.br
 What = term()
.br
.RE
.RS
.LP
Reads characters from the standard input (\fIIoDevice\fR), prompting it with \fIPrompt\fR\&. Interprets the characters in accordance with \fIFormat\fR\&. \fIFormat\fR contains control sequences which directs the interpretation of the input\&.
.LP
\fIFormat\fR may contain:
.RS 2
.TP 2
*
White space characters (SPACE, TAB and NEWLINE) which cause input to be read to the next non-white space character\&.
.TP 2
*
Ordinary characters which must match the next input character\&.
.TP 2
*
Control sequences, which have the general format \fI~*FMC\fR\&. The character \fI*\fR is an optional return suppression character\&. It provides a method to specify a field which is to be omitted\&. \fIF\fR is the \fIfield width\fR of the input field, \fIM\fR is an optional translation modifier (of which \&'t\&' is the only currently supported, meaning Unicode translation) and \fIC\fR determines the type of control sequence\&.
.RS 2
.LP

.LP
Unless otherwise specified, leading white-space is ignored for all control sequences\&. An input field cannot be more than one line wide\&. The following control sequences are available:
.LP

.RS 2
.TP 4
.B
\fI~\fR:
A single \fI~\fR is expected in the input\&.
.TP 4
.B
\fId\fR:
A decimal integer is expected\&.
.TP 4
.B
\fIu\fR:
An unsigned integer in base 2\&.\&.36 is expected\&. The field width parameter is used to specify base\&. Leading white-space characters are not skipped\&.
.TP 4
.B
\fI-\fR:
An optional sign character is expected\&. A sign character \&'-\&' gives the return value \fI-1\fR\&. Sign character \&'+\&' or none gives \fI1\fR\&. The field width parameter is ignored\&. Leading white-space characters are not skipped\&.
.TP 4
.B
\fI#\fR:
An integer in base 2\&.\&.36 with Erlang-style base prefix (for example \fI"16#ffff"\fR) is expected\&.
.TP 4
.B
\fIf\fR:
A floating point number is expected\&. It must follow the Erlang floating point number syntax\&.
.TP 4
.B
\fIs\fR:
A string of non-white-space characters is read\&. If a field width has been specified, this number of characters are read and all trailing white-space characters are stripped\&. An Erlang string (list of characters) is returned\&.
.RS 4
.LP

.LP
If Unicode translation is in effect (~ts), characters larger than 255 are accepted, otherwise not\&. With the translation modifier, the list returned may as a consequence also contain integers larger than 255:
.LP


.nf
1> io:fread("Prompt> ","~s")\&.

Prompt> <Characters beyond latin1 range not printable in this medium>

{error,{fread,string}}
2> io:fread("Prompt> ","~ts")\&.

Prompt> <Characters beyond latin1 range not printable in this medium>

{ok,[[1091,1085,1080,1094,1086,1076,1077]]}
.fi
.RE
.TP 4
.B
\fIa\fR:
Similar to \fIs\fR, but the resulting string is converted into an atom\&.
.RS 4
.LP

.LP
The Unicode translation modifier is not allowed (atoms can not contain characters beyond the latin1 range)\&.
.RE
.TP 4
.B
\fIc\fR:
The number of characters equal to the field width are read (default is 1) and returned as an Erlang string\&. However, leading and trailing white-space characters are not omitted as they are with \fIs\fR\&. All characters are returned\&.
.RS 4
.LP

.LP
The Unicode translation modifier works as with \fIs\fR:
.LP


.nf
1> io:fread("Prompt> ","~c")\&.

Prompt> <Character beyond latin1 range not printable in this medium>

{error,{fread,string}}
2> io:fread("Prompt> ","~tc")\&.

Prompt> <Character beyond latin1 range not printable in this medium>

{ok,[[1091]]}
.fi
.RE
.TP 4
.B
\fIl\fR:
Returns the number of characters which have been scanned up to that point, including white-space characters\&.
.RE
.LP

.LP
It returns:
.LP

.RS 2
.TP 4
.B
\fI{ok, Terms}\fR:
The read was successful and \fITerms\fR is the list of successfully matched and read items\&.
.TP 4
.B
\fIeof\fR:
End of file was encountered\&.
.TP 4
.B
\fI{error, What}\fR:
The read operation failed and the parameter \fIWhat\fR gives a hint about the error\&.
.RE
.RE
.RE
.LP
Examples:

.nf
20> io:fread(\&'enter>\&', "~f~f~f")\&.

enter>1\&.9 35\&.5e3 15\&.0

{ok,[1\&.9,3\&.55e4,15\&.0]}
21> io:fread(\&'enter>\&', "~10f~d")\&.

enter>     5\&.67899

{ok,[5\&.678,99]}
22> io:fread(\&'enter>\&', ":~10s:~10c:")\&.

enter>:
   alan
   :
   joe
    :

{ok, ["alan", "   joe    "]}
.fi
.RE
.LP
.B
rows([IoDevice]) -> {ok,int()} | {error, enotsup}
.br
.RS
.TP
Types
IoDevice = io_device()
.br
.RE
.RS
.LP
Retrieves the number of rows of the \fIIoDevice\fR (i\&.e\&. the height of a terminal)\&. The function only succeeds for terminal devices, for all other devices the function returns \fI{error, enotsup}\fR
.RE
.LP
.B
scan_erl_exprs(Prompt) ->
.br
.B
scan_erl_exprs([IoDevice,] Prompt, StartLine) -> Result
.br
.RS
.TP
Types
IoDevice = io_device()
.br
Prompt = atom() | string()
.br
StartLine = int()
.br
Result = {ok, Tokens, EndLine} | {eof, EndLine} | {error, ErrorInfo, EndLine}
.br
 Tokens -- see erl_scan(3)
.br
 EndLine = int()
.br
 ErrorInfo -- see section Error Information below
.br
.RE
.RS
.LP
Reads data from the standard input (\fIIoDevice\fR), prompting it with \fIPrompt\fR\&. Reading starts at line number \fIStartLine\fR (1)\&. The data is tokenized as if it were a sequence of Erlang expressions until a final \fI\&'\&.\&'\fR is reached\&. This token is also returned\&. It returns:
.RS 2
.TP 4
.B
\fI{ok, Tokens, EndLine}\fR:
The tokenization succeeded\&.
.TP 4
.B
\fI{eof, EndLine}\fR:
End of file was encountered\&.
.TP 4
.B
\fI{error, ErrorInfo, EndLine}\fR:
An error occurred\&.
.RE
.LP
Example:

.nf
23> io:scan_erl_exprs(\&'enter>\&')\&.

enter>abc(), "hey"\&.

{ok,[{atom,1,abc},{\&'(\&',1},{\&')\&',1},{\&',\&',1},{string,1,"hey"},{dot,1}],2}
24> io:scan_erl_exprs(\&'enter>\&')\&.

enter>1\&.0er\&.

{error,{1,erl_scan,{illegal,float}},2}
.fi
.RE
.LP
.B
scan_erl_form(Prompt) ->
.br
.B
scan_erl_form([IoDevice,] Prompt, StartLine) -> Result
.br
.RS
.TP
Types
IoDevice = io_device()
.br
Prompt = atom() | string()
.br
StartLine = int()
.br
Result = {ok, Tokens, EndLine} | {eof, EndLine} | {error, ErrorInfo, EndLine}
.br
 Tokens -- see erl_scan(3)
.br
 EndLine = int()
.br
 ErrorInfo -- see section Error Information below
.br
.RE
.RS
.LP
Reads data from the standard input (\fIIoDevice\fR), prompting it with \fIPrompt\fR\&. Starts reading at line number \fIStartLine\fR (1)\&. The data is tokenized as if it were an Erlang form - one of the valid Erlang expressions in an Erlang source file - until a final \fI\&'\&.\&'\fR is reached\&. This last token is also returned\&. The return values are the same as for \fIscan_erl_exprs/1, 2, 3\fR above\&.
.RE
.LP
.B
parse_erl_exprs(Prompt) ->
.br
.B
parse_erl_exprs([IoDevice,] Prompt, StartLine) -> Result
.br
.RS
.TP
Types
IoDevice = io_device()
.br
Prompt = atom() | string()
.br
StartLine = int()
.br
Result = {ok, Expr_list, EndLine} | {eof, EndLine} | {error, ErrorInfo, EndLine}
.br
 Expr_list -- see erl_parse(3)
.br
 EndLine = int()
.br
 ErrorInfo -- see section Error Information below
.br
.RE
.RS
.LP
Reads data from the standard input (\fIIoDevice\fR), prompting it with \fIPrompt\fR\&. Starts reading at line number \fIStartLine\fR (1)\&. The data is tokenized and parsed as if it were a sequence of Erlang expressions until a final \&'\&.\&' is reached\&. It returns:
.RS 2
.TP 4
.B
\fI{ok, Expr_list, EndLine}\fR:
The parsing was successful\&.
.TP 4
.B
\fI{eof, EndLine}\fR:
End of file was encountered\&.
.TP 4
.B
\fI{error, ErrorInfo, EndLine}\fR:
An error occurred\&.
.RE
.LP
Example:

.nf
25> io:parse_erl_exprs(\&'enter>\&')\&.

enter>abc(), "hey"\&.

{ok, [{call,1,{atom,1,abc},[]},{string,1,"hey"}],2}
26> io:parse_erl_exprs (\&'enter>\&')\&.

enter>abc("hey"\&.

{error,{1,erl_parse,["syntax error before: ",["\&'\&.\&'"]]},2}
.fi
.RE
.LP
.B
parse_erl_form(Prompt) ->
.br
.B
parse_erl_form([IoDevice,] Prompt, StartLine) -> Result
.br
.RS
.TP
Types
IoDevice = io_device()
.br
Prompt = atom() | string()
.br
StartLine = int()
.br
Result = {ok, AbsForm, EndLine} | {eof, EndLine} | {error, ErrorInfo, EndLine}
.br
 AbsForm -- see erl_parse(3)
.br
 EndLine = int()
.br
 ErrorInfo -- see section Error Information below
.br
.RE
.RS
.LP
Reads data from the standard input (\fIIoDevice\fR), prompting it with \fIPrompt\fR\&. Starts reading at line number \fIStartLine\fR (1)\&. The data is tokenized and parsed as if it were an Erlang form - one of the valid Erlang expressions in an Erlang source file - until a final \&'\&.\&' is reached\&. It returns:
.RS 2
.TP 4
.B
\fI{ok, AbsForm, EndLine}\fR:
The parsing was successful\&.
.TP 4
.B
\fI{eof, EndLine}\fR:
End of file was encountered\&.
.TP 4
.B
\fI{error, ErrorInfo, EndLine}\fR:
An error occurred\&.
.RE
.RE
.SH STANDARD INPUT/OUTPUT
.LP
All Erlang processes have a default standard IO device\&. This device is used when no \fIIoDevice\fR argument is specified in the above function calls\&. However, it is sometimes desirable to use an explicit \fIIoDevice\fR argument which refers to the default IO device\&. This is the case with functions that can access either a file or the default IO device\&. The atom \fIstandard_io\fR has this special meaning\&. The following example illustrates this:

.nf
27> io:read(\&'enter>\&')\&.

enter>foo\&.

{ok,foo}
28> io:read(standard_io, \&'enter>\&')\&.

enter>bar\&.

{ok,bar}
.fi
.LP
There is always a process registered under the name of \fIuser\fR\&. This can be used for sending output to the user\&.
.SH STANDARD ERROR
.LP
In certain situations, especially when the standard output is redirected, access to an io_server() specific for error messages might be conveninet\&. The io_device \&'standard_error\&' can be used to direct output to whatever the current operating system considers a suitable device for error output\&. Example on a Unix-like operating system:

.nf
$ erl -noshell -noinput -eval \&'io:format(standard_error,"Error: ~s~n",["error 11"]),init:stop()\&.\&' > /dev/null

Error: error 11
.fi
.SH ERROR INFORMATION
.LP
The \fIErrorInfo\fR mentioned above is the standard \fIErrorInfo\fR structure which is returned from all IO modules\&. It has the format:

.nf
{ErrorLine, Module, ErrorDescriptor}
.fi
.LP
A string which describes the error is obtained with the following call:

.nf
apply(Module, format_error, ErrorDescriptor)
.fi
