.TH erl_prettypr 3 "syntax_tools  1.6.3" "Ericsson AB" "ERLANG MODULE DEFINITION"
.SH MODULE
erl_prettypr \- Pretty printing of abstract Erlang syntax trees\&.
.SH DESCRIPTION
.LP
Pretty printing of abstract Erlang syntax trees\&.
.LP
This module is a front end to the pretty-printing library module \fIprettypr\fR, for text formatting of abstract syntax trees defined by the module \fIerl_syntax\fR\&.

.SH DATA TYPES
.RS 2
.TP 4
.B
\fIcontext()\fR:

.RS 4
.LP
A representation of the current context of the pretty-printer\&. Can be accessed in hook functions\&. 
.LP

.RE
.TP 4
.B
\fIhook() = (syntaxTree(), context(), Continuation) -> document()\fR:

.RS 4
.RS 2
.TP 2
*
\fIContinuation = (syntaxTree(), context()) -> document()\fR
.RE
.LP

.LP
A call-back function for user-controlled formatting\&. See format/2\&. 
.LP

.RE
.RE
.SH EXPORTS
.LP
.B
best(Tree::syntaxTree()) -> empty | document()
.br
.RS
.LP
Equivalent to best(Tree, [])\&.
.RE
.LP
.B
best(Tree::syntaxTree(), Options::[term()]) -> empty | document()
.br
.RS
.LP
Creates a fixed "best" abstract layout for a syntax tree\&. This is similar to the \fIlayout/2\fR function, except that here, the final layout has been selected with respect to the given options\&. The atom \fIempty\fR is returned if no such layout could be produced\&. For information on the options, see the \fIformat/2\fR function\&. 
.LP
\fISee also:\fR best/1, format/2, layout/2, prettypr:best/3\&.
.RE
.LP
.B
format(Tree::syntaxTree()) -> string()
.br
.RS
.LP
Equivalent to format(Tree, [])\&.
.RE
.LP
.B
format(Tree::syntaxTree(), Options::[term()]) -> string()
.br
.RS
.TP
Types
syntaxTree() (see module erl_syntax)
.br
.RE
.RS
.LP
Prettyprint-formats an abstract Erlang syntax tree as text\&. For example, if you have a \fI\&.beam\fR file that has been compiled with \fIdebug_info\fR, the following should print the source code for the module (as it looks in the debug info representation): 

.nf
     {ok,{_,[{abstract_code,{_,AC}}]}} =
             beam_lib:chunks("myfile\&.beam",[abstract_code]),
     io:put_chars(erl_prettypr:format(erl_syntax:form_list(AC)))
.fi
.LP
Available options: 
.RS 2
.TP 4
.B
{hook, none | hook()}:
Unless the value is \fInone\fR, the given function is called for each node whose list of annotations is not empty; see below for details\&. The default value is \fInone\fR\&.
.TP 4
.B
{paper, integer()}:
Specifies the preferred maximum number of characters on any line, including indentation\&. The default value is 80\&.
.TP 4
.B
{ribbon, integer()}:
Specifies the preferred maximum number of characters on any line, not counting indentation\&. The default value is 65\&.
.TP 4
.B
{user, term()}:
User-specific data for use in hook functions\&. The default value is \fIundefined\fR\&.
.RE
.LP
A hook function (cf\&. the hook() type) is passed the current syntax tree node, the context, and a continuation\&. The context can be examined and manipulated by functions such as \fIget_ctxt_user/1\fR and \fIset_ctxt_user/2\fR\&. The hook must return a "document" data structure (see layout/2 and best/2); this may be constructed in part or in whole by applying the continuation function\&. For example, the following is a trivial hook: 

.nf
      fun (Node, Ctxt, Cont) -> Cont(Node, Ctxt) end
.fi
.LP
which yields the same result as if no hook was given\&. The following, however: 

.nf
      fun (Node, Ctxt, Cont) ->
          Doc = Cont(Node, Ctxt),
          prettypr:beside(prettypr:text("<b>"),
                          prettypr:beside(Doc,
                                          prettypr:text("</b>")))
      end
.fi
.LP
will place the text of any annotated node (regardless of the annotation data) between HTML "boldface begin" and "boldface end" tags\&. 
.LP
\fISee also:\fR erl_syntax, best/2, format/1, get_ctxt_user/1, layout/2, set_ctxt_user/2\&.
.RE
.LP
.B
get_ctxt_hook(Ctxt::context()) -> hook()
.br
.RS
.LP
Returns the hook function field of the prettyprinter context\&.
.LP
\fISee also:\fR set_ctxt_hook/2\&.
.RE
.LP
.B
get_ctxt_linewidth(Ctxt::context()) -> integer()
.br
.RS
.LP
Returns the line widh field of the prettyprinter context\&.
.LP
\fISee also:\fR set_ctxt_linewidth/2\&.
.RE
.LP
.B
get_ctxt_paperwidth(Ctxt::context()) -> integer()
.br
.RS
.LP
Returns the paper widh field of the prettyprinter context\&.
.LP
\fISee also:\fR set_ctxt_paperwidth/2\&.
.RE
.LP
.B
get_ctxt_precedence(Ctxt::context()) -> context()
.br
.RS
.LP
Returns the operator precedence field of the prettyprinter context\&. 
.LP
\fISee also:\fR set_ctxt_precedence/2\&.
.RE
.LP
.B
get_ctxt_user(Ctxt::context()) -> term()
.br
.RS
.LP
Returns the user data field of the prettyprinter context\&.
.LP
\fISee also:\fR set_ctxt_user/2\&.
.RE
.LP
.B
layout(Tree::syntaxTree()) -> document()
.br
.RS
.LP
Equivalent to layout(Tree, [])\&.
.RE
.LP
.B
layout(Tree::syntaxTree(), Options::[term()]) -> document()
.br
.RS
.TP
Types
document() (see module prettypr)
.br
.RE
.RS
.LP
Creates an abstract document layout for a syntax tree\&. The result represents a set of possible layouts (cf\&. module \fIprettypr\fR)\&. For information on the options, see format/2; note, however, that the \fIpaper\fR and \fIribbon\fR options are ignored by this function\&.
.LP
This function provides a low-level interface to the pretty printer, returning a flexible representation of possible layouts, independent of the paper width eventually to be used for formatting\&. This can be included as part of another document and/or further processed directly by the functions in the \fIprettypr\fR module, or used in a hook function (see \fIformat/2\fR for details)\&. 
.LP
\fISee also:\fR prettypr, format/2, layout/1\&.
.RE
.LP
.B
set_ctxt_hook(Ctxt::context(), Hook::hook()) -> context()
.br
.RS
.LP
Updates the hook function field of the prettyprinter context\&.
.LP
\fISee also:\fR get_ctxt_hook/1\&.
.RE
.LP
.B
set_ctxt_linewidth(Ctxt::context(), W::integer()) -> context()
.br
.RS
.LP
Updates the line widh field of the prettyprinter context\&.
.LP
Note: changing this value (and passing the resulting context to a continuation function) does not affect the normal formatting, but may affect user-defined behaviour in hook functions\&. 
.LP
\fISee also:\fR get_ctxt_linewidth/1\&.
.RE
.LP
.B
set_ctxt_paperwidth(Ctxt::context(), W::integer()) -> context()
.br
.RS
.LP
Updates the paper widh field of the prettyprinter context\&.
.LP
Note: changing this value (and passing the resulting context to a continuation function) does not affect the normal formatting, but may affect user-defined behaviour in hook functions\&. 
.LP
\fISee also:\fR get_ctxt_paperwidth/1\&.
.RE
.LP
.B
set_ctxt_precedence(Ctxt::context(), Prec::integer()) -> context()
.br
.RS
.LP
Updates the operator precedence field of the prettyprinter context\&. See the erl_parse(3) module for operator precedences\&. 
.LP
\fISee also:\fR erl_parse(3), get_ctxt_precedence/1\&.
.RE
.LP
.B
set_ctxt_user(Ctxt::context(), X::term()) -> context()
.br
.RS
.LP
Updates the user data field of the prettyprinter context\&.
.LP
\fISee also:\fR get_ctxt_user/1\&.
.RE
