.TH gb_sets 3 "stdlib  1.16.3" "Ericsson AB" "ERLANG MODULE DEFINITION"
.SH MODULE
gb_sets \- General Balanced Trees
.SH DESCRIPTION
.LP
An implementation of ordered sets using Prof\&. Arne Andersson\&'s General Balanced Trees\&. This can be much more efficient than using ordered lists, for larger sets, but depends on the application\&.

.SH COMPLEXITY NOTE
.LP
The complexity on set operations is bounded by either O(|S|) or O(|T| * log(|S|)), where S is the largest given set, depending on which is fastest for any particular function call\&. For operating on sets of almost equal size, this implementation is about 3 times slower than using ordered-list sets directly\&. For sets of very different sizes, however, this solution can be arbitrarily much faster; in practical cases, often between 10 and 100 times\&. This implementation is particularly suited for accumulating elements a few at a time, building up a large set (more than 100-200 elements), and repeatedly testing for membership in the current set\&.
.LP
As with normal tree structures, lookup (membership testing), insertion and deletion have logarithmic complexity\&.
.SH COMPATIBILITY
.LP
All of the following functions in this module also exist and do the same thing in the \fIsets\fR and \fIordsets\fR modules\&. That is, by only changing the module name for each call, you can try out different set representations\&.
.LP

.RS 2
.TP 2
*
\fIadd_element/2\fR
.TP 2
*
\fIdel_element/2\fR
.TP 2
*
\fIfilter/2\fR
.TP 2
*
\fIfold/3\fR
.TP 2
*
\fIfrom_list/1\fR
.TP 2
*
\fIintersection/1\fR
.TP 2
*
\fIintersection/2\fR
.TP 2
*
\fIis_element/2\fR
.TP 2
*
\fIis_set/1\fR
.TP 2
*
\fIis_subset/2\fR
.TP 2
*
\fInew/0\fR
.TP 2
*
\fIsize/1\fR
.TP 2
*
\fIsubtract/2\fR
.TP 2
*
\fIto_list/1\fR
.TP 2
*
\fIunion/1\fR
.TP 2
*
\fIunion/2\fR
.RE
.SH DATA TYPES

.nf
gb_set() = a GB set
.fi
.SH EXPORTS
.LP
.B
add(Element, Set1) -> Set2
.br
.B
add_element(Element, Set1) -> Set2
.br
.RS
.TP
Types
Element = term()
.br
Set1 = Set2 = gb_set()
.br
.RE
.RS
.LP
Returns a new gb_set formed from \fISet1\fR with \fIElement\fR inserted\&. If \fIElement\fR is already an element in \fISet1\fR, nothing is changed\&.
.RE
.LP
.B
balance(Set1) -> Set2
.br
.RS
.TP
Types
Set1 = Set2 = gb_set()
.br
.RE
.RS
.LP
Rebalances the tree representation of \fISet1\fR\&. Note that this is rarely necessary, but may be motivated when a large number of elements have been deleted from the tree without further insertions\&. Rebalancing could then be forced in order to minimise lookup times, since deletion only does not rebalance the tree\&.
.RE
.LP
.B
delete(Element, Set1) -> Set2
.br
.RS
.TP
Types
Element = term()
.br
Set1 = Set2 = gb_set()
.br
.RE
.RS
.LP
Returns a new gb_set formed from \fISet1\fR with \fIElement\fR removed\&. Assumes that \fIElement\fR is present in \fISet1\fR\&.
.RE
.LP
.B
delete_any(Element, Set1) -> Set2
.br
.B
del_element(Element, Set1) -> Set2
.br
.RS
.TP
Types
Element = term()
.br
Set1 = Set2 = gb_set()
.br
.RE
.RS
.LP
Returns a new gb_set formed from \fISet1\fR with \fIElement\fR removed\&. If \fIElement\fR is not an element in \fISet1\fR, nothing is changed\&.
.RE
.LP
.B
difference(Set1, Set2) -> Set3
.br
.B
subtract(Set1, Set2) -> Set3
.br
.RS
.TP
Types
Set1 = Set2 = Set3 = gb_set()
.br
.RE
.RS
.LP
Returns only the elements of \fISet1\fR which are not also elements of \fISet2\fR\&.
.RE
.LP
.B
empty() -> Set
.br
.B
new() -> Set
.br
.RS
.TP
Types
Set = gb_set()
.br
.RE
.RS
.LP
Returns a new empty gb_set\&.
.RE
.LP
.B
filter(Pred, Set1) -> Set2
.br
.RS
.TP
Types
Pred = fun (E) -> bool()
.br
 E = term()
.br
Set1 = Set2 = gb_set()
.br
.RE
.RS
.LP
Filters elements in \fISet1\fR using predicate function \fIPred\fR\&.
.RE
.LP
.B
fold(Function, Acc0, Set) -> Acc1
.br
.RS
.TP
Types
Function = fun (E, AccIn) -> AccOut
.br
Acc0 = Acc1 = AccIn = AccOut = term()
.br
 E = term()
.br
Set = gb_set()
.br
.RE
.RS
.LP
Folds \fIFunction\fR over every element in \fISet\fR returning the final value of the accumulator\&.
.RE
.LP
.B
from_list(List) -> Set
.br
.RS
.TP
Types
List = [term()]
.br
Set = gb_set()
.br
.RE
.RS
.LP
Returns a gb_set of the elements in \fIList\fR, where \fIList\fR may be unordered and contain duplicates\&.
.RE
.LP
.B
from_ordset(List) -> Set
.br
.RS
.TP
Types
List = [term()]
.br
Set = gb_set()
.br
.RE
.RS
.LP
Turns an ordered-set list \fIList\fR into a gb_set\&. The list must not contain duplicates\&.
.RE
.LP
.B
insert(Element, Set1) -> Set2
.br
.RS
.TP
Types
Element = term()
.br
Set1 = Set2 = gb_set()
.br
.RE
.RS
.LP
Returns a new gb_set formed from \fISet1\fR with \fIElement\fR inserted\&. Assumes that \fIElement\fR is not present in \fISet1\fR\&.
.RE
.LP
.B
intersection(Set1, Set2) -> Set3
.br
.RS
.TP
Types
Set1 = Set2 = Set3 = gb_set()
.br
.RE
.RS
.LP
Returns the intersection of \fISet1\fR and \fISet2\fR\&.
.RE
.LP
.B
intersection(SetList) -> Set
.br
.RS
.TP
Types
SetList = [gb_set()]
.br
Set = gb_set()
.br
.RE
.RS
.LP
Returns the intersection of the non-empty list of gb_sets\&.
.RE
.LP
.B
is_disjoint(Set1, Set2) -> bool()
.br
.RS
.TP
Types
Set1 = Set2 = gb_set()
.br
.RE
.RS
.LP
Returns \fItrue\fR if \fISet1\fR and \fISet2\fR are disjoint (have no elements in common), and \fIfalse\fR otherwise\&.
.RE
.LP
.B
is_empty(Set) -> bool()
.br
.RS
.TP
Types
Set = gb_set()
.br
.RE
.RS
.LP
Returns \fItrue\fR if \fISet\fR is an empty set, and \fIfalse\fR otherwise\&.
.RE
.LP
.B
is_member(Element, Set) -> bool()
.br
.B
is_element(Element, Set) -> bool()
.br
.RS
.TP
Types
Element = term()
.br
Set = gb_set()
.br
.RE
.RS
.LP
Returns \fItrue\fR if \fIElement\fR is an element of \fISet\fR, otherwise \fIfalse\fR\&.
.RE
.LP
.B
is_set(Term) -> bool()
.br
.RS
.TP
Types
Term = term()
.br
.RE
.RS
.LP
Returns \fItrue\fR if \fISet\fR appears to be a gb_set, otherwise \fIfalse\fR\&.
.RE
.LP
.B
is_subset(Set1, Set2) -> bool()
.br
.RS
.TP
Types
Set1 = Set2 = gb_set()
.br
.RE
.RS
.LP
Returns \fItrue\fR when every element of \fISet1\fR is also a member of \fISet2\fR, otherwise \fIfalse\fR\&.
.RE
.LP
.B
iterator(Set) -> Iter
.br
.RS
.TP
Types
Set = gb_set()
.br
Iter = term()
.br
.RE
.RS
.LP
Returns an iterator that can be used for traversing the entries of \fISet\fR; see \fInext/1\fR\&. The implementation of this is very efficient; traversing the whole set using \fInext/1\fR is only slightly slower than getting the list of all elements using \fIto_list/1\fR and traversing that\&. The main advantage of the iterator approach is that it does not require the complete list of all elements to be built in memory at one time\&.
.RE
.LP
.B
largest(Set) -> term()
.br
.RS
.TP
Types
Set = gb_set()
.br
.RE
.RS
.LP
Returns the largest element in \fISet\fR\&. Assumes that \fISet\fR is nonempty\&.
.RE
.LP
.B
next(Iter1) -> {Element, Iter2} | none
.br
.RS
.TP
Types
Iter1 = Iter2 = Element = term()
.br
.RE
.RS
.LP
Returns \fI{Element, Iter2}\fR where \fIElement\fR is the smallest element referred to by the iterator \fIIter1\fR, and \fIIter2\fR is the new iterator to be used for traversing the remaining elements, or the atom \fInone\fR if no elements remain\&.
.RE
.LP
.B
singleton(Element) -> gb_set()
.br
.RS
.TP
Types
Element = term()
.br
.RE
.RS
.LP
Returns a gb_set containing only the element \fIElement\fR\&.
.RE
.LP
.B
size(Set) -> int()
.br
.RS
.TP
Types
Set = gb_set()
.br
.RE
.RS
.LP
Returns the number of elements in \fISet\fR\&.
.RE
.LP
.B
smallest(Set) -> term()
.br
.RS
.TP
Types
Set = gb_set()
.br
.RE
.RS
.LP
Returns the smallest element in \fISet\fR\&. Assumes that \fISet\fR is nonempty\&.
.RE
.LP
.B
take_largest(Set1) -> {Element, Set2}
.br
.RS
.TP
Types
Set1 = Set2 = gb_set()
.br
Element = term()
.br
.RE
.RS
.LP
Returns \fI{Element, Set2}\fR, where \fIElement\fR is the largest element in \fISet1\fR, and \fISet2\fR is this set with \fIElement\fR deleted\&. Assumes that \fISet1\fR is nonempty\&.
.RE
.LP
.B
take_smallest(Set1) -> {Element, Set2}
.br
.RS
.TP
Types
Set1 = Set2 = gb_set()
.br
Element = term()
.br
.RE
.RS
.LP
Returns \fI{Element, Set2}\fR, where \fIElement\fR is the smallest element in \fISet1\fR, and \fISet2\fR is this set with \fIElement\fR deleted\&. Assumes that \fISet1\fR is nonempty\&.
.RE
.LP
.B
to_list(Set) -> List
.br
.RS
.TP
Types
Set = gb_set()
.br
List = [term()]
.br
.RE
.RS
.LP
Returns the elements of \fISet\fR as a list\&.
.RE
.LP
.B
union(Set1, Set2) -> Set3
.br
.RS
.TP
Types
Set1 = Set2 = Set3 = gb_set()
.br
.RE
.RS
.LP
Returns the merged (union) gb_set of \fISet1\fR and \fISet2\fR\&.
.RE
.LP
.B
union(SetList) -> Set
.br
.RS
.TP
Types
SetList = [gb_set()]
.br
Set = gb_set()
.br
.RE
.RS
.LP
Returns the merged (union) gb_set of the list of gb_sets\&.
.RE
.SH SEE ALSO
.LP
gb_trees(3), ordsets(3), sets(3)
