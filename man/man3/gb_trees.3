.TH gb_trees 3 "stdlib  1.16.3" "Ericsson AB" "ERLANG MODULE DEFINITION"
.SH MODULE
gb_trees \- General Balanced Trees
.SH DESCRIPTION
.LP
An efficient implementation of Prof\&. Arne Andersson\&'s General Balanced Trees\&. These have no storage overhead compared to unbalaced binary trees, and their performance is in general better than AVL trees\&.

.SH DATA STRUCTURE
.LP
Data structure:

.nf
      
- {Size, Tree}, where `Tree\&' is composed of nodes of the form:
  - {Key, Value, Smaller, Bigger}, and the "empty tree" node:
  - nil\&.
.fi
.LP
There is no attempt to balance trees after deletions\&. Since deletions do not increase the height of a tree, this should be OK\&.
.LP
Original balance condition \fIh(T) <= ceil(c * log(|T|))\fR has been changed to the similar (but not quite equivalent) condition \fI2 ^ h(T) <= |T| ^ c\fR\&. This should also be OK\&.
.LP
Performance is comparable to the AVL trees in the Erlang book (and faster in general due to less overhead); the difference is that deletion works for these trees, but not for the book\&'s trees\&. Behaviour is logaritmic (as it should be)\&.
.SH DATA TYPES

.nf
gb_tree() = a GB tree
.fi
.SH EXPORTS
.LP
.B
balance(Tree1) -> Tree2
.br
.RS
.TP
Types
Tree1 = Tree2 = gb_tree()
.br
.RE
.RS
.LP
Rebalances \fITree1\fR\&. Note that this is rarely necessary, but may be motivated when a large number of nodes have been deleted from the tree without further insertions\&. Rebalancing could then be forced in order to minimise lookup times, since deletion only does not rebalance the tree\&.
.RE
.LP
.B
delete(Key, Tree1) -> Tree2
.br
.RS
.TP
Types
Key = term()
.br
Tree1 = Tree2 = gb_tree()
.br
.RE
.RS
.LP
Removes the node with key \fIKey\fR from \fITree1\fR; returns new tree\&. Assumes that the key is present in the tree, crashes otherwise\&.
.RE
.LP
.B
delete_any(Key, Tree1) -> Tree2
.br
.RS
.TP
Types
Key = term()
.br
Tree1 = Tree2 = gb_tree()
.br
.RE
.RS
.LP
Removes the node with key \fIKey\fR from \fITree1\fR if the key is present in the tree, otherwise does nothing; returns new tree\&.
.RE
.LP
.B
empty() -> Tree
.br
.RS
.TP
Types
Tree = gb_tree()
.br
.RE
.RS
.LP
Returns a new empty tree
.RE
.LP
.B
enter(Key, Val, Tree1) -> Tree2
.br
.RS
.TP
Types
Key = Val = term()
.br
Tree1 = Tree2 = gb_tree()
.br
.RE
.RS
.LP
Inserts \fIKey\fR with value \fIVal\fR into \fITree1\fR if the key is not present in the tree, otherwise updates \fIKey\fR to value \fIVal\fR in \fITree1\fR\&. Returns the new tree\&.
.RE
.LP
.B
from_orddict(List) -> Tree
.br
.RS
.TP
Types
List = [{Key, Val}]
.br
 Key = Val = term()
.br
Tree = gb_tree()
.br
.RE
.RS
.LP
Turns an ordered list \fIList\fR of key-value tuples into a tree\&. The list must not contain duplicate keys\&.
.RE
.LP
.B
get(Key, Tree) -> Val
.br
.RS
.TP
Types
Key = Val = term()
.br
Tree = gb_tree()
.br
.RE
.RS
.LP
Retrieves the value stored with \fIKey\fR in \fITree\fR\&. Assumes that the key is present in the tree, crashes otherwise\&.
.RE
.LP
.B
lookup(Key, Tree) -> {value, Val} | none
.br
.RS
.TP
Types
Key = Val = term()
.br
Tree = gb_tree()
.br
.RE
.RS
.LP
Looks up \fIKey\fR in \fITree\fR; returns \fI{value, Val}\fR, or \fInone\fR if \fIKey\fR is not present\&.
.RE
.LP
.B
insert(Key, Val, Tree1) -> Tree2
.br
.RS
.TP
Types
Key = Val = term()
.br
Tree1 = Tree2 = gb_tree()
.br
.RE
.RS
.LP
Inserts \fIKey\fR with value \fIVal\fR into \fITree1\fR; returns the new tree\&. Assumes that the key is not present in the tree, crashes otherwise\&.
.RE
.LP
.B
is_defined(Key, Tree) -> bool()
.br
.RS
.TP
Types
Tree = gb_tree()
.br
.RE
.RS
.LP
Returns \fItrue\fR if \fIKey\fR is present in \fITree\fR, otherwise \fIfalse\fR\&.
.RE
.LP
.B
is_empty(Tree) -> bool()
.br
.RS
.TP
Types
Tree = gb_tree()
.br
.RE
.RS
.LP
Returns \fItrue\fR if \fITree\fR is an empty tree, and \fIfalse\fR otherwise\&.
.RE
.LP
.B
iterator(Tree) -> Iter
.br
.RS
.TP
Types
Tree = gb_tree()
.br
Iter = term()
.br
.RE
.RS
.LP
Returns an iterator that can be used for traversing the entries of \fITree\fR; see \fInext/1\fR\&. The implementation of this is very efficient; traversing the whole tree using \fInext/1\fR is only slightly slower than getting the list of all elements using \fIto_list/1\fR and traversing that\&. The main advantage of the iterator approach is that it does not require the complete list of all elements to be built in memory at one time\&.
.RE
.LP
.B
keys(Tree) -> [Key]
.br
.RS
.TP
Types
Tree = gb_tree()
.br
Key = term()
.br
.RE
.RS
.LP
Returns the keys in \fITree\fR as an ordered list\&.
.RE
.LP
.B
largest(Tree) -> {Key, Val}
.br
.RS
.TP
Types
Tree = gb_tree()
.br
Key = Val = term()
.br
.RE
.RS
.LP
Returns \fI{Key, Val}\fR, where \fIKey\fR is the largest key in \fITree\fR, and \fIVal\fR is the value associated with this key\&. Assumes that the tree is nonempty\&.
.RE
.LP
.B
map(Function, Tree1) -> Tree2
.br
.RS
.TP
Types
Function = fun(K, V1) -> V2
.br
Tree1 = Tree2 = gb_tree()
.br
.RE
.RS
.LP
maps the function F(K, V1) -> V2 to all key-value pairs of the tree Tree1 and returns a new tree Tree2 with the same set of keys as Tree1 and the new set of values V2\&.
.RE
.LP
.B
next(Iter1) -> {Key, Val, Iter2} | none
.br
.RS
.TP
Types
Iter1 = Iter2 = Key = Val = term()
.br
.RE
.RS
.LP
Returns \fI{Key, Val, Iter2}\fR where \fIKey\fR is the smallest key referred to by the iterator \fIIter1\fR, and \fIIter2\fR is the new iterator to be used for traversing the remaining nodes, or the atom \fInone\fR if no nodes remain\&.
.RE
.LP
.B
size(Tree) -> int()
.br
.RS
.TP
Types
Tree = gb_tree()
.br
.RE
.RS
.LP
Returns the number of nodes in \fITree\fR\&.
.RE
.LP
.B
smallest(Tree) -> {Key, Val}
.br
.RS
.TP
Types
Tree = gb_tree()
.br
Key = Val = term()
.br
.RE
.RS
.LP
Returns \fI{Key, Val}\fR, where \fIKey\fR is the smallest key in \fITree\fR, and \fIVal\fR is the value associated with this key\&. Assumes that the tree is nonempty\&.
.RE
.LP
.B
take_largest(Tree1) -> {Key, Val, Tree2}
.br
.RS
.TP
Types
Tree1 = Tree2 = gb_tree()
.br
Key = Val = term()
.br
.RE
.RS
.LP
Returns \fI{Key, Val, Tree2}\fR, where \fIKey\fR is the largest key in \fITree1\fR, \fIVal\fR is the value associated with this key, and \fITree2\fR is this tree with the corresponding node deleted\&. Assumes that the tree is nonempty\&.
.RE
.LP
.B
take_smallest(Tree1) -> {Key, Val, Tree2}
.br
.RS
.TP
Types
Tree1 = Tree2 = gb_tree()
.br
Key = Val = term()
.br
.RE
.RS
.LP
Returns \fI{Key, Val, Tree2}\fR, where \fIKey\fR is the smallest key in \fITree1\fR, \fIVal\fR is the value associated with this key, and \fITree2\fR is this tree with the corresponding node deleted\&. Assumes that the tree is nonempty\&.
.RE
.LP
.B
to_list(Tree) -> [{Key, Val}]
.br
.RS
.TP
Types
Tree = gb_tree()
.br
Key = Val = term()
.br
.RE
.RS
.LP
Converts a tree into an ordered list of key-value tuples\&.
.RE
.LP
.B
update(Key, Val, Tree1) -> Tree2
.br
.RS
.TP
Types
Key = Val = term()
.br
Tree1 = Tree2 = gb_tree()
.br
.RE
.RS
.LP
Updates \fIKey\fR to value \fIVal\fR in \fITree1\fR; returns the new tree\&. Assumes that the key is present in the tree\&.
.RE
.LP
.B
values(Tree) -> [Val]
.br
.RS
.TP
Types
Tree = gb_tree()
.br
Val = term()
.br
.RE
.RS
.LP
Returns the values in \fITree\fR as an ordered list, sorted by their corresponding keys\&. Duplicates are not removed\&.
.RE
.SH SEE ALSO
.LP
gb_sets(3), dict(3)
